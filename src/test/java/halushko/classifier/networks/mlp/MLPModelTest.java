package halushko.classifier.networks.mlp;

import halushko.classifier.trainingSet.SimpleTrainingSet;
import halushko.classifier.utils.TextFileUtil;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;

public class MLPModelTest {
    @Test
    public void calculateValueMLP() throws Exception {
        MLPModel model = new MLPAlgorithm().buildModel(new TextFileUtil().readPointsFromResource("iris"), SimpleTrainingSet.class);
        System.out.println("Result is: " + model.calculateValue(new double[]{5.5, 2.4, 3.7, 1.0}));
        double sum = 0;
        for(Map.Entry<Double, Double> a: model.calculateRelationsToAllClasses(new double[]{5.5, 2.4, 3.7, 1.0}).entrySet()){
            System.out.println("Relation to " + a.getKey() + " is " + a.getValue());
            sum += a.getValue();
        }
        System.out.println("Sum is " + sum);
    }

}