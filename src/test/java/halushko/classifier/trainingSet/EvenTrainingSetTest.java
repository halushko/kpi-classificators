package halushko.classifier.trainingSet;

import halushko.classifier.TrainingSet;
import halushko.classifier.regression.linear.LinearRegressionModel;
import halushko.classifier.regression.linear.LogisticLinearRegressionAlgorithm;
import halushko.classifier.utils.TextFileUtil;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class EvenTrainingSetTest {
    @Test
    public void generateNextTrainingSample() throws Exception {
        EvenTrainingSet trainingSet = new EvenTrainingSet(new String[4], new String[1]);
        int iteration;
        Set<List<Integer>> arrays = new HashSet<>();
        while ((iteration = trainingSet.generateNextTrainingSample()) != -1) {
            List<Integer> b = new ArrayList<>();
            for (int index : trainingSet.getColumnOrderByIndex(iteration)) {
                b.add(index);
            }
            arrays.add(b);
        }
        assertTrue("Count=" + arrays.size(), arrays.size() == 5 * 4 * 3 * 2);
    }

    @Test
    public void trainingSetSorter() throws Exception {
        TextFileUtil textFileUtil = new TextFileUtil();
        EvenTrainingSet trainingSet = (EvenTrainingSet)textFileUtil.readPointsFromResource("test_sorting");
        int iteration;
        while ((iteration = trainingSet.generateNextTrainingSample()) != -1) {
            int[] order = trainingSet.generatedColumnOrder.get(iteration);
            double[][] in = trainingSet.getTrainingSimpleInputValues(iteration);
            double[][] out = trainingSet.getTrainingSimpleOutputValues(iteration);
            double[][] in2 = trainingSet.getTestSimpleInputValues(iteration);
            double[][] out2 = trainingSet.getTestSimpleOutputValues(iteration);
            double[][] all = trainingSet.getOrderedPoints(order, null);
            assertNotNull(in);
            assertNotNull(out);
            assertNotNull(in2);
            assertNotNull(out2);
            assertNotNull(all);
            assertEquals(in.length, out.length);
            assertEquals(in2.length, out2.length);
            assertEquals(in.length + in2.length, all.length);

            int lengthIn = -1;
            int lengthOut = -1;

            for (int i = 0; i < in.length; i++) {
                assertNotNull(in[i]);
                assertNotNull(out[i]);
                if (lengthIn == -1) lengthIn = in[i].length;
                if (lengthOut == -1) lengthOut = out[i].length;
                assertEquals(in[i].length, lengthIn);
                assertEquals(out[i].length, lengthOut);
            }

            for (int i = 0; i < in2.length; i++) {
                assertNotNull(in2[i]);
                assertNotNull(out2[i]);
                assertEquals(in2[i].length, lengthIn);
                assertEquals(out2[i].length, lengthOut);
            }

            for (double[] row : all) {
                assertNotNull(row);
                assertEquals(row.length, lengthIn + lengthOut);
            }

            System.out.println();
            System.out.print(Arrays.toString(order));
            System.out.println();
            printArray(all);
            System.out.println();
            printArray(in, out);
            System.out.println();
            printArray(in2, out2);
        }
    }

    private void printArray(double[][] all) {
        for (double[] row : all) {
            System.out.println();
            for (double element : row) {
                System.out.print(element + " ");
            }
        }
        System.out.println();
    }


    private static void printArray(double[][] in, double[][] out) {
        for (int i = 0; i < in.length; i++) {
            System.out.println();
            for(int j = 0; j < in[i].length; j++){
                System.out.print(in[i][j] + " ");
            }
            for(int j = 0; j < out[i].length; j++){
                System.out.print(out[i][j] + " ");
            }
        }
        System.out.println();
    }
}