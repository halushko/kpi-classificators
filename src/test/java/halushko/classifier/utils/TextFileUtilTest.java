package halushko.classifier.utils;

import halushko.classifier.Matrix;
import halushko.classifier.TrainingSet;
import org.junit.Test;

import static org.junit.Assert.*;

public class TextFileUtilTest {
    @Test
    public void readPointsFromResource() throws Exception {
        try {
            TextFileUtil textFileUtil = new TextFileUtil();
            TrainingSet trainingSet = textFileUtil.readPointsFromResource("iris");
        } catch (Exception e) {
            fail("Невозможно прочитать файл");
        }

    }

    @Test
    public void writeArrayToFile() throws Exception {
        try {
            TextFileUtil textFileUtil = new TextFileUtil();
            TrainingSet trainingSet = textFileUtil.readPointsFromResource("iris");
            textFileUtil.writeArrayToFile(trainingSet.getAllValues(), "all.txt", ", ", ", ");
            textFileUtil.writeArrayToFile(trainingSet.getAllInputValues(), "input.txt", ", ", ", ");
            textFileUtil.writeArrayToFile(trainingSet.getAllOutputValues(), "output.txt", ", ", ", ");
        } catch (Exception e) {
            fail("Невозможно записать в файл");
        }
    }

    @Test
    public void writeColumnsToFiles() throws Exception {
//        try {
        TextFileUtil textFileUtil = new TextFileUtil();
        TrainingSet trainingSet = textFileUtil.readPointsFromResource("iris");

        double[][] array = trainingSet.getAllInputValues();

        for (int column = 0; column < trainingSet.getInputColumnsNames().length; column++) {
            double[][] columnValues = new double[array.length][1];
            for (int row = 0; row < columnValues.length; row++) {
                columnValues[row][0] = array[row][column];
            }
            textFileUtil.writeArrayToFile(columnValues, trainingSet.getInputColumnsNames()[column] + ".txt", ", ", ", ");
        }
        array = trainingSet.getAllOutputValues();
        for (int column = 0; column < trainingSet.getOutputColumnsNames().length; column++) {
            double[][] columnValues = new double[array.length][1];
            for (int row = 0; row < columnValues.length; row++) {
                columnValues[row][0] = array[row][column];
            }
            textFileUtil.writeArrayToFile(columnValues, trainingSet.getOutputColumnsNames()[column] + ".txt", ", ", ", ");
        }
//        } catch (Exception e) {
//            fail("Невозможно записать в файл");
//        }
    }
}