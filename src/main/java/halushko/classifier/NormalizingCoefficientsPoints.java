package halushko.classifier;

/**
 * Created by Halushko on 02.05.2016.
 */
public final class NormalizingCoefficientsPoints {
    public final Point pointMin, pointMax;

    public NormalizingCoefficientsPoints(Point pointMin, Point pointMax) {
        this.pointMin = pointMin;
        this.pointMax = pointMax;
    }
}
