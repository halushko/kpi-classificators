package halushko.classifier;

import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

@SuppressWarnings("WeakerAccess")
public abstract class TrainingSet extends LinkedHashSet<Point> {
    public String[] getInputColumnsNames() {
        String[] result = new String[inputColumnsNames.length];
        System.arraycopy(inputColumnsNames, 0, result, 0, result.length);
        return result;
    }

    public String[] getOutputColumnsNames() {
        String[] result = new String[outputColumnsNames.length];
        System.arraycopy(outputColumnsNames, 0, result, 0, result.length);
        return result;
    }

    private final String[] inputColumnsNames;
    private final String[] outputColumnsNames;

    public TrainingSet(String[] inputColumnsNames, String[] outputColumnsNames) {
        if (inputColumnsNames == null || outputColumnsNames == null || inputColumnsNames.length < 1 || outputColumnsNames.length < 1) {
            throw new HalushkoRuntimeException("Неверный индекс0!");
        }

        this.inputColumnsNames = Arrays.copyOf(inputColumnsNames, inputColumnsNames.length);
        this.outputColumnsNames = Arrays.copyOf(outputColumnsNames, outputColumnsNames.length);
    }

    public abstract int generateNextTrainingSample();
    protected abstract BitSet getTrainingSampleByIterationNumber(int iterationNumber);
    public abstract double[][] getTrainingSimpleInputValues(int iterationNumber);
    public abstract double[][] getTrainingSimpleOutputValues(int iterationNumber);
    public abstract double[][] getTestSimpleInputValues(int iterationNumber);
    public abstract double[][] getTestSimpleOutputValues(int iterationNumber);



    public double[][] getAllInputValues() {
        double[][] result = new double[size()][];
        Iterator<Point> iterator = iterator();
        int i = -1;
        while (iterator.hasNext()) {
            result[++i] = iterator.next().getInput();
        }
        return result;
    }

    public double[][] getAllOutputValues() {
        double[][] result = new double[size()][];
        Iterator<Point> iterator = iterator();
        int i = -1;
        while (iterator.hasNext()) {
            result[++i] = iterator.next().getOutput();
        }
        return result;
    }

    public double[][] getAllValues() {
        double[][] result = new double[size()][inputColumnsNames.length + outputColumnsNames.length];
        Iterator<Point> iterator = iterator();
        int i = -1;
        while (iterator.hasNext()) {
            Point point = iterator.next();
            double[] input = point.getInput();
            double[] output = point.getOutput();
            ++i;
            System.arraycopy(input, 0, result[i], 0, input.length);
            System.arraycopy(output, 0, result[i], input.length, output.length);
        }
        return result;
    }

    public NormalizingCoefficientsPoints getNormalizingCoefficients() {
        double[] minValuesInput = new double[inputColumnsNames.length];
        double[] maxValuesInput = new double[inputColumnsNames.length];
        double[] minValuesOutput = new double[outputColumnsNames.length];
        double[] maxValuesOutput = new double[outputColumnsNames.length];

        boolean flag = true;

        for (Point point : this) {
            double[] temp = point.getInput();
            for (int i = 0; i < point.getInputCount(); i++) {
                if (flag) {
                    minValuesInput[i] = temp[i];
                    maxValuesInput[i] = temp[i];
                } else {
                    if (minValuesInput[i] > temp[i]) {
                        minValuesInput[i] = temp[i];
                    }
                    if (maxValuesInput[i] < temp[i]) {
                        maxValuesInput[i] = temp[i];
                    }
                }
            }
            temp = point.getOutput();
            for (int i = 0; i < point.getOutputCount(); i++) {
                if (flag) {
                    minValuesOutput[i] = temp[i];
                    maxValuesOutput[i] = temp[i];
                } else {
                    if (minValuesOutput[i] > temp[i]) {
                        minValuesOutput[i] = temp[i];
                    }
                    if (maxValuesOutput[i] < temp[i]) {
                        maxValuesOutput[i] = temp[i];
                    }
                }
            }
            flag = false;
        }
//        for (int i = 0; i < minValuesInput.length; i++) {
//            minValuesInput[i] = Math.rint(100.0 * minValuesInput[i]) / 100.00;
//            maxValuesInput[i] = Math.rint(100.0 * maxValuesInput[i]) / 100.00;
//        }
//        for (int i = 0; i < minValuesOutput.length; i++) {
//            minValuesOutput[i] = Math.rint(100.0 * minValuesOutput[i]) / 100.00;
//            maxValuesOutput[i] = Math.rint(100.0 * maxValuesOutput[i]) / 100.00;
//        }

        return new NormalizingCoefficientsPoints(new Point(minValuesInput, minValuesOutput), new Point(maxValuesInput, maxValuesOutput));
    }

    protected static double[][] getValuesByIndexes(BitSet indexes, double[][] points) {
        if (indexes == null || points == null) {
            throw new NullPointerException("getValuesByIndexes");
        }
        int nextSetBit = indexes.nextSetBit(0);
        List<Integer> resultIndexes = new ArrayList<>();
        for (int i = 0; i < indexes.cardinality(); i++) {
            resultIndexes.add(nextSetBit);
            nextSetBit = indexes.nextSetBit(nextSetBit + 1);
        }
        double[][] result = new double[resultIndexes.size()][];
        int i = -1;
        while (resultIndexes.size() > 0) {
            Integer index = resultIndexes.remove((int) (Math.random() * (resultIndexes.size() - 1)));
            result[++i] = Arrays.copyOf(points[index], points[index].length);
        }
        return result;
    }

    public void print() {
        System.out.print("Input: ");
        for (String in : inputColumnsNames) {
            System.out.print(in + " ");
        }
        System.out.print("Output: ");
        for (String out : outputColumnsNames) {
            System.out.print(out + " ");
        }

        System.out.println();

        for (Point point : this) {
            for (double in : point.getInput()) {
                System.out.print(in + " ");
            }
            for (double out : point.getOutput()) {
                System.out.print(out + " ");
            }
            System.out.println();
        }
    }

    @Override
    public boolean add(Point newPoint) {
        if (newPoint == null) return false;
        if (inputColumnsNames.length != newPoint.getInputCount() || outputColumnsNames.length != newPoint.getOutputCount())
            throw new HalushkoRuntimeException("New vector's length in training set is not equals to columns number of training set");
//        TODO добавить исключение если новое значение уже присутствует, но с другим выходным значением?
        return super.add(newPoint);
    }

    @Override
    public boolean remove(Object pointToRemove) {
        if (pointToRemove == null || !(pointToRemove instanceof Point)) return false;
        if (inputColumnsNames.length != ((Point) pointToRemove).getInputCount() || outputColumnsNames.length != ((Point) pointToRemove).getOutputCount())
            throw new HalushkoRuntimeException("New vector's length in training set is not equals to columns number of training set");
        return super.remove(pointToRemove);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @SuppressWarnings("unused")
    @Deprecated
    @Override
    public boolean removeAll(Collection<?> points) {
        throw new HalushkoRuntimeException("Не ревлизовано. Сложнаааа!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    private TrainingSet() {
        throw new HalushkoRuntimeException("Не использовать этот конструктор!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    private TrainingSet(int columnsNumber) {
        throw new HalushkoRuntimeException("Не использовать этот конструктор!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    private TrainingSet(Collection<Double[]> c) {
        throw new HalushkoRuntimeException("Не использовать этот конструктор!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    private TrainingSet(int initialCapacity, float loadFactor) {
        throw new HalushkoRuntimeException("Не использовать этот конструктор!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    private TrainingSet(int initialCapacity, float loadFactor, boolean dummy) {
        throw new HalushkoRuntimeException("Не использовать этот конструктор!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    @Override
    public boolean removeIf(Predicate<? super Point> filter) {
        throw new HalushkoRuntimeException("Не использовать этот метод!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    @Override
    public Stream<Point> stream() {
        throw new HalushkoRuntimeException("Не использовать этот метод!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    @Override
    public Stream<Point> parallelStream() {
        throw new HalushkoRuntimeException("Не использовать этот метод!");
    }

    @SuppressWarnings("unused")
    @Deprecated
    @Override
    public void forEach(Consumer<? super Point> action) {
        throw new HalushkoRuntimeException("Не использовать этот метод!");
    }
}
