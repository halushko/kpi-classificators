package halushko.classifier.networks.mlp;

import halushko.classifier.Model;
import halushko.classifier.exceptions.HalushkoRuntimeException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.util.*;

public class MLPModel extends Model{
    private RConnection rConnection;
    private String modelMame;
    private List<Double> sortedOutputs;

    MLPModel(RConnection rConnection, String modelMame, List<Double> sortedOutputs) {
        this.rConnection = rConnection;
        this.modelMame = modelMame;
        this.sortedOutputs = sortedOutputs;
    }

    @Override
    protected double calculateValueForNormalizedInputPoint(double[] input) {
        StringBuilder sb = new StringBuilder();
        sb.append("c(");
        for(int i = 0; i < input.length; i++){
            sb.append(input[i]);
            if(i != input.length - 1)
                sb.append(", ");
            else
                sb.append(")");
        }

        try {
            REXP asd = rConnection.eval("asd = predict(" + modelMame + ", " + sb.toString() + ")");
            double[] result  = asd.asDoubles();
            System.out.println("AAA " + String.valueOf(asd.asString()));
            System.out.println("Result1 is: " + Arrays.toString(result));
            System.out.println("Result2 is: " + Arrays.toString(rConnection.eval("summary(asd)").asStrings()));
            System.out.println("Result3 is: " + Arrays.toString(rConnection.eval("summary(model)").asStrings()));

//            System.out.println("Result3 is: " + Arrays.toString(result));
//            System.out.println("Result4 is: " + Arrays.toString(result));

            int outputNumber = -1;
            double max = Double.MIN_VALUE;
            for(int i = 0; i < result.length; i++){
                if(result[i] > max){
                    max = result[i];
                    outputNumber = i;
                }
            }

            System.out.println("Result4 is: " + outputNumber);
            return this.sortedOutputs.get(outputNumber);
        } catch (RserveException | REXPMismatchException e) {
            throw new HalushkoRuntimeException("Ошибка при калькулировании результата", e);
        }
    }

    public Map<Double, Double> calculateRelationsToAllClasses(double[] input) {
        StringBuilder sb = new StringBuilder();
        sb.append("c(");
        for(int i = 0; i < input.length; i++){
            sb.append(input[i]);
            if(i != input.length - 1)
                sb.append(", ");
            else
                sb.append(")");
        }

        try {
            REXP asd = rConnection.eval("asd = predict(" + modelMame + ", " + sb.toString() + ")");
            double[] result  = asd.asDoubles();
            System.out.println("AAA " + String.valueOf(asd.asString()));
            System.out.println("Result1 is: " + Arrays.toString(result));
            System.out.println("Result2 is: " + Arrays.toString(rConnection.eval("summary(asd)").asStrings()));
            System.out.println("Result3 is: " + Arrays.toString(rConnection.eval("summary(model)").asStrings()));

//            System.out.println("Result3 is: " + Arrays.toString(result));
//            System.out.println("Result4 is: " + Arrays.toString(result));

            return new HashMap<Double, Double>(){{
                for(int i = 0; i < sortedOutputs.size(); i++){
                    put(deNormaliseOutput(sortedOutputs.get(i)), result[i]);
                }
            }};
        } catch (RserveException | REXPMismatchException e) {
            throw new HalushkoRuntimeException("Ошибка при калькулировании результата", e);
        }
    }

    private double deNormaliseOutput(double outputValue){
        return normalizingCoefficientsMin.getOutput()[0] +
                outputValue *
                        (normalizingCoefficientsMax.getOutput()[0] - normalizingCoefficientsMin.getOutput()[0]);
    }

    @Override
    protected boolean isModelEqual(Model model) {
        return model instanceof MLPModel &&  rConnection.equals(((MLPModel) model).rConnection);
    }

    @Override
    protected int getHashCode() {
        return rConnection.hashCode();
    }
}
