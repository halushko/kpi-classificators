package halushko.classifier.networks.mlp;

import halushko.classifier.ModelBuilder;
import halushko.classifier.utils.RExecutor;
import halushko.classifier.utils.TextFileUtil;

import java.util.*;

public class MLPAlgorithm extends ModelBuilder<MLPModel> {
    private static final String MODEL_NAME = "model";
    private static final String FILE_NAME = "rscript_mlp.r";
    private static final String MODEL_NAME_PARAMETER = "#$model name$#";
    private static final String COUNT_OF_INPUT_COLUMNS = "#$count of input columns$#";
    private static final String GENERATED_VECTORS_NAMES = "#$generated vector names$#";

    private static final String GENERATE_VECTORS_ACTION = "#$generate input vectors$#";

    @Override
    protected MLPModel buildModel(double[][] input, double[][] output) {
        RExecutor executor = new RExecutor();

        List<Double> sortedOutputs = new ArrayList<Double>(){
            {
                Set<Double> outputValues = new TreeSet<>();
                for (double[] anOutput : output) {
                    outputValues.add(anOutput[0]);
                }
                addAll(outputValues);
            }};

        MLPModel model = new MLPModel(executor.getConnection(), MODEL_NAME, sortedOutputs);

        Map<String, String> params = new HashMap<>();
        params.put(MODEL_NAME_PARAMETER, MODEL_NAME);

        Map<String, String> vectors = generateVectors(input, "in");
        params.put(COUNT_OF_INPUT_COLUMNS, String.valueOf(vectors.size()));

        for (Map.Entry<String, String> outVector : generateVectors(output, "out").entrySet()) {
            vectors.put(outVector.getKey(), outVector.getValue());
        }

        StringBuilder sb = new StringBuilder();
        for (Iterator<String> iterator = vectors.keySet().iterator(); iterator.hasNext(); ) {
            sb.append(iterator.next());
            if(iterator.hasNext()) sb.append(", ");
        }
        params.put(GENERATED_VECTORS_NAMES, sb.toString());

        for (String line : new TextFileUtil().readLinesFromResource(FILE_NAME)) {
            if(line.equals(GENERATE_VECTORS_ACTION)){
                runGenerateVectorsAction(executor, vectors);
            } else {
                line = setParameters(line, params);
                executor.executeLine(line, params);
            }
        }



//        model.setOutputs(sortedOutputs);

        return model;
    }

    private static void runGenerateVectorsAction(RExecutor executor, Map<String, String> vectors) {
        for(Map.Entry<String, String> vector: vectors.entrySet()){
            executor.executeLine(vector.getValue());
        }
    }

    private static String matrixColumnAsVector(String pre, String post, String delimiter, double[][] matrix, int column) {
        StringBuilder sb = new StringBuilder(pre);
        for (int i = 0; i < matrix.length; i++) {
            sb.append(matrix[i][column]);
            if(i != matrix.length - 1) sb.append(delimiter);
        }
        sb.append(post);
        return sb.toString();
    }

    private static String setParameters(String line, Map<String, String> parameters){
        for(Map.Entry<String, String> parameter: parameters.entrySet()) {
            while(line.contains(parameter.getKey())) {
                line = line.replace(parameter.getKey(), parameter.getValue());
            }
        }
        return line;
    }

    private static LinkedHashMap<String, String> generateVectors(double[][] columns, String namePrefix) {
        return new LinkedHashMap<String, String>() {
            {
                for (int i = 0; i < columns[0].length; i++) {
                    String vectorName = namePrefix + "_" + String.format("%02d", i);
                    put(vectorName, matrixColumnAsVector(vectorName + " = c(", ")", ", ", columns, i));
                }
            }
        };
    }
}
