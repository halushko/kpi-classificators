package halushko.classifier.networks.basic;

import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.*;

public abstract class Neuron {
    public final UUID neuronID = UUID.randomUUID();

    private Map<Integer, Double> outputs = new HashMap<>();
    private Set<halushko.classifier.networks.basic.WeightedEdge> inputEdges = new HashSet<>();

    public void addInputEdge(WeightedEdge edge) {
        for (WeightedEdge inputEdge : inputEdges)
            if (inputEdge.fromNeuron.equals(edge.fromNeuron))
                throw new HalushkoRuntimeException("Невозможно добавить новое ребро между нейронами. Связь между заданными нейронами уже существует");
        inputEdges.add(edge);
    }

    public double getOutput(Integer epoch) {
        if (epoch == null || !outputs.containsKey(epoch)) {
            throw new HalushkoRuntimeException("Неверно задано значение эпохи. Невозможно получить output для эпохи <" + String.valueOf(epoch) + ">");
        }
        return outputs.get(epoch);
    }

    public void calculateOutput(Integer epoch) {
        if (!outputs.keySet().isEmpty() && (epoch == null || epoch - Collections.max(outputs.keySet()) != 1)) {
            throw new HalushkoRuntimeException("Невозможно просчитать выходное значение для нейрона - эпоха не совпадает");
        }
        outputs.put(epoch, calculateOutputImplementation(getImpactNeurons(epoch)));
    }

    protected double getImpactNeurons(Integer epoch) {
        if (!outputs.keySet().isEmpty() && (epoch == null || epoch - Collections.max(outputs.keySet()) != 1)) {
            throw new HalushkoRuntimeException("Невозможно просчитать выходное значение для нейрона - эпоха не совпадает");
        }
        double inputSum = 0.0;
        for (WeightedEdge edge : inputEdges)
            inputSum += calculateInputValue(edge);//edge.getWight(epoch) * edge.fromNeuron.getOutput(epoch);
        return inputSum;
    }

    protected abstract double calculateInputValue(WeightedEdge inputEdge);

    protected abstract double calculateOutputImplementation(double input);

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Neuron && this.neuronID.equals(((Neuron) obj).neuronID);
    }

    @Override
    public int hashCode() {
        return this.neuronID.hashCode();
    }
}
