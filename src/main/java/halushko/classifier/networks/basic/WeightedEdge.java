package halushko.classifier.networks.basic;

import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.HashMap;
import java.util.Map;

public class WeightedEdge extends Edge {
    private Map<Integer, Double> valuesHistory = new HashMap<>();

    public WeightedEdge(Neuron fromNeuron, Neuron toNeuron) {
        super(fromNeuron, toNeuron);
        valuesHistory.put(0, 1.0);
    }

    public void setNewWight(Integer epoch, Double wight){
        if(epoch == null || !valuesHistory.containsKey(epoch)) {
            throw new HalushkoRuntimeException("Неверно задано значение эпохи. Невозможно проставить новое значение ребру для эпохи <" + String.valueOf(epoch) + ">");
        }
        valuesHistory.put(epoch, wight);
    }

    public double getWight(Integer epoch){
        if (!valuesHistory.containsKey(epoch)) {
            throw new HalushkoRuntimeException("Невозможно получить вес ребра для эпохи <" + String.valueOf(epoch) + ">");
        }
        return valuesHistory.get(epoch);
    }
}
