package halushko.classifier.networks.basic;

import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class Layer {
    private final Set<Neuron> neurons = new HashSet<>();

    public void calculateOutputs(final Integer epoch){
        final Set<Thread> threads = new HashSet<>();
        final Iterator<Neuron> iterator = getNeuronIterator();

        while (iterator.hasNext()){
            Thread a = new Thread() {
                @Override
                public void run() {
                    iterator.next().calculateOutput(epoch);
                    threads.remove(this);
                }
            };
            threads.add(a);
            a.start();
        }
        while (!threads.isEmpty()){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new HalushkoRuntimeException("Поток остановлен не корректно", e);
            }
        }
    }

    public boolean addNeuron(Neuron neuron){
        return neurons.add(neuron);
    }

    public boolean removeNeuron(Neuron neuron){
        return neurons.remove(neuron);
    }

    public Iterator<Neuron> getNeuronIterator(){
        return neurons.iterator();
    }
}
