package halushko.classifier.networks.basic;

public abstract class InputNeuron extends Neuron {
    protected double calculateInputValue(WeightedEdge inputEdge, Integer epoch) {
        return inputEdge.getWight(epoch);
    }
}
