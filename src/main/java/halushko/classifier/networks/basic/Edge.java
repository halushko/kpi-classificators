package halushko.classifier.networks.basic;

public abstract class Edge {
    public final Neuron fromNeuron, toNeuron;
//    public final String edgeID;

    public Edge(Neuron fromNeuron, Neuron toNeuron) {
        this.fromNeuron = fromNeuron;
        this.toNeuron = toNeuron;
//        edgeID = fromNeuron.neuronID.toString() + toNeuron.neuronID;
    }

}
