package halushko.classifier.networks.basic;

import java.util.*;

public abstract class NeuralNetwork {
    private final List<Layer> layers = new LinkedList<>();

    public boolean addLayer(Layer newLayer){
        return layers.add(newLayer);
    }

    public boolean removeLayer(Layer layerToRemove){
        return layers.remove(layerToRemove);
    }

    public Iterator<Layer> getLayerIterator(){
        return layers.listIterator();
    }
}
