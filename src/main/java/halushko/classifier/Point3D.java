package halushko.classifier;

public class Point3D {
    public final double X, Y, Z, value;

    public Point3D(double x, double y, double z, double value) {
        X = x;
        Y = y;
        Z = z;
        this.value = value;
    }
}
