package halushko.classifier.utils;

import halushko.classifier.Point;
import halushko.classifier.TrainingSet;
import halushko.classifier.exceptions.HalushkoRuntimeException;
import halushko.classifier.trainingSet.EvenTrainingSet;
import halushko.classifier.trainingSet.RandomTrainingSet;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * @author Halushko
 * @version 0.1
 */
public class ExcelUtil {
    private Workbook workbook;
    private Sheet activeSheet;

    public static void setActiveFile(String filePath) {
        instance.changeWorkbook(filePath);
    }

    public static void setActiveSheet(String sheetName) {
        instance.activeSheet = instance.changeSheet(sheetName);
    }

    public static void setActiveSheet(int sheetIndex) {
        sheetIndex--;
        if (instance.workbook == null) {
            throw new HalushkoRuntimeException("На текущий момент не задан Документ, с которого можно выбрать Лист");
        }
        if (sheetIndex < 0 || instance.workbook.getNumberOfSheets() <= sheetIndex) {
            throw new HalushkoRuntimeException("Невозможно получить Лист с заданным индексом: указанный индекс некорректен для данного Документа <" + sheetIndex + ">");
        }
        instance.activeSheet = instance.workbook.getSheetAt(sheetIndex);
    }

    public static void readTable(int outputColumnIndex, final int... tableNumbers) {
        readTable(outputColumnIndex, new TreeSet<Integer>() {{
            for (Integer i : tableNumbers != null ? tableNumbers : new int[0]) {
                add(i);
            }
        }});


    }

    public static void readTable() {
        readTable(Integer.MAX_VALUE);
    }

    public static void readTable(Integer outputColumnIndex, Set<Integer> tableNumbers) {
        instance.readTablesByNumber(outputColumnIndex, tableNumbers);
    }

    public static void readAllTables(Integer outputColumnIndex) {
        readTable(outputColumnIndex, new TreeSet<Integer>() {{
                    for (int i = 1; i < 1000; i++) add(i);
                }}
        );
    }

    public static Map<Integer, List<Cell>> getAllHeaderCells() {
        return instance.headerCells;
    }

    public static Map<Integer, Map<Integer, List<String>>> getAllTables() {
        return instance.allColumns;
    }

    private static double[][] getInputTable() {
        return getInputTable(Collections.min(instance.headerCells.keySet()));
    }

    private static double[][] getInputTable(final int tableNumber) {
        return instance.getTableWithColumns(
                tableNumber,
                new TreeSet<Integer>() {{
                    addAll(instance.allColumns.get(tableNumber).keySet());
                    remove(instance.getOutputColumn(tableNumber));
                }});
    }

    private static double[][] getOutputTable(final int tableNumber) {
        return instance.getTableWithColumns(
                tableNumber,
                new TreeSet<Integer>() {{
                    add(instance.getOutputColumn(tableNumber));
                }});
    }

    public static TrainingSet getTrainingSet(final int tableNumber) {
        double[][] input = getInputTable(tableNumber);
        double[][] output = getOutputTable(tableNumber);

        List<Cell> allHeaderCells = getAllHeaderCells().get(tableNumber);
        int outputColumn = getOutputColumn(tableNumber);
        String[] inNames = new String[allHeaderCells.size() - 1];
        String[] outNames = new String[1];
        for (int i = 0; i < allHeaderCells.size(); i++) {
            String val = getCellValue(allHeaderCells.get(i));
            if (i < outputColumn) {
                inNames[i] = val;
            } else if (i == outputColumn) {
                outNames[0] = val;
            } else {
                inNames[i - 1] = val;
            }
        }
        return new EvenTrainingSet(inNames, outNames) {{
            for (int i = 0; i < input.length; i++) {
                add(new Point(input[i], output[i]));
            }
        }};
    }

    private Workbook changeWorkbook(String filePath) {
        FileInputStream inputStream = instance.changeInputStream(filePath);

        try {
            switch (FilenameUtils.getExtension(filePath).toLowerCase()) {
                case "xlsx":
                    return instance.workbook = new XSSFWorkbook(inputStream);
                case "xls":
                    return instance.workbook = new HSSFWorkbook(inputStream);
                default:
                    throw new HalushkoRuntimeException("Файл с таким расширением не предусмотрен: <" + FilenameUtils.getExtension(filePath).toLowerCase() + ">, для <" + filePath + ">");
            }
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Ошибка при создании обьекта Документ для чтения: указанный путь <" + filePath + ">");
        }
    }

    private Sheet changeSheet(String sheetName) {
        if (workbook == null) {
            throw new HalushkoRuntimeException("На текущий момент не задан Документ, с которого можно выбрать Лист");
        }
        if (sheetName == null || sheetName.equals("")) {
            throw new HalushkoRuntimeException("Невозможно получить Лист с пустым именем");
        }
        int sheetIndex = workbook.getNumberOfSheets();
        while (--sheetIndex >= 0) {
            Sheet sheet = workbook.getSheetAt(sheetIndex);
            if (sheetName.equalsIgnoreCase(sheet.getSheetName())) {
                return sheet;
            }
        }
        throw new HalushkoRuntimeException("Невозможно получить Лист с заданным именем <" + sheetName + "> - такой Лист не найден в Документе");
    }

    /**
     * @param tableNumbers - номера таблиц, которые необходимо считать. Начала счета с 1
     */
    private void readTablesByNumber(Integer outputColumnIndex, Set<Integer> tableNumbers) {
        if (workbook == null || activeSheet == null) {
            throw new HalushkoRuntimeException("Невозможно считать данные с Документа - Документ или Лист не найдены");
        }
        if (tableNumbers == null) tableNumbers = new TreeSet<>();
        if (tableNumbers.size() == 0) tableNumbers.add(1);

        this.outputColumnIndex = outputColumnIndex != null ? outputColumnIndex : Integer.MAX_VALUE;

        Row headerRow;
        Iterator<Row> rowIterator = instance.activeSheet.rowIterator();
        headerCells = new HashMap<>();
        allColumns = new HashMap<>();

        int currentTableNumber = 0;

        if (rowIterator.hasNext()) {
            headerRow = rowIterator.next();
            Iterator<Cell> cellIterator = headerRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell someHeaderCell = cellIterator.next();
                if (someHeaderCell.getCellType() != Cell.CELL_TYPE_BLANK || someHeaderCell.getCellType() != Cell.CELL_TYPE_ERROR) {
                    currentTableNumber++;
                    if (tableNumbers.contains(currentTableNumber)) {
                        final Cell finalSomeHeaderCell = someHeaderCell;
                        headerCells.put(currentTableNumber, new ArrayList<Cell>() {{
                            add(finalSomeHeaderCell);
                        }});
                        allColumns.put(currentTableNumber, new HashMap<Integer, List<String>>() {{
                            put(0, new ArrayList<String>());
                        }});
                    }
                    int counter = 0;
                    while (cellIterator.hasNext()) {
                        someHeaderCell = cellIterator.next();
                        if (someHeaderCell.getCellType() == Cell.CELL_TYPE_BLANK) break;
                        if (tableNumbers.contains(currentTableNumber)) {
                            headerCells.get(currentTableNumber).add(someHeaderCell);
                            allColumns.get(currentTableNumber).put(++counter, new ArrayList<String>());
                        }
                    }
                }
            }
        } else {
            throw new HalushkoRuntimeException("Невозможно считать данные с Документа - указанный Лист пустой");
        }

        int finishedKeys = 0;
        while (rowIterator.hasNext() && finishedKeys < headerCells.keySet().size()) {
            Row activeRow = rowIterator.next();
            for (Integer key : headerCells.keySet()) {
                for (int counter = 0; counter < headerCells.get(key).size(); counter++) {
                    Cell currentCell = activeRow.getCell(headerCells.get(key).get(counter).getColumnIndex());
                    if (currentCell.getCellType() == Cell.CELL_TYPE_BLANK && currentCell.getCellType() == Cell.CELL_TYPE_ERROR) {
                        finishedKeys++;
                        break;
                    }
                    try {
                        allColumns.get(key).get(counter).add(getCellValue(currentCell));
                    } catch (Exception e) {
                        throw new HalushkoRuntimeException("Какая-то левая ошибка для Яичейки [" + activeRow.getRowNum() + "; " + currentCell.getColumnIndex() + "]\n", e);
                    }
                }
            }
        }

        printTables(headerCells, allColumns);
    }

    private double[][] getTableWithColumns(int tableNumber, Set<Integer> columnNumbers) {
        Map<Integer, List<String>> columns = allColumns.get(tableNumber);

        int size = -1;

        for (int columnNumber : columns.keySet()) {
            int columnSize = columns.get(columnNumber).size();
            if (size == -1) {
                size = columnSize;
            } else {
                if (size != columnSize) {
                    throw new HalushkoRuntimeException("В таблице колонки разной длинны");
                }
            }
        }

        double[][] result = new double[columnNumbers.size()][];

        int i = -1;
        for (int columnNumber : columnNumbers) {
            List<String> column = columns.get(columnNumber);
            result[++i] = new double[column.size()];
            for (int j = 0; j < column.size(); j++) {
                String stringValue = column.get(j);
                Double doubleValue;
                try {
                    doubleValue = Double.parseDouble(stringValue);
                } catch (Exception e) {
                    throw new HalushkoRuntimeException("Какая-то лажа форматом чисел", e);
                }
                result[i][j] = doubleValue;
            }
        }

        System.out.println("Table No" + tableNumber);
        for (i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                System.out.print(" " + result[i][j]);
            }
            System.out.println();
        }

        return result;
    }

    private static int getOutputColumn(int tableNumber) {
        int size = instance.headerCells.get(tableNumber).size();
        return instance.outputColumnIndex == Integer.MIN_VALUE ? 0 : instance.outputColumnIndex == Integer.MAX_VALUE ? size - 1 : instance.outputColumnIndex;
    }

    //region Support code
    private static int NUMERIC_VALUE_OF_A = Character.getNumericValue('A');
    private static int NUMERIC_VALUE_OF_Z = Character.getNumericValue('Z');

    private static ExcelUtil instance = new ExcelUtil();
    private FileInputStream inputStream;

    private Map<Integer, List<Cell>> headerCells = new HashMap<>();
    private Map<Integer, Map<Integer, List<String>>> allColumns = new HashMap<>();
    private Integer outputColumnIndex;

    private ExcelUtil() {
    }

    private FileInputStream changeInputStream(String filePath) {
        try {
            if (instance.inputStream != null) {
                instance.inputStream.close();
            }
            return instance.inputStream = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            throw new HalushkoRuntimeException("Файл с указанным именем не найден или поврежрен", e);
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Не удается закрыть поток чтения для предыдущего файла. \nП.С. Это ворнинг, а не эррор", e);
        }
    }

    private void printTables(Map<Integer, List<Cell>> headerCells, Map<Integer, Map<Integer, List<String>>> allColumns) {
        for (int tableCounter = 1; tableCounter < 1000; tableCounter++) {
            if (headerCells.keySet().contains(tableCounter)) {
                StringBuilder sb = new StringBuilder("Table No").append(tableCounter);
                for (int columnCounter = 0; columnCounter < headerCells.get(tableCounter).size(); columnCounter++) {
                    sb.append("\n").append(headerCells.get(tableCounter).get(columnCounter).getStringCellValue()).append(":");
                    for (int rowCounter = 0; rowCounter < allColumns.get(tableCounter).get(columnCounter).size(); rowCounter++) {
                        sb.append(" ").append(allColumns.get(tableCounter).get(columnCounter).get(rowCounter));
                    }
                }
                System.out.println(sb);
            }
        }
    }

    private static int getIndexByColumnName(String columnName) {
        int maxValue = NUMERIC_VALUE_OF_Z - NUMERIC_VALUE_OF_A + 1;
        int multiply = 1;
        int sum = 0;
        for (int i = 0; i < columnName.length(); i++) {
            sum += (Character.getNumericValue(columnName.charAt(columnName.length() - i - 1)) - NUMERIC_VALUE_OF_A + 1) * multiply;
            multiply *= maxValue;
        }
        return sum;
    }

    private static String getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return String.valueOf(cell.getStringCellValue());
            case Cell.CELL_TYPE_BLANK:
                return String.valueOf(" ");
            default:
                return String.valueOf(cell);
        }
    }
    //endregion
}
