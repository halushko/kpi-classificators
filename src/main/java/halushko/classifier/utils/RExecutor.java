package halushko.classifier.utils;

import halushko.classifier.exceptions.HalushkoRuntimeException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.util.Map;

public class RExecutor {
    private final RConnection connection;

    public RExecutor() {
        try {
            this.connection = new RConnection();
        } catch (RserveException e) {
            throw new HalushkoRuntimeException("Не возможно подсоединиться к серверу R");
        }
    }

    protected void finalize() throws Throwable {
        super.finalize();
        connection.close();
    }

    public RConnection getConnection() {
        return connection;
    }

    public REXP executeLine(String line) {
        try {
            return connection.eval(line);
        } catch (RserveException e) {
            throw new HalushkoRuntimeException("Невозможно выполнить строку R {" + line + "}", e);
        }
    }
    public REXP executeLine(String line, Map<String, String> params) {
        for (Map.Entry<String, String> parameter : params.entrySet()) {
            line = line.replaceAll(parameter.getKey(), parameter.getValue());
        }
        return executeLine(line);
    }

    public static void testExecution() {
        RConnection connection = null;
        try {
            /* Create a connection to Rserve instance running * on default port 6311              */
            connection = new RConnection();
            String vector = "c(1,2,3,4)";
            connection.eval("meanVal=mean(" + vector + ")");
            double mean = connection.eval("meanVal").asDouble();
            System.out.println("The mean of given vector is=" + mean);
        } catch (RserveException e) {
            e.printStackTrace();
        } catch (REXPMismatchException e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }
    }
}
