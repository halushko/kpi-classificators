package halushko.classifier.utils;

import halushko.classifier.Model;
import halushko.classifier.Point;
import halushko.classifier.Points3DSet;
import halushko.classifier.TrainingSet;
import halushko.classifier.exceptions.HalushkoRuntimeException;
import halushko.classifier.trainingSet.EvenTrainingSet;
import halushko.classifier.trainingSet.RandomTrainingSet;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TextFileUtil {
    public Points3DSet read3DPoints(String fileName) {
        return new Points3DSet() {{
            try (BufferedReader br = new BufferedReader(new InputStreamReader(getIS(fileName)))) {
                for (String line; (line = br.readLine()) != null; ) {
                    String[] values = line.split("\\s+");
                    add(values[0], values[1], values[3], values[2]);
                }
            } catch (IOException e) {
                throw new HalushkoRuntimeException("Не удалось почитать файлик. Шота не так", e);
            }
        }};
    }

    public TrainingSet readPointsFromResource(String fileName) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getIS(fileName)))) {
            TrainingSet trainingSet = null;
            for (String line; (line = br.readLine()) != null; ) {
                String[] values = line.split("\\s+");
                if (trainingSet != null) {
                    trainingSet.add(new Point(
                            Arrays.copyOfRange(values, 0, values.length - 1),
                            Arrays.copyOfRange(values, values.length - 1, values.length)
                    ));
                } else {
                    trainingSet = new EvenTrainingSet(
                            Arrays.copyOfRange(values, 0, values.length - 1),
                            Arrays.copyOfRange(values, values.length - 1, values.length));
                }
            }
            return trainingSet;
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Не удалось почитать файлик. Шота не так", e);
        }
    }

    public List<String> readLinesFromResource(String fileName) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getIS(fileName)))) {
            for (String line; (line = br.readLine()) != null; ) {
                lines.add(line);
            }
            return lines;
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Не удалось почитать файлик. Шота не так", e);
        }
    }

    /**
     * @return filename
     */
    public String writeModelToFile(Model model) {
        return null;
    }


    public void writeArrayToFile(double[][] array, String fileName, String columnDelimiter, String rowDelimiter) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName) {{
            if (!exists()) createNewFile();
        }}.getAbsoluteFile()))) {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    bw.write(String.valueOf(array[i][j]));
                    if (j != array[i].length - 1) bw.write(columnDelimiter);
                }
                if (i != array.length - 1) bw.write(rowDelimiter);
            }
            bw.close();
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Проблема при записи в файл: ", e);
        }

    }

    public void writeArrayToFile(double[] array, String fileName, String columnDelimiter) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName) {{
            if (!exists()) createNewFile();
        }}.getAbsoluteFile()))) {
            for (int i = 0; i < array.length; i++) {
                bw.write(String.valueOf(array[i]));
                if (i != array.length - 1) bw.write(columnDelimiter);
            }
            bw.close();
        } catch (IOException e) {
            throw new HalushkoRuntimeException("Проблема при записи в файл: ", e);
        }

    }

    private InputStream getIS(String fileName) {
        return getClass().getClassLoader().getResourceAsStream(fileName);
    }
}
