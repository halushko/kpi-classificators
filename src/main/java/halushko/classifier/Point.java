package halushko.classifier;

import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.Arrays;

public class Point{
    private final double[] input;
    private final double[] output;

    public Point(double[] input, double[] output) {
        if(input == null || input.length < 1 || output == null || output.length < 1){
            throw new HalushkoRuntimeException("Массив чисел пустой. Не надо так");
        }

        this.input = Arrays.copyOf(input, input.length);
        this.output = Arrays.copyOf(output, output.length);
    }

    public Point(String[] input, String[] output) {
        if(input == null || input.length < 1 || output == null || output.length < 1){
            throw new HalushkoRuntimeException("Массив чисел пустой. Не надо так");
        }
        this.input = stringToDouble(input);
        this.output = stringToDouble(output);
    }

    public int getInputCount(){
        return input == null ? 0 : input.length;
    }
    public int getOutputCount(){
        return output == null ? 0 : output.length;
    }

    public double[] getInput(){
        return Arrays.copyOf(input, getInputCount());
    }

    public double[] getOutput(){
        return Arrays.copyOf(output, getOutputCount());
    }

    @Override
    public int hashCode(){
        return Arrays.hashCode(getInput());
    }

    @Override
    public boolean equals(Object object){
        if(object != null && object instanceof Point){
            Point otherPoint = (Point) object;
            if(getInputCount() == otherPoint.getInputCount() && getOutputCount() == otherPoint.getOutputCount()) {
                double[] thisPoints = getInput();
                double[] otherPoints = otherPoint.getInput();

                for (int i = 0; i < otherPoint.getInputCount(); i++){
                    if(thisPoints[i] != otherPoints[i]) return false;
                }

                thisPoints = getOutput();
                otherPoints = otherPoint.getOutput();

                for (int i = 0; i < otherPoint.getOutputCount(); i++){
                    if(thisPoints[i] != otherPoints[i]) return false;
                }
                return true;
            }
        }
        return false;
    }
    private static double[] stringToDouble(String[] str){
        double[] result = new double[str.length];
        for (int i = 0; i < str.length ; i++) {
            result[i] = Double.valueOf(str[i]);
        }
        return result;
    }
}
