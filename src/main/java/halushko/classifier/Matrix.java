package halushko.classifier;

import java.util.Arrays;

final public class Matrix {
    private final int numberOfRows;             // number of rowsCount
    private final int numberOfColumns;             // number of columnsCount
    private final double[][] data;   // numberOfRows-by-numberOfColumns array

    /**
     * create numberOfRows-by-numberOfColumns zero matrix
     *
     * @param numberOfRows    rowsCount
     * @param numberOfColumns columnsCount
     */
    public Matrix(int numberOfRows, int numberOfColumns) {
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
        data = new double[numberOfRows][numberOfColumns];
    }

    /**
     * Identity matrix d*d
     *
     * @param d count of columnsCount
     * @return Identity matrix d*d
     */
    public static Matrix eye(int d) {
        Matrix m = new Matrix(d, d);

        for (int i = 0; i < d; i++) {
            m.data[i][i] = 1;
        }
        return m;
    }

    /**
     * @param numberOfRows    - number of rowsCount
     * @param numberOfColumns - number of columnsCount
     * @return Identity matrix m*n
     */
    public static Matrix eye(int numberOfRows, int numberOfColumns) {
        Matrix result = new Matrix(numberOfRows, numberOfColumns);

        int min = Math.min(numberOfRows, numberOfColumns);
        for (int i = 0; i < min; i++) {
            result.data[i][i] = 1;
        }
        return result;
    }

    public Matrix row(int index) {
        if (index < 0 || index > numberOfRows - 1)
            throw new IllegalStateException("There is no such row");
        double[][] tempRow = new double[1][numberOfRows];
        for (int i = 0; i < numberOfRows; i++)
            tempRow[0][i] = get(index, i);
        return new Matrix(tempRow);
    }

    public double[] getRow(int index) {
        return data[index];
    }

    public int rowsCount() {
        return numberOfRows;
    }

    public int columnsCount() {
        return numberOfColumns;
    }

    /**
     * @return pseudoinverse matrix of current matrix
     */
    public Matrix pinv() {
        Matrix T = this.transpose();
        return ((T.multiplyToMatrix(this)).getInverse()).multiplyToMatrix(T);
    }

    public Matrix addRow(int index, double[] row) {
        Matrix result = new Matrix(this.numberOfRows, this.numberOfColumns + 1);
        for (int i = 0; i < result.columnsCount(); i++)
            for (int j = 0; j < result.rowsCount(); j++)
                if (j < index)
                    result.set(j, i, this.get(j, i));
                else if (j > index)
                    result.set(j, i, this.get(j - 1, i));
                else
                    result.set(j, i, row[j]);
        return result;
    }

    public Matrix addColumn(int index, double[] column) {
        Matrix result = new Matrix(this.numberOfRows, this.numberOfColumns + 1);
        for (int i = 0; i < result.rowsCount(); i++)
            for (int j = 0; j < result.columnsCount(); j++)
                if (j < index)
                    result.set(i, j, this.get(i, j));
                else if (j > index)
                    result.set(i, j, this.get(i, j - 1));
                else
                    result.set(i, j, column[i]);
        return result;
    }

    public Matrix addColumnOne(int index) {
        Matrix result = new Matrix(this.numberOfRows, this.numberOfColumns + 1);
        for (int i = 0; i < result.rowsCount(); i++)
            for (int j = 0; j < result.columnsCount(); j++)
                if (j < index)
                    result.set(i, j, this.get(i, j));
                else if (j > index)
                    result.set(i, j, this.get(i, j - 1));
                else
                    result.set(i, j, 1.0);
        return result;
    }

    public double get(int m, int n) {
        return data[m][n];
    }

    public void set(int m, int n, double value) {
        data[m][n] = value;
    }

    // create matrix based on 2d array
    public Matrix(double[][] data) {
        numberOfRows = data.length;
        numberOfColumns = data[0].length;
        this.data = new double[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++)
            this.data[i] = Arrays.copyOf(data[i], columnsCount());
    }

    // copy constructor
    public Matrix(Matrix A) {
        this(A.data);
    }

    // create and return a random numberOfRows-by-numberOfColumns matrix with values between 0 and 1
    public static Matrix random(int M, int N) {
        Matrix A = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[i][j] = Math.random();
        return A;
    }

    // swap rowsCount i and j
    private void swap(int i, int j) {
        double[] temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    public Matrix transpose() {
        Matrix A = new Matrix(numberOfColumns, numberOfRows);
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfColumns; j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }

    /**
     * @param B
     * @return C = A + B
     */
    public Matrix plus(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        Matrix C = new Matrix(numberOfRows, numberOfColumns);
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfColumns; j++)
                C.data[i][j] = A.data[i][j] + B.data[i][j];
        return C;
    }

    /**
     * @param B
     * @return C = A - B
     */
    public Matrix minus(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        Matrix C = new Matrix(numberOfRows, numberOfColumns);
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfColumns; j++)
                C.data[i][j] = A.data[i][j] - B.data[i][j];
        return C;
    }

    // does A = B exactly?
    public boolean eq(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfColumns; j++)
                if (A.data[i][j] != B.data[i][j]) return false;
        return true;
    }

    /**
     * @param B
     * @return C = A * B
     */
    public Matrix multiplyToMatrix(Matrix B) {
        Matrix A = this;
        if (A.numberOfColumns != B.numberOfRows) throw new IllegalStateException("Illegal matrix dimensions.");
        Matrix C = new Matrix(A.numberOfRows, B.numberOfColumns);
        for (int i = 0; i < C.numberOfRows; i++)
            for (int j = 0; j < C.numberOfColumns; j++)
                for (int k = 0; k < A.numberOfColumns; k++)
                    C.data[i][j] += (A.data[i][k] * B.data[k][j]);
        return C;
    }

    /**
     * @param d - number
     * @return C = A * d
     */
    public Matrix multiplyToConst(double d) {
        Matrix C = new Matrix(numberOfRows, numberOfColumns);

        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfColumns; j++)
                C.data[i][j] = data[i][j] * d;

        return C;
    }


    /**
     * @return x = A^-1 b, assuming A is square and has full rank
     */
    public Matrix solve(Matrix rhs) {

        if (numberOfRows != numberOfColumns || rhs.numberOfRows != numberOfColumns || rhs.numberOfColumns != 1)
            throw new IllegalStateException("Illegal matrix dimensions.");

        // create copies of the data
        Matrix A = new Matrix(this);
        Matrix b = new Matrix(rhs);

        // Gaussian elimination with partial pivoting
        for (int i = 0; i < numberOfColumns; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < numberOfColumns; j++)
                if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i]))
                    max = j;
            A.swap(i, max);
            b.swap(i, max);

            // singular
            if (A.data[i][i] == 0.0) throw new IllegalStateException("Matrix is singular.");

            // pivot within b
            for (int j = i + 1; j < numberOfColumns; j++)
                b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];

            // pivot within A
            for (int j = i + 1; j < numberOfColumns; j++) {
                double m = A.data[j][i] / A.data[i][i];
                for (int k = i + 1; k < numberOfColumns; k++) {
                    A.data[j][k] -= A.data[i][k] * m;
                }
                A.data[j][i] = 0.0;
            }
        }

        // back substitution
        Matrix x = new Matrix(numberOfColumns, 1);
        for (int j = numberOfColumns - 1; j >= 0; j--) {
            double t = 0.0;
            for (int k = j + 1; k < numberOfColumns; k++)
                t += A.data[j][k] * x.data[k][0];
            x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
        }
        return x;

    }

    // print matrix to standard output
    public void print() {
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++)
                System.out.printf("%9.4f ", data[i][j]);
            System.out.println();
        }
    }

    public Matrix minor(int m, int n) throws Exception {
        if (m >= numberOfRows || n >= numberOfColumns || m < 0 || n < 0) {
            return null;
        }

        Matrix result = new Matrix(numberOfRows - 1, numberOfColumns - 1);
        int colCount = 0;
        int rowCount = 0;

        for (int i = 0; i < numberOfRows; i++) {
            if (i != m) {
                colCount = 0;
                for (int j = 0; j < numberOfColumns; j++) {
                    if (j != n) {
                        result.set(rowCount, colCount, this.get(i, j));
                        colCount++;
                    }
                }
                rowCount++;
            }
        }
        return result;
    }

    private static void checkDimensions(Matrix B, Matrix a) {
        if (B.numberOfRows != a.numberOfRows || B.numberOfColumns != a.numberOfColumns)
            throw new IllegalStateException("Illegal matrix dimensions.");
    }

    public double[] getColumn(int colnum) {
        double[] col = new double[numberOfRows];
        for (int i = 0; i < numberOfRows; i++) {
            col[i] = data[i][colnum];
        }
        return col;
    }

    public Matrix getInverse() {
        double[][] mass = this.data;
        double[][] LU;
        int cnt_str = numberOfRows;
        //double[][] M_obr = new double[numberOfRows][numberOfRows];
        int i, j, k;
        double sum;
        LU = new double[cnt_str][]; //создаём массив под матрицу LU
        for (i = 0; i < cnt_str; i++)
            LU[i] = new double[cnt_str];
        for (i = 0; i < cnt_str; i++) {
            for (j = 0; j < cnt_str; j++) {
                sum = 0;
                if (i <= j) {
                    for (k = 0; k < i; k++)
                        sum += LU[i][k] * LU[k][j];
                    LU[i][j] = mass[i][j] - sum;//вычисляем элементы верхней треугольной матрицы
                } else {
                    for (k = 0; k < j; k++)
                        sum += LU[i][k] * LU[k][j];
                    if (LU[j][j] == 0)
                        throw new IllegalStateException("Error in inverse matrix");
                    LU[i][j] = (mass[i][j] - sum) / LU[j][j];//вычисляем элементы нижней треугольной матрицы
                }
            }
        }
        double[][] M_obr = new double[cnt_str][]; //вычисляем элементы нижней треугольной матрицы
        for (i = 0; i < cnt_str; i++)
            M_obr[i] = new double[cnt_str];
        int p;
        for (i = cnt_str - 1; i >= 0; i--)//нахождение обратной матрицы
        {
            for (j = cnt_str - 1; j >= 0; j--) {
                sum = 0;
                if (i == j) {
                    for (p = j + 1; p < cnt_str; p++)
                        sum += LU[j][p] * M_obr[p][j];
                    M_obr[j][j] = (1 - sum) / LU[j][j];
                } else if (i < j) {
                    for (p = i + 1; p < cnt_str; p++)
                        sum += LU[i][p] * M_obr[p][j];
                    M_obr[i][j] = -sum / LU[i][i];
                } else {
                    for (p = j + 1; p < cnt_str; p++)
                        sum += M_obr[i][p] * LU[p][j];
                    M_obr[i][j] = -sum;
                }
            }
        }
        return new Matrix(M_obr);
    }

    /**
     * добавить столбец с единицами на начало
     */
    public static Matrix add1rowToTheBegin(Matrix input) {
        int current = -1;
        Matrix result = new Matrix(input.rowsCount(), input.columnsCount() + 1);

        for (int i = 0; i < input.rowsCount(); i++) {
            ++current;
            for (int j = 0; j < input.columnsCount() + 1; j++) {
                if (j == 0) result.set(current, 0, 1);
                else result.set(current, j, input.get(i, j - 1));
            }
        }

        return result;
    }

    /**
     * @param input
     * @param flag  = true - добавить столбец с единицами на начало
     * @return
     */
    public static Matrix getElements(Matrix input, boolean flag) {
        int t = flag ? 1 : 0;
        int count = input.columnsCount() + t;

        int current = -1;
        Matrix result = new Matrix(input.data.length, input.columnsCount() + t);

        for (int i = 0; i < input.rowsCount(); i++) {
            ++current;
            for (int j = 0; j < count; j++)
                if (flag && j == 0) result.set(current, 0, 1);
                else result.set(current, j, input.get(i, j - t));
        }

        return result;
    }

    public void setRow(int num, double[] row) {
        if (num < 0 || num >= rowsCount() || row == null || row.length < columnsCount())
            throw new IllegalArgumentException("setRow");

        for (int i = 0; i < columnsCount(); i++) {
            set(num, i, row[i]);
        }
    }
}
