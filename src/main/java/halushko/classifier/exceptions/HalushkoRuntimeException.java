package halushko.classifier.exceptions;

/**
 * @author Halushko
 * @version 0.1
 */
public class HalushkoRuntimeException extends RuntimeException {
    private String message;
    private Exception exception;

    public HalushkoRuntimeException(String message, Exception e) {
        super(message, e);
        this.message = message;
        this.exception = e;
    }

    public HalushkoRuntimeException(String message) {
        super(message);
        this.message = message;
        this.exception = this;
    }
}
