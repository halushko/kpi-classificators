package halushko.classifier.regression.linear;

import halushko.classifier.Model;
import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.Arrays;

public class LinearRegressionModel extends Model {
    LinearRegressionModel(double[] w) {
        super();
        setCoefficients(w);
    }

    private void setCoefficients(double[] coefficients) {
        if (coefficients == null || coefficients.length < 1) {
            throw new HalushkoRuntimeException("В линейной регрессии должен быть как минимум один параметр - константа");
        }
        this.coefficients = Arrays.copyOf(coefficients, coefficients.length);
    }

    @Override
    protected double calculateValueForNormalizedInputPoint(double[] input) {
        if (input == null || input.length != coefficients.length - 1) {
            throw new HalushkoRuntimeException("Для получения вывода линейной регрессии нужно чтобы входные данные имели ту же размерность, что и обучающая выборка");
        }

        int sum = 0;
        sum += coefficients[0];

        for (int i = 0; i < input.length; i++) {
            sum += input[i] * coefficients[i + 1];
        }
        return sum;
    }

    @Override
    protected boolean isModelEqual(Model model) {
        return model instanceof LinearRegressionModel && Arrays.equals(coefficients, ((LinearRegressionModel) model).coefficients);
    }

    @Override
    protected int getHashCode() {
        return Arrays.hashCode(coefficients);
    }

    private double[] coefficients;

}
