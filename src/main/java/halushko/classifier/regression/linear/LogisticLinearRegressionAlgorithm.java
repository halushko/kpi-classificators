package halushko.classifier.regression.linear;

import halushko.classifier.Matrix;
import halushko.classifier.ModelBuilder;
import halushko.classifier.exceptions.HalushkoRuntimeException;

public class LogisticLinearRegressionAlgorithm extends ModelBuilder<LinearRegressionModel> {
    public static final double ALLOWABLE_ERROR = 0.30;
    public static final double EPSILON = 0.00001;
    public static final int MAX_ITERATIONS_COUNT = 100;

    @Override
    protected LinearRegressionModel buildModel(double[][] inputLearn, double[][] outputLearn) {
        if (inputLearn == null || outputLearn == null || inputLearn.length == 0 || inputLearn.length != outputLearn.length) {
            throw new HalushkoRuntimeException("Для того, чтобы построить модель, нужны параметры одной длинны. И они должны быть не пустыми");
        }

        Matrix W = GetWeights(
                Matrix.getElements(new Matrix(inputLearn), true),
                Matrix.getElements(new Matrix(outputLearn), false),
                getMaxIterationsCount()
        );

        return new LinearRegressionModel(W.getColumn(0));
    }


    private Matrix GetWeights(Matrix X, Matrix Y, int max_iteration) {

        Matrix W = X.pinv().multiplyToMatrix(Y);
        double min_epsilon = 100000;
        Matrix W_min = new Matrix(W);

        while (--max_iteration > 0) {
            Matrix t = Matrix.eye(W.rowsCount()).multiplyToConst(0.003);
            Matrix Z = X.multiplyToMatrix(W);
            Matrix P = new Matrix(Z);
            Matrix G = new Matrix(Z.rowsCount(), Z.rowsCount());
            Matrix U = new Matrix(Z);
            for (int i = 0; i < Z.rowsCount(); i++) {
                double z = Z.get(i, 0);
                double p = 1.0 / (1 + Math.exp(-z));
                double g = p * (1 - p);
                double u = z + (Y.get(i, 0) - p) / g;
                P.set(i, 0, p);
                G.set(i, i, g);
                U.set(i, 0, u);
            }
            Matrix W_new =
                    (X.transpose().multiplyToMatrix(G).multiplyToMatrix(X).plus(t)).getInverse().multiplyToMatrix(X.transpose()).multiplyToMatrix(G).multiplyToMatrix(U);

            double curent_epsilone = ordinaryLeastSquares(W, W_new);
            if (((Double) curent_epsilone).isNaN() || ((Double) curent_epsilone).isInfinite()) {
                W = W_min;
                break;
            }
            if (curent_epsilone < min_epsilon) {
                W_min = W_new;
                min_epsilon = curent_epsilone;
            }
            //System.out.println(curent_epsilone);
            W = W_new;
            if (curent_epsilone <= getEpsilon()) break;
        }

        return W;
    }
    private static double ordinaryLeastSquares(Matrix W, Matrix W_new) {
        double currentEpsilon = 0;
        for (int i = 0; i < W.rowsCount(); i++) {
            currentEpsilon += Math.pow(W_new.get(i, 0) - W.get(i, 0), 2);
        }
        return currentEpsilon;
    }

    private double allowableError = ALLOWABLE_ERROR;
    private double epsilon = EPSILON;
    private int maxIterationsCount = MAX_ITERATIONS_COUNT;

    public int getMaxIterationsCount() {
        return maxIterationsCount;
    }

    public void setMaxIterationsCount(int maxIterationsCount) {
        this.maxIterationsCount = maxIterationsCount;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public double getAllowableError() {
        return allowableError;
    }

    public void setAllowableError(double allowableError) {
        this.allowableError = allowableError;
    }
}
