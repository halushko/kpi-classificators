package halushko.classifier.trainingSet;

import halushko.classifier.Point;
import halushko.classifier.TrainingSet;
import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.*;

public class RandomTrainingSet extends TrainingSet {
    private final Set<BitSet> trainingSamples = new LinkedHashSet<>();

    public RandomTrainingSet(String[] inputColumnsNames, String[] outputColumnsNames) {
        super(inputColumnsNames, outputColumnsNames);
    }

    @Override
    public int generateNextTrainingSample() {
        int length = size() / 2;
        List<Integer> allIndexes = new ArrayList<>();
        for (int i = 0; i < size(); i++) allIndexes.add(i);
        BitSet indexes = generateNextRandomIndexes(allIndexes, new BitSet(size()), length);
        if (indexes != null) {
            trainingSamples.add(indexes);
            return trainingSamples.size() - 1;
        } else {
            return -1;
        }
    }

    @Override
    protected BitSet getTrainingSampleByIterationNumber(int iterationNumber) {
        if (trainingSamples.size() > iterationNumber && iterationNumber >= 0) {
            for (BitSet result : trainingSamples) {
                if (--iterationNumber < 0) {
                    return (BitSet) result.clone();
                }
            }
        } else {
            throw new IndexOutOfBoundsException("Итерации с таким номером не существует");
        }
        throw new HalushkoRuntimeException("Вот не должно было сюда дойти");
    }

    public double[][] getTrainingSimpleInputValues(int iterationNumber) {
        return getValuesByIndexes(getTrainingSampleByIterationNumber(iterationNumber), getAllInputValues());
    }

    public double[][] getTrainingSimpleOutputValues(int iterationNumber) {
        return getValuesByIndexes(getTrainingSampleByIterationNumber(iterationNumber), getAllOutputValues());
    }

    public double[][] getTestSimpleInputValues(int iterationNumber) {
        BitSet res = getTrainingSampleByIterationNumber(iterationNumber);
        res.flip(0, size());
        return getValuesByIndexes(res, getAllInputValues());
    }

    public double[][] getTestSimpleOutputValues(int iterationNumber) {
        BitSet res = getTrainingSampleByIterationNumber(iterationNumber);
        res.flip(0, size());
        return getValuesByIndexes(res, getAllOutputValues());
    }

    @Override
    public boolean add(Point newPoint) {
        boolean flag = super.add(newPoint);
        if (flag) trainingSamples.clear();
        return flag;
    }

    @Override
    public boolean remove(Object pointToRemove) {
        boolean flag = super.remove(pointToRemove);
        if (flag) trainingSamples.clear();
        return flag;
    }

    @Override
    public void clear() {
        super.clear();
        trainingSamples.clear();
    }

    private BitSet generateNextRandomIndexes(List<Integer> allowedIndexesOfCurrentBitSet, BitSet currentBitSet, int length) {
        if (currentBitSet.cardinality() == length) {
            return trainingSamples.contains(currentBitSet) ? null : currentBitSet;
        }

        while (allowedIndexesOfCurrentBitSet.size() > 0) {
            int nextIndex = allowedIndexesOfCurrentBitSet.remove((int) (Math.random() * (allowedIndexesOfCurrentBitSet.size() - 1)));
            currentBitSet.set(nextIndex);
            BitSet testBitSet = generateNextRandomIndexes(allowedIndexesOfCurrentBitSet, currentBitSet, length);
            if (testBitSet != null) {
                return testBitSet;
            } else {
                currentBitSet.clear(nextIndex);
            }
        }
        return null;
    }

//    /**
//     * Это можно заюзать, если важны не только элементы, но и их последовательность
//     */
//    private static int getRandomIndex(long sizeOrSourceList, Set<Integer> usedIndexes) {
//        int randomIndex = (int) (Math.random() * (sizeOrSourceList - usedIndexes.size() - 1));
//        int currentIndex = -1;
//        for (int i = 0; i < sizeOrSourceList; i++) {
//            if (!usedIndexes.contains(i)) currentIndex++;
//            if (currentIndex == randomIndex) return i;
//        }
//        throw new HalushkoRuntimeException("Вот не должно было сюда дойти");
//    }
}
