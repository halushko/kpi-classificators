package halushko.classifier.trainingSet;

import halushko.classifier.Point;
import halushko.classifier.TrainingSet;
import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.*;

public class EvenTrainingSet extends TrainingSet {
    List<int[]> generatedColumnOrder = new ArrayList<>();
    private List<int[]> allCombinations = new ArrayList<>();

    public EvenTrainingSet(String[] inputColumnsNames, String[] outputColumnsNames) {
        super(inputColumnsNames, outputColumnsNames);
    }

    @Override
    public int generateNextTrainingSample() {
        if (allCombinations.size() == 0) {
            if (generatedColumnOrder.size() == 0) {
                Set<Integer> unusedColumnNumbers = new HashSet<>();
                for (int i = 0; i < getInputColumnsNames().length + getOutputColumnsNames().length; i++) {
                    unusedColumnNumbers.add(i);
                }
                generateAllCombinations(unusedColumnNumbers, new int[0]);
            } else {
                return -1;
            }
        }
        generatedColumnOrder.add(allCombinations.remove((int) (Math.random() * (allCombinations.size() - 1))));
        return generatedColumnOrder.size() - 1;
    }

    private void generateAllCombinations(Set<Integer> unusedIndexes, int[] result) {
        if (unusedIndexes.size() == 0) {
            allCombinations.add(result);
        } else {
            for (int unusedIndex : unusedIndexes) {
                int[] newResult = new int[result.length + 1];
                Set<Integer> newUnusedIndexes = new HashSet<>();
                newUnusedIndexes.addAll(unusedIndexes);
                newUnusedIndexes.remove(unusedIndex);
                System.arraycopy(result, 0, newResult, 0, result.length);
                newResult[result.length] = unusedIndex;
                generateAllCombinations(newUnusedIndexes, newResult);
            }
        }
    }

    @Override
    public boolean add(Point newPoint) {
        boolean flag = super.add(newPoint);
        if (flag) {
            allCombinations.clear();
            generatedColumnOrder.clear();
        }
        return flag;
    }

    @Override
    public boolean remove(Object pointToRemove) {
        boolean flag = super.remove(pointToRemove);
        if (flag) {
            allCombinations.clear();
            generatedColumnOrder.clear();
        }
        return flag;
    }

    @Override
    public void clear() {
        super.clear();
        allCombinations.clear();
        generatedColumnOrder.clear();
    }

    @Override
    protected BitSet getTrainingSampleByIterationNumber(int iterationNumber) {
        BitSet result = new BitSet();
        for (int i = 0; i < size(); i += 2) {
            result.set(i);
        }
        return result;
    }

    @Override
    public double[][] getTrainingSimpleInputValues(int iterationNumber) {
        return getValuesByIndexes(getTrainingSampleByIterationNumber(-1), getOrderedPoints(getColumnOrderByIndex(iterationNumber), true));
    }

    @Override
    public double[][] getTrainingSimpleOutputValues(int iterationNumber) {
        return getValuesByIndexes(getTrainingSampleByIterationNumber(-1), getOrderedPoints(getColumnOrderByIndex(iterationNumber), false));
    }

    @Override
    public double[][] getTestSimpleInputValues(int iterationNumber) {
        BitSet res = getTrainingSampleByIterationNumber(iterationNumber);
        res.flip(0, size());
        return getValuesByIndexes(res, getOrderedPoints(getColumnOrderByIndex(iterationNumber), true));

    }

    @Override
    public double[][] getTestSimpleOutputValues(int iterationNumber) {
        BitSet res = getTrainingSampleByIterationNumber(iterationNumber);
        res.flip(0, size());
        return getValuesByIndexes(res, getOrderedPoints(getColumnOrderByIndex(iterationNumber), false));
    }

    double[][] getOrderedPoints(int[] columnOrder, Boolean isInput) {
        double[][] array = getAllValues();
        Arrays.sort(array, (o1, o2) -> {
            for (int columnNumber : columnOrder) {
                int a = Double.compare(o1[columnNumber], o2[columnNumber]);
                if (a != 0) return a;
            }
            return 0;
        });
        int length = isInput == null ? getInputColumnsNames().length + getOutputColumnsNames().length : (isInput ? getInputColumnsNames().length : getOutputColumnsNames().length);
        double[][] result = new double[array.length][length];
        for (int i = 0; i < array.length; i++) {
            System.arraycopy(array[i], isInput == null || isInput ? 0 : getInputColumnsNames().length, result[i], 0, length);
        }
        return result;
    }

    int[] getColumnOrderByIndex(int iterationNumber) {
        if (generatedColumnOrder.size() > iterationNumber && iterationNumber >= 0) {
            return generatedColumnOrder.get(iterationNumber);
        } else {
            throw new HalushkoRuntimeException("Итерации с таким номером не существует");
        }
    }
}
