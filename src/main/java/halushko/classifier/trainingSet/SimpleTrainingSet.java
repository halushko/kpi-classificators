package halushko.classifier.trainingSet;

import halushko.classifier.TrainingSet;
import halushko.classifier.exceptions.HalushkoRuntimeException;

import java.util.BitSet;

public class SimpleTrainingSet extends TrainingSet {
    private boolean iterationGenerated = false;
    public SimpleTrainingSet(String[] inputColumnsNames, String[] outputColumnsNames) {
        super(inputColumnsNames, outputColumnsNames);
    }

    @Override
    public int generateNextTrainingSample() {
        if(iterationGenerated) {
            return -1;
        }
        else {
            iterationGenerated = true;
            return 0;
        }
    }

    @Override
    protected BitSet getTrainingSampleByIterationNumber(int iterationNumber) {
        if(iterationNumber == 0 && iterationGenerated){
            BitSet bs = new BitSet();
            for(int i = 0; i < size(); i++){
                bs.set(i);
            }
            return bs;
        } else {
            throw new HalushkoRuntimeException("Сюда не должно заходить");
        }
    }

    @Override
    public double[][] getTrainingSimpleInputValues(int iterationNumber) {
        return getAllInputValues();
    }

    @Override
    public double[][] getTrainingSimpleOutputValues(int iterationNumber) {
        return getAllOutputValues();
    }

    @Override
    public double[][] getTestSimpleInputValues(int iterationNumber) {
        return new double[0][];
    }

    @Override
    public double[][] getTestSimpleOutputValues(int iterationNumber) {
        return new double[0][];
    }
}
