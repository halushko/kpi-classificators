package halushko.classifier;

import halushko.classifier.exceptions.HalushkoRuntimeException;
import halushko.classifier.trainingSet.EvenTrainingSet;
import halushko.classifier.trainingSet.RandomTrainingSet;
import halushko.classifier.trainingSet.SimpleTrainingSet;

@SuppressWarnings("WeakerAccess")
public abstract class ModelBuilder<ModelType extends Model> {
    protected abstract ModelType buildModel(double[][] input, double[][] output);

    public ModelType buildModel(TrainingSet trainingSet) {
        if (trainingSet == null) {
            throw new HalushkoRuntimeException("Вот нельзя строить модели по пустому множеству. Зачем посылать сюда NULL?");
        }
        NormalizingCoefficientsPoints normalizingCoefficients = trainingSet.getNormalizingCoefficients();
        ModelType result = null;
        double resultAdequacy = Double.MIN_VALUE;
        for (int i = trainingSet.generateNextTrainingSample(); i >= 0; i = trainingSet.generateNextTrainingSample()) {
            double[][] in = trainingSet.getTrainingSimpleInputValues(i);
            double[][] out = trainingSet.getTrainingSimpleOutputValues(i);

            for (int row = 0; row < in.length; row++) {
                for (int col = 0; col < in[0].length; col++) {
                    in[row][col] = (in[row][col] - normalizingCoefficients.pointMin.getInput()[col])
                            / (normalizingCoefficients.pointMax.getInput()[col] - normalizingCoefficients.pointMin.getInput()[col]);
                }
                for (int col = 0; col < out[0].length; col++) {
                    out[row][col] = (out[row][col] - normalizingCoefficients.pointMin.getOutput()[col])
                            / (normalizingCoefficients.pointMax.getOutput()[col] - normalizingCoefficients.pointMin.getOutput()[col]);
                }
            }

            ModelType model = buildModel(in, out);
            model.setNormalizingCoefficientsMin(normalizingCoefficients.pointMin);
            model.setNormalizingCoefficientsMax(normalizingCoefficients.pointMax);

            double modelAdequacy = adequacy(model, trainingSet.getTestSimpleInputValues(i), trainingSet.getTestSimpleOutputValues(i));
            System.out.println(i + " " + modelAdequacy + " vs " + resultAdequacy);
            if (result == null) {
                result = model;
                resultAdequacy = modelAdequacy;
            } else {
                if (modelAdequacy > resultAdequacy) {
                    result = model;
                    resultAdequacy = modelAdequacy;
                }
            }
        }

        return result;
    }
    public ModelType buildModel(TrainingSet trainingSet, Class<? extends TrainingSet> trainingSetClass){
        if(trainingSetClass == null){
            throw new HalushkoRuntimeException("trainingSetClass не может быть NULL");
        }
        TrainingSet newTrainingSet;
        if(trainingSetClass.equals(RandomTrainingSet.class)){
            newTrainingSet = new RandomTrainingSet(trainingSet.getInputColumnsNames(), trainingSet.getOutputColumnsNames());

        } else if(trainingSetClass.equals(EvenTrainingSet.class)){
            newTrainingSet = new EvenTrainingSet(trainingSet.getInputColumnsNames(), trainingSet.getOutputColumnsNames());
        } else if(trainingSetClass.equals(SimpleTrainingSet.class)){
            newTrainingSet = new SimpleTrainingSet(trainingSet.getInputColumnsNames(), trainingSet.getOutputColumnsNames());
        }else {
            throw new HalushkoRuntimeException("trainingSetClass должен быть типа TrainingSet, а он - " + trainingSetClass.toString());
        }

        newTrainingSet.addAll(trainingSet);
        return buildModel(newTrainingSet);
    }

    /**
     * Gets adequacy for model for current indexes
     */
    public double adequacy(ModelType model, double[][] inputTest, double[][] outputTest) {
        if (inputTest == null || outputTest == null || inputTest.length != outputTest.length) {
            throw new HalushkoRuntimeException("Для того, чтобы проверить модель, нужны параметры одной длинны. И они должны быть не пустыми");
        }
        double res = 0;
        for (int i = 0; i < inputTest.length; i++) {
            double a = model.calculateValue(inputTest[i]);
            res += Math.abs(cutDoubleValue(a) - cutDoubleValue(outputTest[i][0]));
        }

        return cutDoubleValue(1.0 - res / inputTest.length);
    }

    protected static double cutDoubleValue(double value) {
        return Math.rint(100.0 * value) / 100.00;
    }
}
