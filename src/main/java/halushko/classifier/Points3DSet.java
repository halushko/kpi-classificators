package halushko.classifier;

import java.util.*;

public class Points3DSet extends HashSet<Point3D> {
    public void add(double X, double Y, double Z, double value) {
        add(new Point3D(X, Y, Z, value));
    }

    public void add(String X, String Y, String Z, String value) {
        add(new Point3D(Double.valueOf(X), Double.valueOf(Z), Double.valueOf(Y), Double.valueOf(value)));
    }

    public Double getMaxX(){
        return getExtremum(0, 1);
    }
    public Double getMaxY(){
        return getExtremum(1, 1);
    }
    public Double getMaxZ(){
        return getExtremum(2, 1);
    }
    public Double getMaxValue(){
        return getExtremum(3, 1);
    }

    public Double getMinX(){
        return getExtremum(0, -1);
    }
    public Double getMinY(){
        return getExtremum(1, -1);
    }
    public Double getMinZ(){
        return getExtremum(2, -1);
    }
    public Double getMinValue(){
        return getExtremum(3, -1);
    }



    /**
     * @param compare if 1 then MAX, if -1 then MIN
     */
    private Double getExtremum(int type, int compare) {
        Double result = null;

        for (Point3D a : this) {
            double value = type == 0 ? a.X : (type == 1 ? a.Y : (type == 2 ? a.Z : a.value));
            if (result == null) result = value;
            else if (result.compareTo(value) == -compare) result = value;
        }

        return result;
    }

    public void rotate() {

    }
}
