package halushko.classifier;

import halushko.classifier.exceptions.HalushkoRuntimeException;

public abstract class Model {
    protected Point normalizingCoefficientsMin;
    protected Point normalizingCoefficientsMax;
    private double adequacy;

    protected abstract double calculateValueForNormalizedInputPoint(double[] input);
    protected abstract boolean isModelEqual(Model model);
    protected abstract int getHashCode();

    public double calculateValue(double[] input) {
        if (input == null || input.length != normalizingCoefficientsMin.getInputCount()) {
            throw new HalushkoRuntimeException("Не совпадают длинны массивов");
        }

        return normalizingCoefficientsMin.getOutput()[0] +
                calculateValueForNormalizedInputPoint(normalizePoint(input)) *
                        (normalizingCoefficientsMax.getOutput()[0] - normalizingCoefficientsMin.getOutput()[0]);
    }

    protected double[] normalizePoint(double[] points) {
        double[] result = new double[points.length];
        for (int i = 0; i < points.length; i++) {
            result[i] = (points[i] - normalizingCoefficientsMin.getInput()[i]) /
                    (normalizingCoefficientsMax.getInput()[i] - normalizingCoefficientsMin.getInput()[i]);
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Model
                && this.normalizingCoefficientsMin.equals(((Model) obj).normalizingCoefficientsMin)
                && this.normalizingCoefficientsMax.equals(((Model) obj).normalizingCoefficientsMax)
                && this.isModelEqual((Model) obj);
    }

    @Override
    public int hashCode() {
        return this.getHashCode();
    }

    public double getAdequacy() {
        return adequacy;
    }

    public void setAdequacy(double adequacy) {
        this.adequacy = adequacy;
    }

    void setNormalizingCoefficientsMin(Point normalizingCoefficientsMin) {
        this.normalizingCoefficientsMin = normalizingCoefficientsMin;
    }
    void setNormalizingCoefficientsMax(Point normalizingCoefficientsMax) {
        this.normalizingCoefficientsMax = normalizingCoefficientsMax;
    }


}
