library(RSNNS)
library(devtools)
library(clusterGeneration)
library(nnet)
library(NeuralNetTools)
library(scatterplot3d)
source_url('https://gist.githubusercontent.com/fawda123/6860630/raw/b8bf4a6c88d6b392b1bfa6ef24759ae98f31877c/lek_fun.mlp')
source_url('https://gist.githubusercontent.com/fawda123/7471137/raw/466c1474d0a505ff044412703516c34f1a4684a5/nnet_plot_update.mlp')

#$generate input vectors$#
df = data.frame(#$generated vector names$#)

#shuffle the vector
df <- df[sample(1:nrow(df),length(1:nrow(df))),1:ncol(df)]
dfValues <- df[,1:#$count of input columns$#]
dfTargets <- decodeClassLabels(df[,#$count of input columns$#+1])
#dfTargets <- decodeClassLabels(df[,5], valTrue=0.9, valFalse=0.1)
df <- splitForTrainingAndTest(dfValues, dfTargets, ratio=0.15)
#df <- normTrainingAndTestSet(df)
#$model name$# <- mlp(df$inputsTrain, df$targetsTrain, size=5, learnFuncParams=c(1), maxit=5000, inputsTest=df$inputsTest, targetsTest=df$targetsTest)

#summary(#$model name$#)
##$model name$#
#weightMatrix(#$model name$#)
#extractNetInfo(#$model name$#)
#par(mfrow=c(2,2))
#plotIterativeError(#$model name$#)
#predictions <- predict(#$model name$#,df$inputsTest)
#plotRegressionError(predictions[,2], df$targetsTest[,2])
## plotRegressionError(df$inputsTrain[,2], df$targetsTrain[,2])
#
#confusionMatrix(df$targetsTrain,fitted.values(#$model name$#))
#confusionMatrix(df$targetsTest,predictions)
#plotROC(fitted.values(#$model name$#)[,2], df$targetsTrain[,2])
#plotROC(predictions[,2], df$targetsTest[,2])
##confusion matrix with 402040-method
#confusionMatrix(df$targetsTrain, encodeClassLabels(fitted.values(#$model name$#), method="402040", l=0.4, h=0.6))
#
#scatterplot3d(df$targetsTest, pch=encodeClassLabels(predictions))
#
#
#plot.nnet(#$model name$#)
#lek.fun(#$model name$#,exp.in=df$inputsTest)
#neuralweights(#$model name$#)