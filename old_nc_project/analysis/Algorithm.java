package com.netcracker.analysis;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;
import java.util.List;

/**
 *
 */
public interface Algorithm {
    Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter);
    List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, AbstractSamplingGetter samplingGetter);

    Model buildModel(LearningParameters params, AbstractSamplingGetter samplingGetter);
}
