package com.netcracker.analysis;

import com.netcracker.analysis.algorithms.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AlgorithmLister {
    static{
        //LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log","org.apache.commons.logging.impl.Jdk14Logger");
        Object attr = LogFactory.getFactory().getAttribute("org.apache.commons.logging.Log");
        int i = 8;
    }

    private static AlgorithmLister ourInstance = new AlgorithmLister();

    Log log = LogFactory.getLog(getClass());

    public static AlgorithmLister getInstance() {

        return ourInstance;
    }

    private AlgorithmLister() {
    }

    public List<Algorithm> getAll() {
            return (List) Collections.unmodifiableList(
                    Arrays.asList(
                            //new LinearRegressionAlgorithm(),
                            new LogisticLinearRegressionAlgorithm(),
                            new NonLinearRegressionAlgorithm(2),
                            new BayesAlgorithm(),
                            new DecisionTreeLinearAlgorithm(),
                            new DecisionTreePotentialAlgorithm(),
                            new PotentialFunctionAlgorithm()
                    )
            );
    }

    public Algorithm getAlgorithm(String algName) {
        for (Algorithm alg : getAll()){
            if (alg.getClass().getSimpleName().equals(algName)) {
                return alg;
            }
        }
        return null;
    }

    class DecisionTreeLinearAlgorithm extends DecisionTreeAlgorithm{
        public DecisionTreeLinearAlgorithm(){
            super(new LogisticLinearRegressionAlgorithm());
        }
    }
    class DecisionTreePotentialAlgorithm extends DecisionTreeAlgorithm{
        public DecisionTreePotentialAlgorithm(){
            super(new PotentialFunctionAlgorithm());
        }
    }
}
