package com.netcracker.analysis.exceptions;

import com.netcracker.analysis.dao.SaveException;
import org.apache.commons.logging.Log;

public class ExceptionFactory {

    public static final String MODEL_DESERIALIZATION_FORMAT = "serializedData for %s %s must have the following format 'd|d|d||p|p|p'";
    public static final String MODEL_DESERIALIZATION_WRONG_DOUBLE = "serializedData for %s %s contains unparseable double value = %s";
    public static final String MODEL_DESERIALIZATION_WRONG_BIGINT = "serializedData for %s %s contains unparseable BigInteger value = %s";

    public static final String MODEL_SAVE_UPDATE = "No rows were updated for model %s [%s]";
    public static final String MODEL_SAVE_INSERT = "No rows were inserted for model %s [%s]";


    private static String formatString(String format, Object[] params) {
        return params == null ? format : String.format(format, params);
    }

    public static IllegalArgumentException IllegalArgument (String format, Object... params){
        String s = String.format(format, params);
        return new IllegalArgumentException(s);
    }

    public static IllegalArgumentException IllegalArgument(Log log, String format, Object... params){
        String s = formatString(format, params);

        log.error(s);
        return new IllegalArgumentException(s);
    }


    public static SaveException SaveException(Log log, String format, Object[] params) {
        String s = formatString(format, params);

        log.error(s);
        return new SaveException(s);
    }
}
