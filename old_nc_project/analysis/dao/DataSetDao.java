package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;

import java.math.BigInteger;
import java.util.List;

public interface DataSetDao extends GenericDao<DataSetHeader>{
    List<DataSetParameter> getParametersList(BigInteger data_set_id);
}
