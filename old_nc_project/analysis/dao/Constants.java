package com.netcracker.analysis.dao;

import com.netcracker.analysis.models.*;

import java.math.BigInteger;
import java.util.HashMap;

public interface Constants {

    static final String DATA_SETS_TABLE_NAME = "nc_analysis_data_sets";
    static final String OBJECTS_TABLE_NAME = "nc_analysis_objects";
    static final String DATA_SETS_TT_TABLE_NAME = "nc_analysis_data_sets_tt";

    static final BigInteger DATA_SET_OT = BigInteger.valueOf(13L);
    static final BigInteger MODEL_CLASS = BigInteger.valueOf(14L);

    static final BigInteger PARAMETER_CLASS = BigInteger.valueOf(15L);
    static final BigInteger DATA_ROW_CLASS = BigInteger.valueOf(16L);
    static final BigInteger HIST_ROW_OT = BigInteger.valueOf(17L);     // Monitoring data
    static final BigInteger LEARN_ROW_OT = BigInteger.valueOf(18L);    // Learning data

    static final BigInteger REAL_TROUBLE_TICKET_OT = BigInteger.valueOf(101L);

    public static final BigInteger NONLINEAR_MODEL_OT = BigInteger.valueOf(12302L);
    public static final BigInteger LOGISTIC_MODEL_OT = BigInteger.valueOf(12303L);
    public static final BigInteger BAYES_MODEL_OT = BigInteger.valueOf(12304L);
    public static final BigInteger POTENTIAL_FUNCTION_MODEL_OT = BigInteger.valueOf(12305L);
    public static final BigInteger DECISION_TREE_MODEL_OT = BigInteger.valueOf(12306L);

    static HashMap<BigInteger, Class> classes = new HashMap<BigInteger, Class>(){{
        put (NONLINEAR_MODEL_OT, NonLinearRegressionModel.class);
        put (LOGISTIC_MODEL_OT, LogisticLinearRegressionModel.class);
        put (BAYES_MODEL_OT, BayesModel.class);
        put (POTENTIAL_FUNCTION_MODEL_OT, PotentialFunctionModel.class);
        put (DECISION_TREE_MODEL_OT, DecisionTreeModel.class);
    }};


    String INSERT_OBJECT =
            "INSERT INTO " + OBJECTS_TABLE_NAME +
                    "(OBJECT_ID, PARENT_ID, OBJECT_TYPE_ID, OBJECT_CLASS_ID, Name, Description, created_when) VALUES" +
                    "(?, ?, ?, ?, ?, ?, ?)";
}
