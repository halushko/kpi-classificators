package com.netcracker.analysis.dao;

public class DaoFactory {
    public static final DataSetDao DATA_SET_DAO = new DataSetDaoImpl();
    public static final DataRowDao DATA_ROW_DAO = new DataRowDaoImpl();
    public static final ModelDao MODEL_DAO = new ModelDaoImpl();
    public static final RealTTDao REAL_TT_DAO = new RealTTDaoImpl();
}
