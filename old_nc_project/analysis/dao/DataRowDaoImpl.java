package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.*;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.PreparedStatementHandler;
import com.netcracker.framework.jdbc.ResultSetHandler;
import com.netcracker.framework.jdbc.oracle.JDBCType;
import oracle.sql.TIMESTAMP;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataRowDaoImpl implements DataRowDao, Constants {

    JDBCTemplates jdbc = Util.jdbc();
    protected static Log log = LogFactory.getLog("DataRowDao");

    private static class DataRowResultSetHandler implements ResultSetHandler {
        private DataSetHeader data_set;

        public DataRowResultSetHandler(DataSetHeader data_set) {

            this.data_set = data_set;
        }

        public List<DataRow> onResultSet(ResultSet resultSet) throws SQLException {

          BigInteger current_row_number = BigInteger.ZERO;
          DataRow cur_data_row = null;
          final List<DataRow> l = new ArrayList<DataRow>();

          while (resultSet.next()) {
                BigInteger id_parameter = BigInteger.valueOf(resultSet.getLong("id_parameter"));
                BigInteger row_number = BigInteger.valueOf(resultSet.getLong("object_id"));

              double val = resultSet.getDouble("val");

              if (row_number.compareTo(current_row_number)!=0) {
                  Date dat = resultSet.getTimestamp("CREATED_WHEN");
                  BigInteger type_id = BigInteger.valueOf(resultSet.getLong("object_type_id"));
                  String name = resultSet.getString("name");
                  String description = resultSet.getString("description");

                  if (type_id.equals(LEARN_ROW_OT)){
                      cur_data_row = new LearningRow(data_set, row_number, dat);
                  } else {
                      cur_data_row = new HistoryRow(data_set, row_number, dat);
                  }
                  cur_data_row.setName(name);
                  cur_data_row.setDescription(description);
                  l.add(cur_data_row);
                  current_row_number = row_number;
              }

              int index = data_set.indexOf(id_parameter);
              cur_data_row.set(index, val);
          }
          return l;
      }
    }

    public DataSet getMatrix(final DataSetHeader data_set) {
        final String query = "select rws.*, vls.id_parameter, vls.val " +
                "  from " + OBJECTS_TABLE_NAME + " rws, " +
                "       " + DATA_SETS_TABLE_NAME + " vls " +
                "  where rws.parent_id = ? and object_type_id = " + LEARN_ROW_OT +   // only learning data
                "       and rws.object_id = vls.row_number" +
                "  ORDER BY rws.created_when, rws.object_id";

        Object[][] par = {{JDBCType.NUMBER, data_set.getId()}};

        final List<DataRow> l = (List<DataRow>)jdbc.executeSelect(query, par, new DataRowResultSetHandler(data_set));

        return new DataSet(data_set, l);
    }

    public DataRow getLatestRow(DataSetHeader data_set) {
        final String query = "WITH  t1 AS" +
                "(select *" +
                "  from " + OBJECTS_TABLE_NAME +
                "  where parent_id = ? and object_type_id = ?" +
                "  ),  t2 AS " +
                "(SELECT * FROM t1 WHERE created_when = (select max (created_when) from t1))" +
                "SELECT t2.*, vls.id_parameter, vls.val FROM " +
                "  t2, " + DATA_SETS_TABLE_NAME + " vls " +
                "  WHERE object_id = (select max (object_id) from t2) AND object_id = vls.row_number";

        Object[][] par = {{JDBCType.NUMBER, data_set.getId()}, {JDBCType.NUMBER, HIST_ROW_OT}};

        List<DataRow> l = (List<DataRow>)jdbc.executeSelect(query, par, new DataRowResultSetHandler(data_set));

        if (l == null || l.size() == 0){
            par[1] = new Object[]{JDBCType.NUMBER, LEARN_ROW_OT};
            l = (List<DataRow>)jdbc.executeSelect(query, par, new DataRowResultSetHandler(data_set));
        }

        return l == null || l.size() == 0 ? null : l.get(0);
    }

    public List<DataRow> getRows(DataSetHeader data_set, BigInteger[] ids) {
        if (ids.length == 0)
            return new ArrayList<DataRow>();

        String ids_s = "(";
        for (BigInteger id : ids)
            ids_s += id + ", ";
        ids_s = ids_s.substring(0, ids_s.length() - 2) + ")";

        final String query = "select rws.*, vls.id_parameter, vls.val " +
                "  from " + OBJECTS_TABLE_NAME + " rws, " +
                "       " + DATA_SETS_TABLE_NAME + " vls " +
                "  where rws.parent_id = ? and object_type_id = " + LEARN_ROW_OT +   // only learning data
                "       and rws.object_id = vls.row_number and rws.object_id in  " + ids_s +
                "  ORDER BY rws.created_when, rws.object_id";

        Object[][] par = {{JDBCType.NUMBER, data_set.getId()}};

        final List<DataRow> l = (List<DataRow>)jdbc.executeSelect(query, par, new DataRowResultSetHandler(data_set));

        return l;
    }

    /**
     * only new Learning or History Rows saving allowed
     * @param dataRow
     * @throws SaveException
     */
    @Override
    public void save(DataRow dataRow) throws SaveException {

        boolean isLearningRow = dataRow instanceof LearningRow;
        DataSetHeader dataSetHeader = dataRow.getParent();

        BigInteger objectType = isLearningRow ? LEARN_ROW_OT : HIST_ROW_OT;
        String name = dataRow.getName();
        String description = dataRow.getDescription();
        Object[][] par_ins = {
                {JDBCType.NUMBER, dataRow.getId()},
                {JDBCType.NUMBER, dataSetHeader.getId()},
                {JDBCType.NUMBER, objectType},
                {JDBCType.NUMBER, DATA_ROW_CLASS},
                {JDBCType.STRING, name},
                {JDBCType.STRING, description},
                {JDBCType.TIMESTAMP, new Timestamp(dataRow.getDate().getTime())}
        };

        try{
            Boolean inserted = (Boolean) jdbc.execute(INSERT_OBJECT, par_ins, new PreparedStatementHandler() {
                public Object onPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.execute();
                    return preparedStatement.getUpdateCount() == 1;
                }
            });

            if(!inserted)
                throw ExceptionFactory.SaveException(log, "No rows were inserted for trouble ticket %s [%s]",
                        new Object[]{dataRow.getDate(), description});

            BigInteger rownum = dataRow.getId();

            List<DataSetParameter> parameterList = dataSetHeader.getParameterList();
            for (int i = 0; i< dataSetHeader.getRealParametersCount(); i++){
                insertParameterValue(dataSetHeader.getId(), parameterList.get(i).getId(), rownum, dataRow.get(i));
            }
        } catch (SaveException e)
        {
            throw e;
        }
    }

    private void insertParameterValue(BigInteger ds_id, BigInteger par_id, BigInteger rownum, double value) throws SaveException {
        final String query = "insert into  " + DATA_SETS_TABLE_NAME + " values (?, ?, ?, "+value+")";

        Object[][] par_ins = {
                {JDBCType.NUMBER, ds_id},
                {JDBCType.NUMBER, par_id},
                {JDBCType.NUMBER, rownum}
        };

        Boolean inserted = (Boolean) jdbc.execute(query, par_ins, new PreparedStatementHandler() {
            public Object onPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.execute();
                return preparedStatement.getUpdateCount() == 1;
            }
        });

        if(!inserted)
            throw ExceptionFactory.SaveException(log, "Unable to save row for trouble ticket. ds_id: %s; par_id: %s; rownum: %s; value : %s;",
                    new Object[]{ds_id, par_id, rownum, value});
    }
}
