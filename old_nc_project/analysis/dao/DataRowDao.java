package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;

import java.math.BigInteger;
import java.util.List;

public interface DataRowDao {

    DataSet getMatrix (DataSetHeader data_set);

    DataRow getLatestRow(DataSetHeader data_set);
    List<DataRow> getRows(DataSetHeader data_set, BigInteger[] ids);

    void save(DataRow dataRow) throws SaveException;
}
