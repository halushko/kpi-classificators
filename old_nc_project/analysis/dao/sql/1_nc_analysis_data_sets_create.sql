drop table nc_analysis_data_sets;

CREATE TABLE nc_analysis_data_sets (

  id_data_set 	NUMBER(20),
  id_parameter	NUMBER(20),
row_number	    NUMBER(20),
val	            FLOAT
);
