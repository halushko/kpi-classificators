DELETE FROM nc_analysis_data_sets
  WHERE id_data_set = 326215615;

DELETE FROM nc_analysis_objects
  WHERE parent_ID = 326215615 AND OBJECT_CLASS_ID != 15;

insert into nc_analysis_data_sets values
('326215615', '326255601', 336255000 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255000 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255000 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255000 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255001 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255001 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255001 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255001 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255002 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255002 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255002 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255002 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255003 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255003 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255003 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255003 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255004 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255004 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255004 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255004 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255005 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255005 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255005 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255005 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255006 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255006 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255006 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255006 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255007 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255007 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255007 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255007 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255008 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255008 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255008 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255008 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255009 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255009 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255009 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255009 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255010 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255010 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255010 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255010 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255011 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255011 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255011 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255011 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255012 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255012 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255012 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255012 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255013 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255013 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255013 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255013 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255014 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255014 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255014 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255014 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255015 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255015 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255015 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255015 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255016 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255016 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255016 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255016 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255017 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255017 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255017 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255017 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255018 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255018 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255018 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255018 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255019 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255019 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255019 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255019 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255020 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255020 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255020 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255020 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255021 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255021 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255021 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255021 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255022 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255022 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255022 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255022 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255023 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255023 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255023 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255023 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255024 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255024 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255024 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255024 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255025 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255025 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255025 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255025 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255026 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255026 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255026 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255026 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255027 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255027 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255027 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255027 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255028 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255028 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255028 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255028 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255029 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255029 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255029 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255029 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255030 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255030 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255030 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255030 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255031 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255031 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255031 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255031 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255032 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255032 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255032 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255032 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255033 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255033 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255033 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255033 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255034 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255034 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255034 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255034 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255035 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255035 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255035 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255035 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255036 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255036 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255036 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255036 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255037 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255037 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255037 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255037 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255038 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255038 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255038 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255038 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255039 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255039 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255039 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255039 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255040 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255040 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255040 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255040 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255041 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255041 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255041 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255041 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255042 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255042 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255042 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255042 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255043 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255043 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255043 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255043 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255044 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255044 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255044 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255044 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255045 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255045 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255045 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255045 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255046 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255046 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255046 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255046 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255047 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255047 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255047 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255047 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255048 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255048 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255048 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255048 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255049 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255049 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255049 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255049 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255050 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255050 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255050 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255050 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255051 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255051 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255051 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255051 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255052 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255052 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255052 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255052 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255053 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255053 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255053 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255053 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255054 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255054 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255054 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255054 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255055 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255055 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255055 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255055 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255056 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255056 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255056 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255056 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255057 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255057 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255057 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255057 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255058 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255058 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255058 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255058 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255059 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255059 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255059 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255059 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255060 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255060 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255060 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255060 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255061 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255061 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255061 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255061 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255062 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255062 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255062 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255062 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255063 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255063 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255063 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255063 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255064 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255064 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255064 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255064 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255065 ,10.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255065 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255065 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255065 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255066 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255066 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255066 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255066 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255067 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255067 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255067 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255067 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255068 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255068 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255068 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255068 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255069 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255069 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255069 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255069 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255070 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255070 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255070 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255070 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255071 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255071 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255071 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255071 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255072 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255072 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255072 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255072 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255073 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255073 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255073 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255073 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255074 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255074 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255074 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255074 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255075 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255075 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255075 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255075 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255076 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255076 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255076 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255076 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255077 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255077 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255077 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255077 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255078 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255078 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255078 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255078 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255079 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255079 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255079 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255079 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255080 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255080 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255080 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255080 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255081 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255081 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255081 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255081 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255082 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255082 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255082 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255082 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255083 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255083 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255083 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255083 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255084 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255084 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255084 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255084 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255085 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255085 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255085 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255085 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255086 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255086 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255086 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255086 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255087 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255087 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255087 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255087 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255088 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255088 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255088 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255088 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255089 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255089 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255089 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255089 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255090 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255090 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255090 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255090 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255091 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255091 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255091 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255091 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255092 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255092 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255092 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255092 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255093 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255093 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255093 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255093 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255094 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255094 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255094 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255094 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255095 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255095 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255095 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255095 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255096 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255096 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255096 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255096 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255097 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255097 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255097 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255097 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255098 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255098 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255098 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255098 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255099 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255099 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255099 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255099 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255100 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255100 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255100 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255100 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255101 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255101 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255101 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255101 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255102 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255102 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255102 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255102 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255103 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255103 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255103 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255103 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255104 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255104 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255104 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255104 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255105 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255105 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255105 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255105 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255106 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255106 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255106 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255106 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255107 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255107 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255107 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255107 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255108 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255108 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255108 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255108 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255109 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255109 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255109 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255109 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255110 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255110 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255110 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255110 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255111 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255111 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255111 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255111 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255112 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255112 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255112 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255112 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255113 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255113 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255113 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255113 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255114 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255114 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255114 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255114 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255115 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255115 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255115 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255115 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255116 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255116 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255116 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255116 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255117 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255117 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255117 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255117 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255118 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255118 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255118 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255118 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255119 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255119 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255119 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255119 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255120 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255120 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255120 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255120 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255121 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255121 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255121 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255121 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255122 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255122 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255122 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255122 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255123 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255123 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255123 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255123 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255124 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255124 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255124 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255124 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255125 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255125 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255125 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255125 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255126 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255126 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255126 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255126 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255127 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255127 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255127 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255127 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255128 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255128 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255128 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255128 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255129 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255129 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255129 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255129 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255130 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255130 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255130 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255130 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255131 ,20.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255131 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255131 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255131 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255132 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255132 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255132 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255132 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255133 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255133 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255133 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255133 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255134 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255134 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255134 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255134 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255135 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255135 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255135 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255135 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255136 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255136 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255136 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255136 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255137 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255137 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255137 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255137 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255138 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255138 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255138 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255138 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255139 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255139 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255139 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255139 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255140 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255140 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255140 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255140 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255141 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255141 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255141 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255141 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255142 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255142 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255142 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255142 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255143 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255143 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255143 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255143 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255144 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255144 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255144 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255144 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255145 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255145 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255145 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255145 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255146 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255146 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255146 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255146 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255147 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255147 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255147 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255147 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255148 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255148 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255148 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255148 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255149 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255149 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255149 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255149 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255150 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255150 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255150 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255150 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255151 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255151 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255151 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255151 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255152 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255152 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255152 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255152 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255153 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255153 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255153 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255153 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255154 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255154 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255154 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255154 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255155 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255155 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255155 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255155 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255156 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255156 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255156 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255156 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255157 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255157 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255157 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255157 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255158 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255158 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255158 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255158 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255159 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255159 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255159 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255159 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255160 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255160 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255160 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255160 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255161 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255161 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255161 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255161 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255162 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255162 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255162 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255162 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255163 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255163 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255163 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255163 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255164 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255164 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255164 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255164 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255165 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255165 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255165 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255165 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255166 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255166 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255166 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255166 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255167 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255167 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255167 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255167 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255168 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255168 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255168 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255168 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255169 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255169 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255169 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255169 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255170 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255170 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255170 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255170 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255171 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255171 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255171 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255171 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255172 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255172 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255172 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255172 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255173 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255173 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255173 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255173 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255174 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255174 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255174 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255174 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255175 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255175 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255175 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255175 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255176 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255176 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255176 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255176 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255177 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255177 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255177 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255177 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255178 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255178 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255178 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255178 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255179 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255179 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255179 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255179 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255180 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255180 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255180 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255180 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255181 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255181 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255181 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255181 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255182 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255182 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255182 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255182 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255183 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255183 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255183 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255183 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255184 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255184 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255184 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255184 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255185 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255185 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255185 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255185 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255186 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255186 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255186 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255186 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255187 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255187 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255187 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255187 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255188 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255188 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255188 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255188 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255189 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255189 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255189 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255189 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255190 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255190 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255190 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255190 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255191 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255191 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255191 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255191 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255192 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255192 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255192 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255192 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255193 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255193 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255193 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255193 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255194 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255194 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255194 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255194 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255195 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255195 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255195 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255195 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255196 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255196 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255196 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255196 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255197 ,30.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255197 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255197 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255197 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255198 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255198 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255198 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255198 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255199 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255199 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255199 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255199 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255200 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255200 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255200 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255200 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255201 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255201 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255201 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255201 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255202 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255202 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255202 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255202 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255203 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255203 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255203 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255203 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255204 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255204 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255204 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255204 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255205 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255205 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255205 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255205 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255206 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255206 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255206 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255206 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255207 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255207 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255207 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255207 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255208 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255208 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255208 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255208 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255209 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255209 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255209 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255209 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255210 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255210 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255210 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255210 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255211 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255211 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255211 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255211 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255212 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255212 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255212 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255212 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255213 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255213 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255213 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255213 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255214 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255214 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255214 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255214 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255215 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255215 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255215 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255215 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255216 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255216 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255216 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255216 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255217 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255217 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255217 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255217 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255218 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255218 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255218 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255218 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255219 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255219 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255219 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255219 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255220 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255220 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255220 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255220 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255221 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255221 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255221 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255221 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255222 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255222 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255222 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255222 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255223 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255223 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255223 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255223 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255224 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255224 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255224 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255224 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255225 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255225 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255225 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255225 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255226 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255226 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255226 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255226 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255227 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255227 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255227 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255227 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255228 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255228 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255228 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255228 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255229 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255229 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255229 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255229 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255230 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255230 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255230 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255230 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255231 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255231 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255231 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255231 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255232 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255232 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255232 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255232 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255233 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255233 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255233 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255233 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255234 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255234 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255234 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255234 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255235 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255235 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255235 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255235 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255236 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255236 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255236 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255236 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255237 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255237 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255237 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255237 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255238 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255238 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255238 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255238 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255239 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255239 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255239 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255239 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255240 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255240 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255240 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255240 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255241 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255241 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255241 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255241 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255242 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255242 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255242 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255242 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255243 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255243 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255243 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255243 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255244 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255244 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255244 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255244 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255245 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255245 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255245 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255245 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255246 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255246 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255246 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255246 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255247 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255247 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255247 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255247 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255248 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255248 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255248 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255248 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255249 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255249 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255249 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255249 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255250 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255250 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255250 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255250 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255251 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255251 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255251 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255251 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255252 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255252 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255252 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255252 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255253 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255253 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255253 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255253 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255254 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255254 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255254 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255254 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255255 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255255 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255255 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255255 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255256 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255256 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255256 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255256 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255257 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255257 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255257 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255257 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255258 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255258 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255258 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255258 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255259 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255259 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255259 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255259 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255260 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255260 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255260 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255260 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255261 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255261 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255261 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255261 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255262 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255262 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255262 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255262 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255263 ,40.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255263 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255263 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255263 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255264 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255264 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255264 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255264 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255265 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255265 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255265 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255265 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255266 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255266 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255266 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255266 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255267 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255267 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255267 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255267 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255268 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255268 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255268 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255268 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255269 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255269 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255269 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255269 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255270 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255270 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255270 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255270 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255271 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255271 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255271 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255271 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255272 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255272 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255272 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255272 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255273 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255273 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255273 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255273 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255274 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255274 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255274 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255274 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255275 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255275 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255275 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255275 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255276 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255276 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255276 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255276 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255277 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255277 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255277 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255277 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255278 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255278 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255278 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255278 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255279 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255279 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255279 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255279 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255280 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255280 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255280 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255280 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255281 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255281 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255281 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255281 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255282 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255282 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255282 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255282 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255283 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255283 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255283 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255283 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255284 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255284 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255284 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255284 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255285 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255285 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255285 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255285 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255286 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255286 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255286 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255286 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255287 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255287 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255287 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255287 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255288 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255288 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255288 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255288 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255289 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255289 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255289 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255289 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255290 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255290 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255290 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255290 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255291 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255291 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255291 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255291 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255292 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255292 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255292 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255292 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255293 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255293 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255293 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255293 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255294 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255294 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255294 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255294 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255295 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255295 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255295 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255295 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255296 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255296 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255296 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255296 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255297 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255297 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255297 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255297 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255298 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255298 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255298 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255298 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255299 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255299 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255299 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255299 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255300 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255300 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255300 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255300 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255301 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255301 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255301 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255301 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255302 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255302 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255302 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255302 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255303 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255303 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255303 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255303 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255304 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255304 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255304 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255304 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255305 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255305 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255305 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255305 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255306 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255306 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255306 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255306 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255307 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255307 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255307 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255307 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255308 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255308 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255308 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255308 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255309 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255309 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255309 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255309 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255310 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255310 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255310 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255310 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255311 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255311 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255311 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255311 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255312 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255312 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255312 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255312 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255313 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255313 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255313 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255313 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255314 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255314 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255314 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255314 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255315 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255315 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255315 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255315 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255316 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255316 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255316 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255316 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255317 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255317 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255317 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255317 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255318 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255318 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255318 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255318 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255319 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255319 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255319 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255319 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255320 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255320 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255320 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255320 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255321 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255321 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255321 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255321 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255322 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255322 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255322 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255322 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255323 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255323 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255323 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255323 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255324 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255324 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255324 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255324 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255325 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255325 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255325 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255325 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255326 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255326 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255326 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255326 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255327 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255327 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255327 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255327 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255328 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255328 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255328 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255328 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255329 ,50.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255329 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255329 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255329 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255330 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255330 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255330 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255330 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255331 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255331 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255331 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255331 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255332 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255332 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255332 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255332 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255333 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255333 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255333 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255333 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255334 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255334 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255334 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255334 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255335 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255335 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255335 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255335 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255336 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255336 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255336 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255336 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255337 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255337 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255337 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255337 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255338 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255338 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255338 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255338 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255339 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255339 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255339 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255339 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255340 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255340 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255340 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255340 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255341 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255341 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255341 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255341 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255342 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255342 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255342 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255342 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255343 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255343 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255343 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255343 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255344 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255344 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255344 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255344 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255345 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255345 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255345 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255345 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255346 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255346 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255346 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255346 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255347 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255347 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255347 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255347 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255348 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255348 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255348 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255348 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255349 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255349 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255349 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255349 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255350 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255350 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255350 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255350 ,1.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255351 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255351 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255351 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255351 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255352 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255352 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255352 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255352 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255353 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255353 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255353 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255353 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255354 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255354 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255354 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255354 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255355 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255355 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255355 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255355 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255356 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255356 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255356 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255356 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255357 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255357 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255357 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255357 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255358 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255358 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255358 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255358 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255359 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255359 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255359 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255359 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255360 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255360 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255360 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255360 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255361 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255361 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255361 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255361 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255362 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255362 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255362 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255362 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255363 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255363 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255363 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255363 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255364 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255364 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255364 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255364 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255365 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255365 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255365 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255365 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255366 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255366 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255366 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255366 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255367 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255367 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255367 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255367 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255368 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255368 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255368 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255368 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255369 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255369 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255369 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255369 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255370 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255370 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255370 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255370 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255371 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255371 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255371 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255371 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255372 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255372 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255372 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255372 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255373 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255373 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255373 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255373 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255374 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255374 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255374 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255374 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255375 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255375 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255375 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255375 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255376 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255376 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255376 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255376 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255377 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255377 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255377 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255377 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255378 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255378 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255378 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255378 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255379 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255379 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255379 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255379 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255380 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255380 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255380 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255380 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255381 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255381 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255381 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255381 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255382 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255382 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255382 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255382 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255383 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255383 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255383 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255383 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255384 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255384 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255384 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255384 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255385 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255385 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255385 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255385 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255386 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255386 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255386 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255386 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255387 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255387 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255387 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255387 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255388 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255388 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255388 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255388 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255389 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255389 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255389 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255389 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255390 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255390 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255390 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255390 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255391 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255391 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255391 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255391 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255392 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255392 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255392 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255392 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255393 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255393 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255393 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255393 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255394 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255394 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255394 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255394 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255395 ,60.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255395 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255395 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255395 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255396 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255396 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255396 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255396 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255397 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255397 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255397 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255397 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255398 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255398 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255398 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255398 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255399 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255399 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255399 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255399 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255400 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255400 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255400 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255400 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255401 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255401 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255401 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255401 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255402 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255402 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255402 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255402 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255403 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255403 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255403 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255403 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255404 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255404 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255404 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255404 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255405 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255405 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255405 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255405 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255406 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255406 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255406 ,0.01 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255406 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255407 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255407 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255407 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255407 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255408 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255408 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255408 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255408 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255409 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255409 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255409 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255409 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255410 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255410 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255410 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255410 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255411 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255411 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255411 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255411 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255412 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255412 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255412 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255412 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255413 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255413 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255413 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255413 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255414 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255414 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255414 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255414 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255415 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255415 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255415 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255415 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255416 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255416 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255416 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255416 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255417 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255417 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255417 ,0.02 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255417 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255418 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255418 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255418 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255418 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255419 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255419 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255419 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255419 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255420 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255420 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255420 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255420 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255421 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255421 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255421 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255421 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255422 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255422 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255422 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255422 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255423 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255423 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255423 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255423 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255424 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255424 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255424 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255424 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255425 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255425 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255425 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255425 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255426 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255426 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255426 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255426 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255427 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255427 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255427 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255427 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255428 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255428 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255428 ,0.03 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255428 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255429 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255429 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255429 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255429 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255430 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255430 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255430 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255430 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255431 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255431 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255431 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255431 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255432 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255432 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255432 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255432 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255433 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255433 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255433 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255433 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255434 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255434 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255434 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255434 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255435 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255435 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255435 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255435 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255436 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255436 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255436 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255436 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255437 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255437 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255437 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255437 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255438 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255438 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255438 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255438 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255439 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255439 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255439 ,0.04 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255439 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255440 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255440 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255440 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255440 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255441 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255441 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255441 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255441 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255442 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255442 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255442 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255442 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255443 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255443 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255443 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255443 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255444 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255444 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255444 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255444 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255445 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255445 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255445 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255445 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255446 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255446 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255446 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255446 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255447 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255447 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255447 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255447 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255448 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255448 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255448 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255448 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255449 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255449 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255449 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255449 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255450 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255450 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255450 ,0.06 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255450 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255451 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255451 ,100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255451 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255451 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255452 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255452 ,300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255452 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255452 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255453 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255453 ,600.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255453 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255453 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255454 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255454 ,900.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255454 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255454 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255455 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255455 ,1100.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255455 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255455 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255456 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255456 ,1200.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255456 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255456 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255457 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255457 ,1300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255457 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255457 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255458 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255458 ,1500.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255458 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255458 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255459 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255459 ,1700.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255459 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255459 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255460 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255460 ,2000.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255460 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255460 ,0.0 );
insert into nc_analysis_data_sets values
('326215615', '326255601', 336255461 ,80.0 );
insert into nc_analysis_data_sets values
('326215615', '326255600', 336255461 ,2300.0 );
insert into nc_analysis_data_sets values
('326215615', '326255602', 336255461 ,0.08 );
insert into nc_analysis_data_sets values
('326215615', '326255603', 336255461 ,0.0 );



DELETE FROM NC_ANALYSIS_OBJECTS WHERE
        object_type_id = 18
                AND
                parent_id = 326215615
;

insert INTO NC_ANALYSIS_OBJECTS(object_id, PARENT_ID, OBJECT_TYPE_ID, Name, object_class_id, CREATED_WHEN)
         select t.*, sysdate from
        (select  distinct row_number,
                ID_DATA_SET,
                18,
                '',
                16 from nc_analysis_data_sets WHERE id_data_set = 326215615
                ) t
;

--UPDATE nc_analysis_data_sets SET val = dbms_random.value(1, 1300)
--  WHERE id_parameter = 326255600
--          AND val  <= 200
--          AND row_number IN (
--            SELECT object_id
--    from nc_analysis_objects rws
--    where
--            rws.parent_id = '326215615'
--                    and object_type_id = 18
--          );
