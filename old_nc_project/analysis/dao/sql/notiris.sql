--INSERT INTO NC_ANALYSIS_OBJECTS(object_id, OBJECT_TYPE_ID, Name) VALUES
--        ('6555408963513261980', '13', 'notiris');
--
--INSERT INTO NC_ANALYSIS_OBJECTS(object_id, PARENT_ID, OBJECT_TYPE_ID, Name, object_class_id) VALUES
--        ('6555408963513261981', '6555408963513261980', '15', 'Height', '15');
--INSERT INTO NC_ANALYSIS_OBJECTS(object_id, PARENT_ID, OBJECT_TYPE_ID, Name, object_class_id) VALUES
--        ('6555408963513261982', '6555408963513261980', '15', 'Weight', '15');
--INSERT INTO NC_ANALYSIS_OBJECTS(object_id, PARENT_ID, OBJECT_TYPE_ID, Name, object_class_id) VALUES
--        ('6555408963513261983', '6555408963513261980', '15', 'FootSize', '15');
--INSERT INTO NC_ANALYSIS_OBJECTS(object_id, PARENT_ID, OBJECT_TYPE_ID, Name, object_class_id) VALUES
--        ('6555408963513261984', '6555408963513261980', '15', 'Sex', '15');
--
----            table.Rows.Add("male", 6, 180, 12);
----            table.Rows.Add("male", 5.92, 190, 11);
----            table.Rows.Add("male", 5.58, 170, 12);
----            table.Rows.Add("male", 5.92, 165, 10);
--
--            
--DELETE FROM nc_analysis_data_sets WHERE id_data_set = '6555408963513261980'

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 0, sysdate, 6);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 0, sysdate, 180);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 0, sysdate, 12);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 0, sysdate, 0);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 1, sysdate, 5.92);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 1, sysdate, 190);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 1, sysdate, 11);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 1, sysdate, 0);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 2, sysdate, 5.58);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 2, sysdate, 170);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 2, sysdate, 12);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 2, sysdate, 0);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 3, sysdate, 5.92);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 3, sysdate, 165);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 3, sysdate, 10);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 3, sysdate, 0);

--table.Rows.Add("female", 5, 100, 6);
--            table.Rows.Add("female", 5.5, 150, 8);
--            table.Rows.Add("female", 5.42, 130, 7);
--            table.Rows.Add("female", 5.75, 150, 9);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 4, sysdate, 5);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 4, sysdate, 100);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 4, sysdate, 6);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 4, sysdate, 1);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 5, sysdate, 5.5);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 5, sysdate, 150);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 5, sysdate, 8);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 5, sysdate, 1);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 6, sysdate, 5.42);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 6, sysdate, 130);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 6, sysdate, 7);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 6, sysdate, 1);

insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261981', 7, sysdate, 5.75);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261982', 7, sysdate, 150);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261983', 7, sysdate, 9);
insert into nc_analysis_data_sets values ('6555408963513261980', '6555408963513261984', 7, sysdate, 1);