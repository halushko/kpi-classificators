DROP SEQUENCE SQSYSTEM;
DROP TYPE ARRAYOFNUMBERS;


CREATE SEQUENCE SQSYSTEM
  START WITH 754155
  INCREMENT BY 1
  MAXVALUE 999999
  MINVALUE 1
  CACHE 100
  CYCLE;


CREATE OR REPLACE PACKAGE "PKGUTILS" 
IS
  TYPE ref_cur IS REF CURSOR;

  FUNCTION getid
    RETURN NUMBER;

  FUNCTION getid (p_count NUMBER)
    RETURN ref_cur;

--***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] Begin
  FUNCTION date_to_id(date_for_id DATE)
     RETURN NUMBER;

   FUNCTION id_to_date(id NUMBER)
     RETURN DATE;
--***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] End

  FUNCTION gethostid
    RETURN NUMBER;

END;
/

CREATE OR REPLACE PACKAGE BODY "PKGUTILS" 
IS

  c_count     NUMBER := 100;
  --***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] Begin
--***DECH0808 [08-18-2009][Change SWITCH_DATE for problem 2010 from January, 1 to date below] Start
    --SWITCH_DATE constant date := to_date('01012010','MMDDYYYY');
	SWITCH_DATE constant date := to_date('08012009','MMDDYYYY');
--***DECH0808 [08-18-2009][Change SWITCH_DATE for problem 2010 from January, 1 to date below] End
    ID_LENGTH constant NUMBER := 19; -- length of NetCracker ID
--***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] End

  v_host_id   NUMBER := 78;

  FUNCTION gethostid
    RETURN NUMBER
  IS
  BEGIN
 -- ���� ��� ���

    RETURN v_host_id;
  END;



--***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] Begin
--  FUNCTION getid
--    RETURN NUMBER
--  IS
--    v_sq   NUMBER;
--  BEGIN
--    SELECT /*+ncid.pl:PKGUS.getid*/ sqsystem.NEXTVAL
--      INTO v_sq
--      FROM DUAL;
--
--    RETURN    TO_CHAR (SYSDATE, 'YYMMDDSSSSS')
--           || TRIM (TO_CHAR (pkgutils.gethostid, '000'))
--           || TRIM (TO_CHAR (v_sq, '000000'));
--  END;

  FUNCTION getid
    RETURN NUMBER
  IS
    cdate date := sysdate;
    v_sq   NUMBER;
  BEGIN
    SELECT /*+ncid.pl:PKGUTILS.getid*/ sqsystem.NEXTVAL
      INTO v_sq
      FROM DUAL;

     IF cdate < SWITCH_DATE THEN
        RETURN TO_CHAR (cdate, 'YYMMDDSSSSS')
               || TRIM (TO_CHAR (pkgutils.gethostid, '000'))
               || TRIM (TO_CHAR (v_sq, '000000'));

     ELSE
         RETURN TO_CHAR(91232000000 + round((cdate-SWITCH_DATE) * 86400))
               || TRIM (TO_CHAR (pkgutils.gethostid, '00'))
               || TRIM (TO_CHAR (v_sq, '000000'));
     END IF;
  END;

  FUNCTION date_to_id(date_for_id DATE)
     RETURN NUMBER
     IS
     BEGIN

       IF date_for_id < SWITCH_DATE THEN
         RETURN  TO_CHAR (date_for_id, 'YYMMDDSSSSS') || '000000000';

       ELSE
         RETURN TO_CHAR(91232000000 + round((date_for_id - SWITCH_DATE) * 86400)) || '00000000';

       END IF;
     END;

   FUNCTION id_to_date(id NUMBER)
     RETURN DATE
    IS

    date_prefix NUMBER;
    seconds NUMBER;
    BEGIN
           IF (LENGTH(id) != ID_LENGTH) -- ID has 19 characters length
             THEN
             RETURN NULL;
           END IF;

           IF (id < 0)
            THEN
              RETURN NULL;
           END IF;

           IF (TRUNC(id) <> id)
            THEN
              RETURN NULL;
           END IF;

            date_prefix := TO_NUMBER(SUBSTR(id, 1, 5)); -- YMMDD part

            IF date_prefix >= 91232 THEN
                seconds := TO_NUMBER(SUBSTR(id, 2, 10)) - 1232000000;
                RETURN SWITCH_DATE + TRUNC(seconds/86400, 0) + mod(seconds,86400)/86400;
            ELSE
                RETURN TO_DATE('0' || SUBSTR(id, 1, 10), 'YYMMDDSSSSS');
            END IF;

      EXCEPTION WHEN OTHERS THEN
         RETURN NULL;

   END;
--***ALZA0707 [01-07-2009][Problem 2010 in ID's generation] End


--***MASU0808 [01/10/08] [NC.PSUP.FRM#8093 ] [Rerfomance problem: PKGUTILS.getid(p_count NUMBER)  ] start
FUNCTION getid (p_count NUMBER)
    RETURN ref_cur
  IS
    v_ref_cur   ref_cur;
    v_sqlstr    VARCHAR2 (10000) := '';
    v_count     NUMBER;

  BEGIN
    v_count := p_count;

    IF p_count < 1
    THEN
      v_count := 1;
    END IF;

    IF p_count > c_count
    THEN
      v_count := c_count;
    END IF;

    FOR k IN 1 .. v_count
    LOOP

      v_sqlstr := v_sqlstr || 'select /*+ncid.pl:PKGUS.getid*/ pkgutils.getid from dual ';

      IF k <> v_count
      THEN
        v_sqlstr := v_sqlstr || 'union all ';
      END IF;
    END LOOP;

    OPEN v_ref_cur FOR v_sqlstr;
    RETURN v_ref_cur;
  END;
--***MASU0808 [01/10/08] [NC.PSUP.FRM#8093 ] [Rerfomance problem: PKGUTILS.getid(p_count NUMBER)  ] end

END;
/

CREATE TYPE ARRAYOFNUMBERS AS
    TABLE OF NUMBER(20);
/