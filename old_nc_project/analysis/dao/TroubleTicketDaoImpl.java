package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.LearningRow;
import com.netcracker.analysis.data.RealTroubleTicket;
import com.netcracker.analysis.util.Util;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.ResultSetHandler;
import com.netcracker.framework.jdbc.oracle.JDBCType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TroubleTicketDaoImpl implements TroubleTicketDao, Constants {
    JDBCTemplates jdbc = Util.jdbc();
    protected static Log log = LogFactory.getLog("TroubleTicketDao");

    final String Q_TT_SELECT_FOR_DATASET =
            "select object_id, name, description " +
                    "from " + OBJECTS_TABLE_NAME +
                    " where object_type_id = ? and parent_id = ?";

    @Override
    public List getAll() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object get(BigInteger id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private class TTResultSetHandler implements ResultSetHandler {
        DataSetHeader dataset;

        public TTResultSetHandler(DataSetHeader data_set) {
            this.dataset = data_set;
        }

        public Object onResultSet(ResultSet resultSet) throws SQLException {
            List<LearningRow> l = new ArrayList<LearningRow>();
            while (resultSet.next()) {
                BigInteger id = BigInteger.valueOf(resultSet.getLong("object_id"));
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");

                if (dataset == null)
                    try{
                        BigInteger dataset_id = BigInteger.valueOf(resultSet.getLong("parent_id"));
                        dataset = DaoFactory.DATA_SET_DAO.get(dataset_id);
                    } catch (SQLException e){
                        throw new IllegalStateException("DataSet not specified for learningRow "+ name + " [" + id + "]");
                    }
//
//                LearningRow learningRow = new LearningRow(dataset, id, null, description);
//                if (learningRow != null) {
//                    l.add(learningRow);
//                }
            }
            return l;
        }
    }

    public List<RealTroubleTicket> getTTForDataSet(DataSetHeader data_set) {
        Object[][] par = {{JDBCType.NUMBER, DATA_ROW_CLASS}, {JDBCType.NUMBER, data_set.getId()}};

        List<RealTroubleTicket> l = (List<RealTroubleTicket>) jdbc.executeSelect(Q_TT_SELECT_FOR_DATASET, par,
                new TTResultSetHandler(data_set));

        return l;
    }

    @Override
    public void save(RealTroubleTicket tt) throws SaveException {
        //To change body of implemented methods use File | Settings | File Templates.
    }


}
