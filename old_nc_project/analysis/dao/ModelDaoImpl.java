package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.models.DecisionTreeModel;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.util.Util;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.PreparedStatementHandler;
import com.netcracker.framework.jdbc.ResultSetHandler;
import com.netcracker.framework.jdbc.oracle.JDBCType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.netcracker.analysis.dao.ModelFactory.createModel;
import static com.netcracker.analysis.dao.ModelFactory.getModelObjectTypeID;

public class ModelDaoImpl implements ModelDao, Constants {

    JDBCTemplates jdbc = Util.jdbc();
    protected static Log log = LogFactory.getLog("ModelDao");

    final String Q_MODEL_SELECT_FOR_DATASET =
            "select object_id, object_type_id, name, description " +
            " from " + OBJECTS_TABLE_NAME +
            " where object_class_id = ? and parent_id = ? order by object_id";

    final String Q_MODEL_SELECT_ONE =
            "select object_id, object_type_id, name, description, parent_id " +
            " from " + OBJECTS_TABLE_NAME +
            " where object_id = ?";

    final String Q_MODEL_CHECK = "select object_id from " + OBJECTS_TABLE_NAME + " where object_id = ?";

    final String Q_MODEL_INSERT =
            "INSERT INTO " + OBJECTS_TABLE_NAME +
            " (OBJECT_ID, PARENT_ID, OBJECT_TYPE_ID, OBJECT_CLASS_ID, Name, Description) VALUES" +
            " (?, ?, ?, ?, ?, ?)";

    final String Q_MODEL_UPDATE =
            "UPDATE " + OBJECTS_TABLE_NAME +
            " set Name = ?, Description = ? " +
            " where object_id = ?";

    final String Q_MODEL_DELETE  =
            "DELETE FROM " + OBJECTS_TABLE_NAME +
                    " where object_id = ?";

    private class ModelResultSetHandler implements ResultSetHandler {
        DataSetHeader dataset;

        public ModelResultSetHandler(DataSetHeader data_set) {
            this.dataset = data_set;
        }

        public Object onResultSet(ResultSet resultSet) throws SQLException {
          List<GenericModel> l = new ArrayList<GenericModel>();
          while (resultSet.next()) {
              BigInteger id = BigInteger.valueOf(resultSet.getLong("object_id"));
              BigInteger type = BigInteger.valueOf(resultSet.getLong("object_type_id"));
              String name = resultSet.getString("name");
              String description = resultSet.getString("description");

              if (dataset == null)
              try{
                  BigInteger dataset_id = BigInteger.valueOf(resultSet.getLong("parent_id"));
                  dataset = DaoFactory.DATA_SET_DAO.get(dataset_id);
              } catch (SQLException e){
                  throw new IllegalStateException("DataSet not specified for model "+ name + " [" + id + "]");
              }

              GenericModel model = createModel(type, id, dataset, description);
              if (model != null) {
                  model.setName(name);
                  l.add(model);
              }
          }
          return l;
        }
    }

    public List<GenericModel> getForDataSet(final DataSetHeader data_set) {

        Object[][] par = {{JDBCType.NUMBER, MODEL_CLASS}, {JDBCType.NUMBER, data_set.getId()}};

        List<GenericModel> l = (List<GenericModel>) jdbc.executeSelect(Q_MODEL_SELECT_FOR_DATASET, par,
                new ModelResultSetHandler(data_set));

        return l;
    }

    /**
     * Caution! Creates new DataSetHeader. Do not use if possible!
     * @param id
     * @return
     */
    public GenericModel get(BigInteger id) {
        return get(null, id);
    }

    public GenericModel get(final DataSetHeader data_set, BigInteger id) {

        Object[][] par = {{JDBCType.NUMBER, id}};

        List<GenericModel> l = (List<GenericModel>) jdbc.executeSelect(Q_MODEL_SELECT_ONE, par,
                new ModelResultSetHandler(data_set));

        return l != null && l.size() > 0 ? l.get(0) : null;
    }

    public void save(GenericModel mdl) throws SaveException {
        save(mdl, mdl.getParent().getId());
    }

    private void save(GenericModel mdl, BigInteger parentID) throws SaveException {

        Object[][] par = {{JDBCType.NUMBER, mdl.getId()}};

        Boolean model_exists = (Boolean) jdbc.executeSelect(Q_MODEL_CHECK, par,
                new ResultSetHandler() {
                    public Object onResultSet(ResultSet resultSet) throws SQLException {
                        return resultSet.next();
                    }
                });

        String serialized = mdl.serializeData();

        if (model_exists){
            Object[][] par_upd = {
                {JDBCType.STRING, mdl.getName()},
                {JDBCType.STRING, serialized},
                {JDBCType.NUMBER, mdl.getId()}
            };
            int updated = jdbc.executeUpdate(Q_MODEL_UPDATE, par_upd);
            if (updated != 1)
                throw ExceptionFactory.SaveException(log, ExceptionFactory.MODEL_SAVE_UPDATE, new Object[]{mdl.getName(), mdl.getId()});
        } else {
            Object[][] par_ins = {
                {JDBCType.NUMBER, mdl.getId()},
                {JDBCType.NUMBER, parentID},
                {JDBCType.NUMBER, getModelObjectTypeID(mdl)},
                {JDBCType.NUMBER, MODEL_CLASS},
                {JDBCType.STRING, mdl.getName()},
                {JDBCType.STRING, serialized}
            };
            Boolean inserted = (Boolean) jdbc.execute(Q_MODEL_INSERT, par_ins, new PreparedStatementHandler() {
                public Object onPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.execute();
                    return preparedStatement.getUpdateCount() == 1;
                }
            });
            if(!inserted)
                        throw ExceptionFactory.SaveException(log, ExceptionFactory.MODEL_SAVE_INSERT,
                                new Object[] {mdl.getName(), mdl.getId()});
            }

            if (mdl instanceof DecisionTreeModel){
                List<GenericModel> innerModels = ((DecisionTreeModel) mdl).getModels();
                for (GenericModel model : innerModels) {
                    save(model, mdl.getId());
                }
            }
        }

    public boolean delete(BigInteger model_id) {
        Object[][] par = {
                {JDBCType.NUMBER, model_id}
        };
        Boolean deleted = (Boolean) jdbc.execute(Q_MODEL_DELETE, par, new PreparedStatementHandler() {
            public Object onPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.execute();
                return preparedStatement.getUpdateCount() == 1;
            }
        });
        return deleted;
    }
}
