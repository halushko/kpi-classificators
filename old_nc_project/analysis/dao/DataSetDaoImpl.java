package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.util.PrimeNumbers;
import com.netcracker.analysis.util.Util;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.ResultSetHandler;
import com.netcracker.framework.jdbc.oracle.JDBCType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DataSetDaoImpl implements DataSetDao, Constants{

    JDBCTemplates jdbc = Util.jdbc();
    protected static Log log = LogFactory.getLog("DataSetDao");

    private class DataSetResultSetHandler implements ResultSetHandler{
        public Object onResultSet(ResultSet resultSet) throws SQLException {
          List<DataSetHeader> l = new ArrayList<DataSetHeader>();
          while (resultSet.next()) {
              DataSetHeader ds = new DataSetHeader(BigInteger.valueOf(resultSet.getLong("object_id")), resultSet.getString("name"));
              l.add(ds);
          }
          return l;
        }
    }

    public List<DataSetHeader> getAll() {

        final String query = "select object_id, name from " + OBJECTS_TABLE_NAME + " where object_type_id = ?";
        Object[][] par = {{JDBCType.NUMBER, DATA_SET_OT}};

        List<DataSetHeader> l = (List<DataSetHeader>) jdbc.executeSelect(query, par,
                new DataSetResultSetHandler());

        return l;
    }

    public DataSetHeader get(BigInteger id) {
        final String query = "select object_id, name from " + OBJECTS_TABLE_NAME + " where object_id = ?";
        Object[][] par = {{JDBCType.NUMBER, id}};

        List<DataSetHeader> l = (List<DataSetHeader>) jdbc.executeSelect(query, par,
                new DataSetResultSetHandler());

        return l != null && l.size() > 0 ? l.get(0) : null;
    }

    public List<DataSetParameter> getParametersList(BigInteger data_set_id) {
        final List<DataSetParameter> l = new ArrayList<DataSetParameter>();

        final String query = "select * from " + OBJECTS_TABLE_NAME + " where object_class_id = ? and parent_id = ? order by object_id";
        Object[][] par = {{JDBCType.NUMBER, PARAMETER_CLASS}, {JDBCType.NUMBER, data_set_id}};

        jdbc.executeSelect(query, par,
                new ResultSetHandler(){
                    public Object onResultSet(ResultSet resultSet) throws SQLException {
                        int count = 0;
                        while (resultSet.next()) {
                            BigInteger id_parameter = BigInteger.valueOf(resultSet.getLong("object_id"));
                            String name = resultSet.getString("name");

                            DataSetParameter dsp = new DataSetParameter(id_parameter, name, PrimeNumbers.get(count++));
                            dsp.setMapping(resultSet.getString("description"));
                            l.add(dsp);
                        }
                        return null;
                    }
                });

        return l;
    }
}
