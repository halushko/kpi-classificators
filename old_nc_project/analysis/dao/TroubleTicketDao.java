package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.RealTroubleTicket;

import java.util.List;

public interface TroubleTicketDao extends GenericDao {
    List<RealTroubleTicket> getTTForDataSet(DataSetHeader data_set);
    void save(RealTroubleTicket tt) throws SaveException;
}
