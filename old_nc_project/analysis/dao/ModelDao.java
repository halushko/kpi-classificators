package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.GenericModel;

import java.math.BigInteger;
import java.util.List;

public interface ModelDao {
    List<GenericModel> getForDataSet(DataSetHeader data_set_id);
    public GenericModel get(BigInteger id);
    void save(GenericModel mdl) throws SaveException;

    boolean delete(BigInteger model_id);

    GenericModel get(DataSetHeader dataSet, BigInteger model_id);
}
