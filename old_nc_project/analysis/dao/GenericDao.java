package com.netcracker.analysis.dao;

import java.math.BigInteger;
import java.util.List;

public interface GenericDao <T extends Object>  {
    public List<T> getAll();
    public T get(BigInteger id);
}
