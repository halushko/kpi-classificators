package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.RealTroubleTicket;
import com.netcracker.analysis.data.states.State;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.PreparedStatementHandler;
import com.netcracker.framework.jdbc.ResultSetHandler;
import com.netcracker.framework.jdbc.oracle.JDBCType;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.netcracker.analysis.dao.Constants.*;

public class RealTTDaoImpl implements RealTTDao {
    Log log = LogFactory.getLog(getClass());
    JDBCTemplates jdbc = Util.jdbc();

    @Override
    public RealTroubleTicket get(DataSetHeader dataSet, BigInteger id) {
        final String query = "select * from " + OBJECTS_TABLE_NAME + " where object_class_id = ? and object_id = ? and parent_id = ?";
        Object[][] par = {{JDBCType.NUMBER, REAL_TROUBLE_TICKET_OT}, {JDBCType.NUMBER, id}, {JDBCType.NUMBER, dataSet.getId()}};

        List<RealTroubleTicket> l = (List<RealTroubleTicket>) jdbc.executeSelect(query, par, new TTResultSetHandler(dataSet));

        return l.size()>0?l.get(0):null;
    }

    @Override
    @Deprecated
    public RealTroubleTicket get(BigInteger id) {
        final String query = "select * from " + OBJECTS_TABLE_NAME + " where object_class_id = ? and object_id = ?";
        Object[][] par = {{JDBCType.NUMBER, REAL_TROUBLE_TICKET_OT}, {JDBCType.NUMBER, id}};

        List<RealTroubleTicket> l = (List<RealTroubleTicket>) jdbc.executeSelect(query, par, new TTResultSetHandler(null /*trololo*/));

        return l.size()>0?l.get(0):null;
    }

    @Override
    public List<RealTroubleTicket> getForDataSet(DataSetHeader dataSet) {
        final String query = "select * from " + OBJECTS_TABLE_NAME + " where object_class_id = ? and parent_id = ? order by object_id";
        Object[][] par = {{JDBCType.NUMBER, REAL_TROUBLE_TICKET_OT}, {JDBCType.NUMBER, dataSet.getId()}};

        List<RealTroubleTicket> l = (List<RealTroubleTicket>) jdbc.executeSelect(query, par, new TTResultSetHandler(dataSet));

        return l;
    }

    @Override
    public void save(RealTroubleTicket troubleTicket) throws SaveException {
        String json = serialize(troubleTicket);
        String query = INSERT_OBJECT;

        Object[][] par_ins = {//(OBJECT_ID, PARENT_ID, OBJECT_TYPE_ID, OBJECT_CLASS_ID, Name, Description)
                {JDBCType.NUMBER, troubleTicket.getId()},
                {JDBCType.NUMBER, troubleTicket.getDataSet().getId()},
                {JDBCType.NUMBER, REAL_TROUBLE_TICKET_OT},
                {JDBCType.NUMBER, REAL_TROUBLE_TICKET_OT},
                {JDBCType.STRING, "TT:"+DATE_FORMAT.format(troubleTicket.getReceivedWhen())},
                {JDBCType.STRING, json},
                {JDBCType.TIMESTAMP, new Timestamp(new Date().getTime())}
        };

        Boolean inserted = (Boolean) jdbc.execute(query, par_ins, new PreparedStatementHandler() {
            public Object onPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.execute();
                return preparedStatement.getUpdateCount() == 1;
            }
        });

        if(!inserted)
            throw ExceptionFactory.SaveException(log, "Unable to save trouble ticket: %s; data_set: %s;" ,
                    new Object[]{DATE_FORMAT.format(troubleTicket.getReceivedWhen()), troubleTicket.getDataSet().getId()}
            );

    }

    private class TTResultSetHandler implements ResultSetHandler {

        private final DataSetHeader dataSet;

        public TTResultSetHandler(DataSetHeader dataSet) {
            this.dataSet = dataSet;
        }

        @Override
        public Object onResultSet(ResultSet resultSet) throws SQLException {
            List<RealTroubleTicket> lst = new ArrayList<RealTroubleTicket>();

            while (resultSet.next()) {
                RealTroubleTicket current = new RealTroubleTicket(BigInteger.valueOf(resultSet.getLong("object_id")), dataSet);
                lst.add(current);

                String json = resultSet.getString("description");

                deserialize(current, json);
            }

            return lst;
        }
    }

    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz ");

    void deserialize(RealTroubleTicket target, String jsonString) {
        JSONObject json = JSONObject.fromObject((jsonString));

        BeanMap accessMap = new BeanMap(target);
        for(Map.Entry<String,Object> prop : ((Map<String,Object>)json).entrySet()){
            String key = prop.getKey();
            Object val = prop.getValue();
            if(val instanceof JSONNull)
                val = null;

            Class type = accessMap.getType(key);
            if (Date.class.equals(type)) {
                try {
                    val = DATE_FORMAT.parse((String)val);
                } catch (ParseException e) {
                    log.error("Can't parse date for TT with ID: "+target.getId(), e);
                    val = null;
                }
            } else if (State.class.equals(type)){
                val = State.valueOf((String) val);
            }

            accessMap.put(key, val);
        }
    }

    String serialize(RealTroubleTicket tt){
        BeanMap accessMap = new BeanMap(tt);
        JSONObject serialized = new JSONObject();
        for(Map.Entry<String,Object> prop : ((Map<String,Object>)accessMap).entrySet()){
            String key = prop.getKey();
            Object value = prop.getValue();

            if ("id".equals(key) ||
                "class".equals(key) ||
                "name".equals(key) ||
                "dataSet".equals(key))
                continue;

            if (value instanceof Date)
                value= DATE_FORMAT.format(value);
            serialized.put(key, String.valueOf(value));
        }
        return serialized.toString();
    }
}
