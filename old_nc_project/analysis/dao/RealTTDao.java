package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.RealTroubleTicket;

import java.math.BigInteger;
import java.util.List;

public interface RealTTDao
{
    public RealTroubleTicket get(BigInteger id);
    RealTroubleTicket get(DataSetHeader dataSet, BigInteger id);
    public List<RealTroubleTicket> getForDataSet(DataSetHeader dataSet);

    void save(RealTroubleTicket troubleTicket) throws SaveException;
}
