package com.netcracker.analysis.dao;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.GenericModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.Map;

public class ModelFactory{
        /**
         * Creates a model of specified type
         *
         * @param type object_type_id of a model
         * @param id object_id of a model
         * @param dataset
         * @param serializedData model serialized data
         * @return null if there is no such object_type
         */
        public static GenericModel createModel(BigInteger type, BigInteger id, DataSetHeader dataset, String serializedData) {
            Exception ex = null;
            try {
                Class clas = Constants.classes.get(type);
                if (clas == null)
                    throw new IllegalArgumentException("Unknown model class for object_type_id = " + type);
                Constructor constructor = clas.getConstructor(BigInteger.class, DataSetHeader.class, String.class);
                Object model = constructor.newInstance(id, dataset, serializedData);
                return (GenericModel) model;
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException("Cannot get constructor for model with object_type_id = " + type);
            } catch (InvocationTargetException e) {
                ex = e;
            } catch (InstantiationException e) {
                ex = e;
            } catch (IllegalAccessException e) {
                ex = e;
            }
            throw new IllegalArgumentException("Cannot instantiate model with object_type_id = " + type, ex);
        }

        public static BigInteger getModelObjectTypeID(GenericModel mdl) {
            for (Map.Entry<BigInteger, Class> cl : Constants.classes.entrySet()){
                if (cl.getValue().equals(mdl.getClass()))
                    return cl.getKey();
            }
            throw new IllegalArgumentException("Unknown object_type_id for model class " + mdl.getClass());
        }
}
