package com.netcracker.analysis.util;

import java.util.Date;

public class Timer {
    private Date timer;
    private long stopTicks;
    public Timer(){
        timer = new Date();
    }

    public long stop(){
        stopTicks = getTicks();
        return stopTicks;
    }

    public long getStopTicks(){
        return stopTicks;
    }

    public long getTicks(){
        Date end = new Date();
        return end.getTime() - timer.getTime();
    }
}
