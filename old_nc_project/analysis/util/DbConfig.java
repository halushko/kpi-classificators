package com.netcracker.analysis.util;

import oracle.jdbc.pool.OracleDataSource;

import javax.sql.*;
import java.sql.SQLException;

abstract class DbConfig {

//    // local server
    public static DataSource getDataSource() throws SQLException {
        return new OracleDataSource() {{
            setURL("jdbc:oracle:thin:@wsua-01267.netcracker.com:1521:XE");
            setUser("NC");
            setPassword("NC");
        }};
    }
//
//    // alexis server
//    public static DataSource getDataSource() throws SQLException {
//        return new OracleDataSource() {{
//            setURL("jdbc:oracle:thin:@wsua-01267.netcracker.com:1521:XE");
//            setUser("ApokALEXIS");
//            setPassword("ApokALEXIS");
//        }};
//    }
//
//    //Remote server 6210
//    public static DataSource getDataSource() throws SQLException {
//        return new OracleDataSource() {{
//            setURL("jdbc:oracle:thin:@db026co.netcracker.com:1522:DBG103");
//            setUser("mbr_ndb_6210");
//            setPassword("mbr_ndb_6210");
//        }};
//    }
//    // KPI server
//    public static DataSource getDataSource() throws SQLException {
//        return new OracleDataSource() {{
//            setURL("jdbc:oracle:thin:@192.168.221.107:1521:XE");
//            setUser("NC");
//            setPassword("NC");
//        }};
//    }
//    //WINNIE-AUTS
//    public static DataSource getDataSource() throws SQLException {
//        return new OracleDataSource() {{
//            setURL("jdbc:oracle:thin:@127.0.0.1:1521:XE");
//            setUser("URS");
//            setPassword("1qaz2wsx");
//        }};
//    }
}
