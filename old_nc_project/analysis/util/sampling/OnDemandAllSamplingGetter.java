package com.netcracker.analysis.util.sampling;

public class OnDemandAllSamplingGetter extends AbstractSamplingGetter {
    public OnDemandAllSamplingGetter(int rows, int[] learn, int[] check) {
        super(rows);    //To change body of overridden methods use File | Settings | File Templates.

        setLearn_indexes(learn);
        setCheck_indexes(check);
    }

    @Override
    public SAMPLING_TYPE getType() {
        return SAMPLING_TYPE.ON_DEMAND;
    }

    @Override
    protected void GenerateSamplings() {
        return;
    }

    public OnDemandAllSamplingGetter(int n, int[] learn_indexes) {
        super(n);
        setLearn_indexes(learn_indexes);
    }

    public OnDemandAllSamplingGetter(int n) {
        super(n);
        setLearn_indexes(new int[0]);
    }
    
    @Override
    public void setLearn_indexes(int[] learn_indexes) {
        super.setLearn_indexes(learn_indexes);
        setCheck_indexes(learn_indexes);
    }
}
