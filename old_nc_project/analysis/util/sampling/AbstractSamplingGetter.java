package com.netcracker.analysis.util.sampling;


import com.netcracker.analysis.util.Matrix;
import com.netcracker.analysis.util.Util;

import java.util.*;

public abstract class AbstractSamplingGetter {

    private int[] learn_indexes, check_indexes;

    public int getCount() {
        return _count;
    }
    
    public abstract SAMPLING_TYPE getType();

    protected void setCount(int count) {
        if(count < 1)
            throw new IllegalArgumentException("Count can't be less then '1'");
        learn_indexes = null;
        check_indexes = null;
        this._count = count;
    }

    protected int _count = 1;
    

    protected AbstractSamplingGetter(int n){
        setCount(n);
    }

    public static AbstractSamplingGetter GetSamplingGetter(SAMPLING_TYPE type, int n){
        switch (type){
            case RANDOM: return new RandomSamplingGetter(n);
            case ALL: return new AllSamplingGetter(n);
            case ON_DEMAND: return new OnDemandSamplingGetter(n);
            case ON_DEMAND_ALL: return new OnDemandAllSamplingGetter(n);
            default: assert false; return null;
        }
    }

    public static AbstractSamplingGetter GetSamplingGetter(String name, int n){
        return GetSamplingGetter(SAMPLING_TYPE.get(name), n);
    }

    public int[] getLearn_indexes() {
        if(learn_indexes == null)
            GenerateSamplings();
        return Util.shuffleArray(learn_indexes);
    }

    public int[] getCheck_indexes() {
        if(check_indexes == null)
            GenerateSamplings();
        return Util.shuffleArray(check_indexes);
    }

    protected void setLearn_indexes(int[] learn_indexes) {
        this.learn_indexes = learn_indexes;
    }

    @Deprecated
    protected  void setCheck_indexes(int[] check_indexes) {
        this.check_indexes = check_indexes;
    }

    public Matrix GetElements(Matrix m, int[] learn_indexes) {
        Matrix result = new Matrix(learn_indexes.length, m.columns());

        int current_row = -1;
        for(int i: learn_indexes)
            result.setRow(++current_row, m.getRow(i));
        return result;
    }
    
    /**
     *
     * @param m
     * @param learn_indexes
     * @param flag = true - добавить столбец с единицами на начало
     * @return
     */
    public Matrix GetElements(Matrix m, int[] learn_indexes, boolean flag) {
        int count = m.columns();
        int t = 0;
        if(flag){
            count++;
            t = 1;
        }
        int current = -1;
        Matrix result = new Matrix(learn_indexes.length, count);

        for(int i: learn_indexes)
            for(int j = 0; j < count; j++)
                if(flag && j == 0 ) result.set(++current, 0, 1);
                else result.set(++current, j, m.get(i, j - t));

        return result;
    }

    /**
     * Splits existing check sampling on arrays
     * @param count_arrays - count of arrays
     * @return - ArrayList of arrays
     */
    public Collection<int[]> splitCheckSampling_Simple(int count_arrays){
        if(count_arrays < 1)
            throw new IllegalArgumentException("Count of arrays can't be less then '1'");
        if(getCheck_indexes().length < count_arrays)
            throw  new IllegalArgumentException("Count of arrays can't be more then count of elements");

        ArrayList<int[]> res = new ArrayList<int[]>();
        int a = 0, length = getCheck_indexes().length/count_arrays;
        int remainder = getCheck_indexes().length - count_arrays*length;
        for(int i = 0; i < count_arrays; i++){
            int current_length = length;
            if(remainder > 0){
                current_length++;
                remainder--;
            }
            int[] newArray = new int[current_length];
            System.arraycopy(getCheck_indexes(), a, newArray, 0, current_length);
            a += current_length;
            res.add(newArray);
        }
        
        return res;
    }


    int[] selectRandomElements(List<Integer> list, int count){
        int[] res = new int[count];
        Random random = new Random();
        for(int i = 0; i < count; i++){
            int a = list.size() > 1 ? random.nextInt(list.size()-1) : 0;
            res[i] = list.get(a);
            list.remove(a);
        }

        return res;
    }

    
    protected abstract void GenerateSamplings();
}
