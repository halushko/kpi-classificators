package com.netcracker.analysis.util.sampling;

public enum SAMPLING_TYPE{
    RANDOM, ALL, ON_DEMAND, ON_DEMAND_ALL;
    
    public static SAMPLING_TYPE get(String name){
        if(name.equals("RANDOM"))
            return RANDOM;
        if(name.equals("ALL"))
            return ALL;
        if(name.equals("ON_DEMAND"))
            return ON_DEMAND;
        if(name.equals("ON_DEMAND_ALL"))
            return ON_DEMAND_ALL;
        assert false;
        return null;
    }
}
