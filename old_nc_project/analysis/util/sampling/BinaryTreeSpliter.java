package com.netcracker.analysis.util.sampling;

import java.util.ArrayList;

@Deprecated
public class BinaryTreeSpliter{
    private RandomSamplingGetter getter;
    private ArrayList<int[]> indexes;

    public int count = 1;
    public int steps = 0;
    
    public ArrayList<int[]> getIndexes(){
        if(indexes == null){
            indexes = new ArrayList<int[]>();
            indexes.add(new int[]{1});        
        }
        return indexes;
    }
    
    private class _tree{
        public int[] value = null;
        public _tree left = null;
        public _tree right = null;

        public _tree(int length, int steps){
            int[] vals = new int[length];
            for(int i = 0; i < length; i++)
                vals[i] = i;
            getter = (RandomSamplingGetter) AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, length);
            generateChild(vals, steps);
        }
        private _tree(int[] vals, int steps){
            getter = (RandomSamplingGetter) AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, vals.length);
            generateChild(vals, steps);
        }

        public void generateChild(int[] vals, int steps){
            if(steps < 1){
                value = vals.clone();
                return;
            }
            getter.setCount(vals.length);
            getter.GenerateSamplings();
            int[] left_vals = getValsByIndexes(getter.getLearn_indexes(), vals).clone(),
                    rigth_vals = getValsByIndexes(getter.getLearn_indexes(), vals).clone();
            left = new _tree(left_vals, steps - 1);
            right = new _tree(rigth_vals, steps -1);
        }

        public int[] getValsByIndexes(int[] indexes, int[] vals){
            int[] res = new int[indexes.length];
            for(int i = 0; i < indexes.length; i++)
                res[i] = vals[indexes[i]];
            return res;
        }

        public ArrayList<int[]> getIndexes(){
            ArrayList<int[]> res = new ArrayList<int[]>();
            if(value!= null){
                res.add(value);
            }
            else{
                res.addAll(left.getIndexes());
                res.addAll(right.getIndexes());
            }

            return res;
        }

    }

    public ArrayList<int[]> GenerateSplitedIndexes() {
        _tree t = new _tree(count, steps);
        return indexes = t.getIndexes();
    }
}
