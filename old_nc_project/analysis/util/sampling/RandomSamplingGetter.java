package com.netcracker.analysis.util.sampling;

import java.util.ArrayList;
import java.util.List;

public class RandomSamplingGetter extends AbstractSamplingGetter {

    @Override
    public SAMPLING_TYPE getType() {
        return SAMPLING_TYPE.RANDOM;
    }

    protected RandomSamplingGetter(int n) {
        super(n);
    }

    @Override
    public void GenerateSamplings() {
        int to_learn;
        if(getCount() < 1) throw new IllegalArgumentException("Matrix is too short");
        else if(getCount() < 3) to_learn = 2;
        else to_learn = getCount() - getCount()/2;
        
        List<Integer> check = new ArrayList<Integer>();

        for(int i = 0; i < getCount(); i++)
            check.add(i);

        setLearn_indexes(selectRandomElements(check, to_learn));
        setCheck_indexes(selectRandomElements(check, check.size()));
    }
}
