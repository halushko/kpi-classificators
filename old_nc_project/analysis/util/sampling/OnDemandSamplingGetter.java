package com.netcracker.analysis.util.sampling;

import java.util.Arrays;

public class OnDemandSamplingGetter extends AbstractSamplingGetter {
    @Override
    public SAMPLING_TYPE getType() {
        return SAMPLING_TYPE.ON_DEMAND;
    }

    @Override
    protected void GenerateSamplings() {
        return;
    }

    public OnDemandSamplingGetter(int n, int[] learn_indexes) {
        super(n);
        setLearn_indexes(learn_indexes);
    }

    public OnDemandSamplingGetter(int n) {
        super(n);
        setLearn_indexes(new int[0]);
    }
    
    @Override
    public void setLearn_indexes(int[] learn_indexes) {
        super.setLearn_indexes(learn_indexes);
        int[] learn = learn_indexes.clone();
        int[]check = new int[getCount()-learn.length];
        for (int i = 0, c = 0; i<getCount(); i++){
            Arrays.sort(learn);
            if (Arrays.binarySearch(learn, i)<0){
                check[c++] = i;
            }
        }
        setCheck_indexes(check);
    }
}
