package com.netcracker.analysis.util.sampling;


import java.util.ArrayList;
import java.util.List;

public class AllSamplingGetter extends AbstractSamplingGetter {


    @Override
    public SAMPLING_TYPE getType() {
        return SAMPLING_TYPE.ALL;
    }

    protected AllSamplingGetter(int n) {
        super(n);
    }

    @Override
    public void GenerateSamplings() {
        List<Integer> t = new ArrayList<Integer>();
        for(int i = 0; i < getCount(); i++)
            t.add(i);
        
        setLearn_indexes(selectRandomElements(t, getCount()));
        setCheck_indexes(getLearn_indexes().clone());
    }
}
