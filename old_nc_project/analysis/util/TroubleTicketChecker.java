package com.netcracker.analysis.util;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.AlgorithmLister;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.TestTT;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.dao.SaveException;
import com.netcracker.analysis.data.*;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class TroubleTicketChecker extends Thread {
    @Override
    public void run() {

        for(String tts: TestTT.TTs){
            try {
                sleep(0);

                ReceiveTroubleTicket(tts);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Receive Trouble Ticket as XML from the outer system
     * @param xml of the following format
     * <DataSet>ID</DataSet>
     * <Type></Type>
     * <Date></Date>
     * <Params>
     *     <Parameter>
     *         <ID></ID>
     *         <Value></Value>
     *     </Parameter>
     *            ...
     * </Params>
     * Achtung! Parameters contain a type-parameter too (good or bad ticket)
     */
    public void ReceiveTroubleTicket(String xml){
        LearningRow TT = deserializeTroubleTicket(xml);
//        XmlTroubleTicket xtt = deserializeXmlTroubleTicket(xml);

        if (true) throw new UnsupportedOperationException();

        try {
            DaoFactory.DATA_ROW_DAO.save(TT);
            //DaoFactory.TT_DAO.save(TT);
        } catch (SaveException e) {
            e.printStackTrace();
        }

        DataSetHeader dataSetHeader = TT.getParent();
        List<GenericModel> ml = dataSetHeader.getModelList();

        AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM,  dataSetHeader.getDataSet().rows());
        
        for (Algorithm a : AlgorithmLister.getInstance().getAll()){
            List<Model> models = a.rebuildModel((List) ml, dataSetHeader, samplingGetter);

            for (Model m : models){
                try {
                    GenericModel model = (GenericModel) m;
                    model.setName(model.getName()+'_');

                    DaoFactory.MODEL_DAO.save(model);
                } catch (SaveException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        dataSetHeader.refreshModelList();

//
//        List<GenericModel> models =  DaoFactory.MODEL_DAO.getForDataSet(TT.getParent());
//
//        final DataSetHeader dataset = new DataSetDaoImpl().get(BigInteger.valueOf(6555408163513261980L));
//
//        BitSet in = new BitSet(3){{
//            set(dataset.indexOf("length"));
//            set(dataset.indexOf("weigth"));
//        }};
//        BitSet out = new BitSet(3){{
//            set(dataset.indexOf("class"));
//        }};
//        for(int i = 0 ; i < models.size(); i++){
//            if(models.get(i) instanceof LogisticLinearRegressionModel) {
//                new LogisticLinearRegressionAlgorithm().buildModel(TT.getParent())
//                (LogisticLinearRegressionModel) models.get(i);
//            }
//
//        }
    }


    public void generateTroubleTickets(){

    }

    public LearningRow deserializeTroubleTicket(String xml){
        JAXBContext jc;
        XmlTroubleTicket xml_class = null;
        try {
            jc = JAXBContext.newInstance(new Class[] { XmlTroubleTicket.class});
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            xml_class = (XmlTroubleTicket)unmarshaller.unmarshal(new StringReader(xml));
            System.out.print("");
        } catch (JAXBException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String ID_xml = xml_class.dataset;

        Date date_xml = new Date();
        DataSetHeader ds = DaoFactory.DATA_SET_DAO.get(new BigInteger(ID_xml));

        throw new UnsupportedOperationException();

//        LearningRow TT = new LearningRow(ds, date_xml, xml_class.type, xml);
//        for(XmlTTParameter xml_p: xml_class.parameters){
//            BigInteger idPar = new BigInteger(xml_p.id);
//            double value = xml_p.value;
//            TT.set(ds.indexOf(idPar), value);
//        }
//        TT.xmlTroubleTicket = xml_class;
//        return TT;
    }

//    public static XmlTroubleTicket deserializeXmlTroubleTicket(String xml){
//        JAXBContext jc;
//        XmlTroubleTicket xml_class = null;
//        xml = null;
//        try {
//            jc = JAXBContext.newInstance(new Class[] { XmlTroubleTicket.class});
//            Unmarshaller unmarshaller = jc.createUnmarshaller();
//            xml_class = (XmlTroubleTicket)unmarshaller.unmarshal(new StringReader(xml));
//            System.out.print("");
//        } catch (JAXBException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//
//        return xml_class;
//    }
    public String serializeTroubleTicket(XmlTroubleTicket xml_class){

        JAXBContext jc = null;
        String res = null;
        try {
            jc = JAXBContext.newInstance(new Class[] { XmlTroubleTicket.class});
            Marshaller marshaller = jc.createMarshaller();

            StringWriter sw = new StringWriter();

            marshaller.marshal(xml_class, sw);
            res = new String(sw.toString());
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return res;
    }
}
