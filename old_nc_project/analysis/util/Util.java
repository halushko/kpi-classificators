package com.netcracker.analysis.util;

import com.netcracker.analysis.HasId;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.framework.jdbc.JDBCTemplates;
import com.netcracker.framework.jdbc.ResultSetHandler;
import oracle.jdbc.OracleResultSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Util {

    private static Log log = LogFactory.getLog(Util.class);


    public static int countFlags (BitSet bitSet) {
        int c = 0;
        for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet.nextSetBit(i+1), c++);

        return c;
    }

    public static int getNSetFlag (BitSet bitSet, int n) {
        int i = bitSet.nextSetBit(0);
        for (int j = 0; i != -1 && j<n; i = bitSet.nextSetBit(i+1), j++);

        return i;
    }

    /**
     *
     * @param s in format "1010101111010"
     * @return
     */
    public static BitSet bitsetFromString(String s){
        if (s == null)
            return null;
        BitSet b = new BitSet(s.length());
        for(int i = s.indexOf('1', 0); i!=-1; i = s.indexOf('1', i+1)){
            b.set(i);
        }
        return b;
    }

    public static Matrix getMatrix(DataSet dataset, BitSet in, int[] indexes) {
        // fill matrix
        synchronized (dataset){
            int column = Util.countFlags(in);
            Matrix A = new Matrix(indexes != null ? indexes.length : dataset.rows(), column);
            int row_count = 0;
            for (int i = 0; i<dataset.rows(); i++){
                boolean flag = true;
                if (indexes != null)
                    for(int t_row: indexes){
                        if(t_row == i){
                            flag = false;
                            break;
                        }
                    }
                if(flag) continue;
                int col_count = 0;
                for (int j = in.nextSetBit(0); j != -1; j = in.nextSetBit(j+1)) {
                    A.set(row_count, col_count++, dataset.get(i, j));
                }
                row_count++;
            }
            return A;
        }
    }

    public static JDBCTemplates jdbc(){
        return new JDBCTemplates(getDataSource());
    }


    private static DataSource dataSource;
    private static DataSource getDataSource() {

        if(dataSource == null)
        try {

                InitialContext ctx = null;
                try {
                    ctx = new InitialContext();
                    //The JDBC Data source that we just created
                    dataSource = (DataSource) ctx.lookup("jdbc/NC_Analisys");

                } catch (NamingException e) {
                    log.error("Can't connect to JNDI", e);
                }

            if(dataSource == null) // can't get from JNDI
            {
                log.error("Can't acquire DataSource from JNDI, falling back to hardcoded DbConfig");
                dataSource = DbConfig.getDataSource();
            }
        } catch (SQLException e) {
            log.error("Can't connect to DB", e);
            throw new RuntimeException(e);
        }

        return dataSource;
    }

    public static void checkNull(Object o) {
        if (o == null)
            throw new IllegalArgumentException("NULL");
    }

    public static int[] toIntArr (List<Integer> list){
        int[] arr = new int[list.size()];
        for (int i = 0; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }

    public static Integer[] toIntegerArr(int[] array){
        Integer[] tmp = new Integer[array.length];
        int i = 0;
        for (int value : array) {
            tmp[i++] = Integer.valueOf(value);
        }
        return tmp;
    }

    public static int[] shuffleArray(int[] array){
        Integer[] tmp=toIntegerArr(array);
        Collections.shuffle(Arrays.asList(tmp));
        return toIntArr(Arrays.asList(tmp));
    }

    public static double[] toDoubleArr (List<Double> list){
        double[] arr = new double[list.size()];
        for (int i = 0; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }

    /**
     * Get elements of source by their indexes
     * @param source - array of source elements
     * @param indexes - array of indexes of source elements
     * @return array of needes elements
     */
    public static int[] getIndexes(int[] source, int[] indexes){
        int[] res = new int[indexes.length];
        for(int i = 0; i < indexes.length; i++){
            res[i] = source[indexes[i]];
        }
        return res;
    }

    public static <T extends HasId> HashMap<String,T> toHashMap(List<T> list){
        HashMap<String,T> hashMap=new HashMap<String, T>();
        for (T item:list){
            hashMap.put(item.getId().toString(),item);
        }
        return hashMap;
    }

    private static Queue<BigInteger> idList = new ArrayDeque<BigInteger>(500);

    public static BigInteger getID() {
        if (idList.size()==0)
            Util.jdbc().executeSelect("SELECT PKGUTILS.getid(500) from DUAL ", new Object[0][],
                new ResultSetHandler() {
                    public Object onResultSet(ResultSet resultSet) throws SQLException {
                        while (resultSet.next()) {
                            ResultSet cursor = resultSet.unwrap(OracleResultSet.class).getCursor(1);

                            while (cursor.next()) {
                                BigDecimal oo = cursor.getBigDecimal(1);

                                idList.add(oo.toBigInteger());
                            }
                        }
                        return idList;
                    }
                });

        return idList.poll();
    }



    /**
     * Format double values
     * @return
     */
    public static String fd (double d){
        return String.format("%3.3f", d);
    }

    public static <T extends Object> T getFromSession(HttpServletRequest ctx,String name, Class<T> clazz){
        try {
            Object obj = ctx.getSession().getAttribute(name);
            if(obj==null)      {
                 obj = clazz.newInstance();
                ctx.getSession().setAttribute(name,obj);
            }
                return (T)obj;
        } catch (InstantiationException e) {
            log.error("123123123",e);
        } catch (IllegalAccessException e) {
            log.error("123123123",e);
        }

        return null;
    }
}
