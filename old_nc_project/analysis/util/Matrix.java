package com.netcracker.analysis.util;

final public class Matrix {
    private final int M;             // number of rows
    private final int N;             // number of columns
    private final double[][] data;   // M-by-N array

    /**
     * create M-by-N zero matrix
     * @param M rows
     * @param N columns
     */
    public Matrix(int M, int N) {
        this.M = M;
        this.N = N;
        data = new double[M][N];
    }

    /**
     * Identity matrix d*d
     * @param d count of columns
     * @return Identity matrix d*d
     */
    public static Matrix eye(int d){
        Matrix m = new Matrix(d, d);

        for (int i = 0; i< d; i++){
            m.data[i][i] = 1;
        }
        return m;
    }

    /**
     * @param m - number of rows
     * @param n - number of columns
     * @return  Identity matrix m*n
     */
    public static Matrix eye(int m, int n){
        Matrix result = new Matrix(m, n);

        int min = Math.min(m,n);
        for (int i = 0; i< min; i++){
            result.data[i][i] = 1;
        }
        return result;
    }

    public Matrix row(int index){
        if(index < 0 || index > M - 1)
            throw new IllegalStateException("There is no such row");
        double[][] tempRow = new double[1][M];
        for(int i = 0; i < M; i++)
            tempRow[0][i] = get(index, i);
        return new Matrix(tempRow);
    }

    public double[] getRow(int index) {
        return data[index];
    }

    public int rows(){
        return M;
    }

    public int columns(){
        return N;
    }

    /**
     *
     * @return pseudoinverse matrix of current matrix
     */
    public Matrix pinv(){
        Matrix T = this.transpose();
        return ((T.times(this)).getInverse()).times(T);
    }

    public Matrix addRow(int index, double[] row){
        Matrix result = new Matrix(this.M, this.N+1);
        for(int i = 0; i < result.columns(); i++)
            for(int j = 0; j < result.rows(); j++)
                if(j<index)
                    result.set(j,i,this.get(j,i));
                else if(j>index)
                    result.set(j,i,this.get(j-1, i));
                else
                    result.set(j, i,row[j]);
        return result;
    }

    public Matrix addColumn(int index, double[] column){
        Matrix result = new Matrix(this.M, this.N+1);
        for(int i = 0; i < result.rows(); i++)
            for(int j = 0; j < result.columns(); j++)
                if(j<index)
                    result.set(i,j,this.get(i,j));
                else if(j>index)
                    result.set(i,j,this.get(i,j-1));
                else
                    result.set(i,j,column[i]);
        return result;
    }

    public Matrix addColumnOne(int index){
        Matrix result = new Matrix(this.M, this.N+1);
        for(int i = 0; i < result.rows(); i++)
            for(int j = 0; j < result.columns(); j++)
                if(j<index)
                    result.set(i,j,this.get(i,j));
                else if(j>index)
                    result.set(i,j,this.get(i,j-1));
                else
                    result.set(i,j,1.0);
        return result;
    }

    public double get(int m, int n) {
        return data[m][n];
    }

    public void set(int m, int n, double value) {
        data[m][n] = value;
    }

    // create matrix based on 2d array
    public Matrix(double[][] data) {
        M = data.length;
        N = data[0].length;
        this.data = new double[M][N];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                this.data[i][j] = data[i][j];
    }

    // copy constructor
    public Matrix(Matrix A) {
        this(A.data);
    }

    // create and return a random M-by-N matrix with values between 0 and 1
    public static Matrix random(int M, int N) {
        Matrix A = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[i][j] = Math.random();
        return A;
    }

    // swap rows i and j
    private void swap(int i, int j) {
        double[] temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    public Matrix transpose() {
        Matrix A = new Matrix(N, M);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }

    /**
     *
     * @param B
     * @return C = A + B
     */
    public Matrix plus(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        Matrix C = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] + B.data[i][j];
        return C;
    }

    /**
     *
     * @param B
     * @return C = A - B
     */
    public Matrix minus(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        Matrix C = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] - B.data[i][j];
        return C;
    }

    // does A = B exactly?
    public boolean eq(Matrix B) {
        Matrix A = this;
        checkDimensions(B, A);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (A.data[i][j] != B.data[i][j]) return false;
        return true;
    }

    /**
     *
     * @param B
     * @return C = A * B
     */
    public Matrix times(Matrix B) {
        Matrix A = this;
        if (A.N != B.M) throw new IllegalStateException("Illegal matrix dimensions.");
        Matrix C = new Matrix(A.M, B.N);
        for (int i = 0; i < C.M; i++)
            for (int j = 0; j < C.N; j++)
                for (int k = 0; k < A.N; k++)
                    C.data[i][j] += (A.data[i][k] * B.data[k][j]);
        return C;
    }

    /**
     *
     * @param d - number
     * @return C = A * d
     */
    public Matrix mult(double d) {
        Matrix C = new Matrix(M, N);
        
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j]=data[i][j]*d;

        return C;
    }


    /**
     *  @return x = A^-1 b, assuming A is square and has full rank
     */
    public Matrix solve(Matrix rhs) {

        if (M != N || rhs.M != N || rhs.N != 1)
            throw new IllegalStateException("Illegal matrix dimensions.");

        // create copies of the data
        Matrix A = new Matrix(this);
        Matrix b = new Matrix(rhs);

        // Gaussian elimination with partial pivoting
        for (int i = 0; i < N; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < N; j++)
                if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i]))
                    max = j;
            A.swap(i, max);
            b.swap(i, max);

            // singular
            if (A.data[i][i] == 0.0) throw new IllegalStateException("Matrix is singular.");

            // pivot within b
            for (int j = i + 1; j < N; j++)
                b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];

            // pivot within A
            for (int j = i + 1; j < N; j++) {
                double m = A.data[j][i] / A.data[i][i];
                for (int k = i + 1; k < N; k++) {
                    A.data[j][k] -= A.data[i][k] * m;
                }
                A.data[j][i] = 0.0;
            }
        }

        // back substitution
        Matrix x = new Matrix(N, 1);
        for (int j = N - 1; j >= 0; j--) {
            double t = 0.0;
            for (int k = j + 1; k < N; k++)
                t += A.data[j][k] * x.data[k][0];
            x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
        }
        return x;

    }

    // print matrix to standard output
    public void print() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++)
                System.out.printf("%9.4f ", data[i][j]);
            System.out.println();
        }
    }

    public Matrix minor(int m, int n) throws Exception {
        if (m >= M || n >= N || m < 0 || n < 0) {
            return null;
        }

        Matrix result = new Matrix(M - 1, N - 1);
        int colCount = 0;
        int rowCount = 0;

        for (int i = 0; i < M; i++) {
            if (i != m) {
                colCount = 0;
                for (int j = 0; j < N; j++) {
                    if (j != n) {
                        result.set(rowCount, colCount, this.get(i, j));
                        colCount++;
                    }
                }
                rowCount++;
            }
        }
        return result;
    }

    private static void checkDimensions(Matrix B, Matrix a) {
        if (B.M != a.M || B.N != a.N) throw new IllegalStateException("Illegal matrix dimensions.");
    }

    public double[] getColumn(int colnum) {
        double[] col =  new double[M];
        for (int i = 0; i<M; i++){
            col[i] = data[i][colnum];
        }
        return col;
    }
    
    public Matrix getInverse()
    {
        double[][]mass = this.data;
        double[][] LU;
        int cnt_str = M;
        //double[][] M_obr = new double[M][M];
        int i,j,k;
        double sum;
        LU=new double[cnt_str][]; //создаём массив под матрицу LU
        for(i=0;i<cnt_str;i++)
            LU[i]=new double [cnt_str];
        for(i=0;i<cnt_str;i++)
        {
            for(j=0;j<cnt_str;j++)
            {
                sum=0;
                if(i<=j)
                {
                    for(k=0;k<i;k++)
                        sum+=LU[i][k]*LU[k][j];
                    LU[i][j]=mass[i][j]-sum;//вычисляем элементы верхней треугольной матрицы
                }
                else
                {
                    for(k=0;k<j;k++)
                        sum+=LU[i][k]*LU[k][j];
                    if(LU[j][j]==0)
                        throw new IllegalStateException("Error in inverse matrix");
                    LU[i][j]=(mass[i][j]-sum)/LU[j][j];//вычисляем элементы нижней треугольной матрицы
                }
            }
        }
        double[][] M_obr=new double [cnt_str][]; //вычисляем элементы нижней треугольной матрицы
        for(i=0;i<cnt_str;i++)
            M_obr[i]=new double [cnt_str];
        int p;
        for(i=cnt_str-1;i>=0;i--)//нахождение обратной матрицы
        {
            for(j=cnt_str-1;j>=0;j--)
            {
                sum=0;
                if(i==j)
                {
                    for(p=j+1;p<cnt_str;p++)
                        sum+=LU[j][p]*M_obr[p][j];
                    M_obr[j][j]=(1-sum)/LU[j][j];
                }
                else if(i<j)
                {
                    for(p=i+1;p<cnt_str;p++)
                        sum+=LU[i][p]*M_obr[p][j];
                    M_obr[i][j]=-sum/LU[i][i];
                }
                else
                {
                    for(p=j+1;p<cnt_str;p++)
                        sum+=M_obr[i][p]*LU[p][j];
                    M_obr[i][j]=-sum;
                }
            }
        }
        return new Matrix(M_obr);
    }

    /**
     *
     * @param x_all
     * @param learn_indexes
     * @param flag = true - добавить столбец с единицами на начало
     * @return
     */
    public static Matrix GetElements(Matrix x_all, int[] learn_indexes, boolean flag) {
        int count = x_all.columns();
        int t = 0;
        if(flag){
            count++;
            t = 1;
        }
        int current = -1;
        Matrix result = new Matrix(learn_indexes.length, count);

        for(int i: learn_indexes)
        {
            ++current;
            for(int j = 0; j < count; j++)
                if(flag && j == 0 ) result.set(current, 0, 1);
                else result.set(current, j, x_all.get(i, j - t));
        }

        return result;
    }

    public void setRow(int num, double[] row) {
        if (num < 0 || num >= rows() || row == null || row.length<columns())
            throw new IllegalArgumentException("setRow");

        for (int i = 0; i<columns(); i++){
            set(num, i, row[i]);
        }
    }
}
