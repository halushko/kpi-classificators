package com.netcracker.analysis.util;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.discarder.params.AbstractDiscarder;
import com.netcracker.analysis.discarder.params.DISCARDER_TYPE;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.estimation.estimators.ParamsEstimator;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class Print {
    public static String AAAAAAAAAAAAAA(DataSetHeader dataSetHeader, BitSet in, BitSet out, Algorithm algorithm){
        List<ModelBenchmark> benchmarks = getModelBenchmarks(dataSetHeader, in, out, algorithm, AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, dataSetHeader.getDataSet().rows()));
        List<ModelBenchmark> best = AbstractDiscarder.getBestEstimationModels(benchmarks, ESTIMATION_TYPE.ADEQUACY);

        return printModelBenchmarks(best);
    }

    public static String printModelHeaders(List<ModelBenchmark> modelBenchmarks){

        String legend = "";
        for(ModelBenchmark mb: modelBenchmarks){
            PlaneModel model = mb.getModel();
            legend += "\n\n\t" + model.toString() + (model.isInvalid() ? " invalid " : " valid ");
            for(int i= 0; i < model.getColsIn().length(); i++){
                if(model.getColsIn().get(i))
                    legend += " [" + model.getParent().getParameterList().get(i).getName() + "] ";
            }

            legend += "\n------------------------------\n";
            legend += "Model params:\n\t\t"+model.getLegend();
        }

       return legend;
    }

    public static String printModelBenchmarks(List<ModelBenchmark> modelBenchmarks){
        String legend = "";
        for(ModelBenchmark modelBenchmark: modelBenchmarks){

            legend += modelBenchmark.toString();

            PlaneModel model = modelBenchmark.getModel();
            DataSet ds = model.getParent().getDataSet();

            ParamsEstimator a = new ParamsEstimator(ds, model.getColsIn(), model.getColsOut());

            legend += "\n" + "Output Ratio:\n\t\t" + a.getOutRatio() +"\n";
            legend += "Input Ratio:\n\t\t";

            double [] b = a.getInRatio();

            for(int j = 0; j < ds.columns(); j++)
                legend += b[j] + ", ";
            legend +="\n";
        }

        return legend;
    }

    public static String BBBBBBBBBBBBBBBB(DataSetHeader dataSetHeader, BitSet in, BitSet out, Algorithm algorithm){
        return BBBBBBBBBBBBBBBB(dataSetHeader, in, out, Arrays.asList(algorithm));
    }

    public static String BBBBBBBBBBBBBBBB(DataSetHeader dataSetHeader, BitSet in, BitSet out, List<Algorithm> algorithms){
        List<ModelBenchmark> mb = getModelBenchmarks(dataSetHeader, in, out, algorithms, AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, dataSetHeader.getDataSet().rows()));
        String res = printModelBenchmarksTable(mb);

        return res;
    }

    public static List<ModelBenchmark> getModelBenchmarks(DataSetHeader dataSetHeader, BitSet in, BitSet out, Algorithm algorithm, AbstractSamplingGetter sg){
        return getModelBenchmarks(dataSetHeader, in, out, Arrays.asList(algorithm), sg);
    }

    public static List<ModelBenchmark> getModelBenchmarks(DataSetHeader dataSetHeader, BitSet in, BitSet out, List<Algorithm> algorithms, AbstractSamplingGetter sg){
        List<ModelBenchmark> res = new ArrayList<ModelBenchmark>();

        for (Algorithm algorithm : algorithms) {
            AbstractDiscarder ad = AbstractDiscarder.GetDiscarder(dataSetHeader, in, out, sg, DISCARDER_TYPE.BASE);
            ad.discard(algorithm);

            res.addAll(ad.get_values().values());
        }
        return res;
    }

    public static String printModelBenchmarksTable (List<ModelBenchmark> modelBenchmarks){
        String res = printTableHeader();
        for(ModelBenchmark value: modelBenchmarks){
            res += (value.printIntoTable()+"\n");
        }
        return res;
    }

    public static String printTableHeader() {
        String res = "Name\tParams\tLearning Time\tLearning Memory";

        Object[] ests = ESTIMATION_TYPE.values();
        Arrays.sort(ests);
        for(Object est: ests)
            res += ("\t"+ est);

        res += "\n";
        return res;
    }
}
