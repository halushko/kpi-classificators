package com.netcracker.analysis.util;


public abstract class MemoryBanchmarker<T> {
    public static final Runtime RUNTIME = Runtime.getRuntime();
    private long memUsed=-1;

    public static long memoryUsed()
    {
        return RUNTIME.totalMemory () - RUNTIME.freeMemory ();
    }
    
    public final T doBencmark(){
        synchronized (MemoryBanchmarker.class){
            RUNTIME.gc();
            long before =  memoryUsed();
            T t = run();
            long after = memoryUsed();

            memUsed = after - before;
            if(memUsed < 5120)
                memUsed = -1;
            return t;
        }
    } 
    
    protected abstract T run();

    public long getMemUsed() {
        return memUsed;
    }
}
