package com.netcracker.analysis.util;

public class Pair <T1, T2> {
    private T1 v1;
    private T2 v2;

    public Pair(T1 v1, T2 v2){

        this.v1 = v1;
        this.v2 = v2;
    }

    public T2 getV2() {
        return v2;
    }

    public void setV2(T2 v2) {
        this.v2 = v2;
    }
}
