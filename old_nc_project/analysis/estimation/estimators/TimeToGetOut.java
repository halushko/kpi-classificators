package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleComparatorMax;
import com.netcracker.analysis.models.PlaneModel;

import java.util.BitSet;
import java.util.Date;

public class TimeToGetOut extends AbstractEstimate<Double> {
    protected TimeToGetOut(DataSetHeader data, BitSet in, BitSet out, int[] check_rows) {
        super(data, in, out, check_rows);
    }

    @Override
    public Double estimate(PlaneModel model) {
        Date dateTime = new Date();
        
        DataSet dataSet = getDataSet();
        
        for(int i = 0; i < rows; i++){
            model.getOut(dataSet.getRow(i));
        }

        return ((double)(new Date().getTime()) - (double)dateTime.getTime())/rows;
    }

    @Override
    public String toString(Object estimate) {
        return toStringDouble(estimate);
    }

    @Override
    public String getName() {
        return "TimeToGetOut";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleComparatorMax();
        return comparator;
    }
}
