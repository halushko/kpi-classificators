package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleComparatorMin;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Util;

import java.util.BitSet;

public class Sensitivity extends AbstractEstimate<Double[]> {
    
    protected Sensitivity(DataSetHeader data, BitSet in, BitSet out, int[] check_rows) {
        super(data, in, out, check_rows);
    }

    /**
     * Checks model
     * 
     * @param model
     * @return array of sensitivity of each parameter.
     *          Row[0] - input parameters
     *          Row[1] - corresponding them output parameter
     */
    @Override
    public Double[] estimate(PlaneModel model) {
        Double[] res = new Double[columns];
        double[] means = new double[columns];

        for(int i = 0; i < columns; i++){
            int bi = Util.getNSetFlag(getIn(), i);
            means[i] = getMean(bi);
            res[i] = getAverageIncrease(bi);
        }

//        DataRow dr = getDataSetHeader().createHistoryRowFromArray(means, getIn(), getOut());
//
//        for(int i = 0; i < columns; i++){
//            double ymin, ymax;
//            int bi = Util.getNSetFlag(getIn(), i);
//            double avg = means[i];
//
//            dr.set(bi, getMaxInColumn(bi));
//            ymax = model.getOut(dr);
//
//            dr.set(bi, getMinInColumn(bi));
//            ymin = model.getOut(dr);
//
//            dr.set(bi, avg);
//
//            res[1][i] = getAverageIncrease(ymax, ymin);
//        }

        return res;
    }

    @Override
    public String toString(Object estimate) {
        return toStringDoubleArr(estimate);
    }

    @Override
    public String getName() {
        return "Sensitivity";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleComparatorMin();
        return comparator;
    }
}
