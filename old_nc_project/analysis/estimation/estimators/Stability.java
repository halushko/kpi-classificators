package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleComparatorMax;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;
import java.util.Collection;

public class Stability extends AbstractEstimate<Double> {
    private int _steps;

    public int getSteps() {
        return _steps;
    }
    public void setSteps(int _steps) {
        if(_steps > 2)
        this._steps = _steps;
    }


    protected Stability(DataSetHeader data, BitSet in, BitSet out, int[] check_rows) {
        super(data, in, out, check_rows);
        setSteps(8);
    }

    /**
     * Check model
     * @return for count = getSteps() of subdatasets return A = (n - Sum|Ymodel[i] - Yout[i]|)/n
     */
    @Override
    public Double estimate(PlaneModel model) {
        double res=1.0;
        int steps = getSteps();

        if(steps < 2) throw new IllegalArgumentException("Steps must be more then '1'");

        AbstractSamplingGetter sg = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, get_check_indexes().length);
        Collection<int[]> indexes = sg.splitCheckSampling_Simple(getSteps());

        for(int[] i: indexes){
            double temp = adequacy(i, model);
            if(temp < res)
                res = temp;
        }

        return res;
    }

    @Override
    public String toString(Object estimate) {
        return toStringDouble(estimate);
    }

    @Override
    public String getName() {
        return "Stability";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleComparatorMax();
        return comparator;
    }
}
