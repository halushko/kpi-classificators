package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.util.Util;

import java.util.BitSet;

public class ParamsEstimator {
    private DataSet dataSet;
    private BitSet in, out;
    public ParamsEstimator(DataSet dataSet, BitSet in, BitSet out){
        this.in = in;
        this.out = out;
        this.dataSet = dataSet;
    }
    
    public double getOutRatio(){
        double count_1 = 0.0;
        for(int i = 0; i < dataSet.rows(); i++){
            if(dataSet.get(i, out.nextSetBit(0)) > 0)
                count_1++;
        }
        return count_1/dataSet.rows();   
    }
    
    public double[] getInRatio(){
        double[] res = new double[dataSet.columns()];
        int columns = dataSet.columns();

        for(int i = 0; i < columns; i++){
            if(!in.get(i)) continue;
            double max = getMaxInColumn(i, dataSet);
            double min = getMinInColumn(i, dataSet);
            
            double count = 0.0;
            
            for(int j = 0; j < dataSet.rows(); j++){
                if(dataSet.get(j, i) > (max + min)/2)
                    count++;
            }
            
            res[i] = (double)count/dataSet.rows();
         }    
        
        return res;
    }

    /**
     * Get max value in column
     * @param index - index of column
     */
    private double getMaxInColumn(int index, DataSet data){
        int rows = data.rows();
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = data.get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = data.get(i, index);
            if(temp > res) res = temp;
        }

        return res;
    }

    /**
     * Get min value in column
     * @param index - index of column
     */
    private double getMinInColumn(int index, DataSet data){
        int rows = data.rows();
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = data.get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = data.get(i, index);
            if(temp < res) res = temp;
        }

        return res;
    }
}
