package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleArrayMinComparator;
import com.netcracker.analysis.models.PlaneModel;

import java.util.BitSet;

@Deprecated
public class APE extends AbstractEstimate<Double[]> {


    protected APE(DataSetHeader data, BitSet in, BitSet out, int[] check_indexes) {
        super(data, in, out, check_indexes);
    }

    /**
     * Return the specific estimate of model for chosen DataSet
     *
     * @param model - model to check
     * @return specific estimate of model
     */
    @Override
    public Double[] estimate(PlaneModel model) {
        Double[] errors = new Double[rows];
        for(int i = 0; i< rows; i++){
            errors[i]=absolutePercentError(i, model);
        }

        return errors;
    }

    @Override
    public String toString(Object estimate) {
        return toStringDoubleArr(estimate);
    }

    @Override
    public String getName() {
        return "APE";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleArrayMinComparator();
        return comparator;
    }
}
