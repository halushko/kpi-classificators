package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleComparatorMax;
import com.netcracker.analysis.models.PlaneModel;

import java.util.BitSet;

public class Adequacy extends AbstractEstimate<Double> {

    protected Adequacy(DataSetHeader data, BitSet in, BitSet out, int[] check_rows) {
        super(data, in, out, check_rows);
    }

    /**
     * Check model
     * @return A = (n - Sum|Ymodel[i] - Yout[i]|)/n
     */
    @Override
    public Double estimate(PlaneModel model) {
        return adequacy(get_check_indexes(), model);
    }

    @Override
    public String toString(Object estimate) {
        return toStringDouble(estimate);
    }

    @Override
    public String getName() {
        return "Adequacy";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleComparatorMax();
        return comparator;
    }
}
