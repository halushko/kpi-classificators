package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Util;

import java.util.BitSet;
import java.util.List;

public abstract class AbstractEstimate<T extends Object> {
    private DataSetHeader dataSetHeader;
    private DataSet dataSet;
    private BitSet in, out;
    private int[] _check_indexes;
    protected int columns;
    protected int rows;
    protected AbstractComparator comparator;

    /*-----------------GETTERS---------------------*/
    public DataSetHeader getDataSetHeader() {
        return dataSetHeader;
    }
    public DataSet getDataSet() {
        return dataSet;
    }
    public BitSet getOut() {
        return out;
    }
    public BitSet getIn() {
        return in;
    }
    public int[] get_check_indexes() {
        return _check_indexes;
    }
    /*----------------/GETTERS---------------------*/

    /*-----------------SETTERS---------------------*/
    public void setDataSetHeader(DataSetHeader dsh) {
        this.dataSet = dsh.getDataSet();
        this.dataSetHeader = dsh;
    }
    public void setOut(BitSet out) {
        this.out = out;
    }
    public void setIn(BitSet in) {
        this.in = in;
        columns = Util.countFlags(in);
    }
    public void set_check_indexes(int[] value) {
        this._check_indexes = value;
        this.rows = value.length;
    }
    /*----------------/SETTERS---------------------*/

    protected AbstractEstimate(DataSetHeader data, BitSet in, BitSet out, int[] check_rows){
        setDataSetHeader(data);
        setIn(in);
        setOut(out);
        set_check_indexes(check_rows);
    }

    public static AbstractEstimate GenerateEstimate(DataSetHeader dataSetHeader, BitSet in, BitSet out, int[] check_rows, ESTIMATION_TYPE type){
        switch (type){
            case ADEQUACY: return new Adequacy(dataSetHeader, in, out, check_rows);
            case SENSITIVITY: return new Sensitivity(dataSetHeader, in, out, check_rows);
            case STABILITY: return new Stability(dataSetHeader, in, out, check_rows);
            case TIME_TO_GET_OUT: return new TimeToGetOut(dataSetHeader, in, out, check_rows);
            default: throw new IllegalArgumentException("Can't create Estimate class. No such type");
        }
    }

    /**
     * Get maximum value of current estimations
     * @param estimations - current estimations
     * @return maximum estimation
     */
    public T getBestEstimation(List<T> estimations){
        T max = estimations.get(0);

        for (T est: estimations){
            if(getComparator().compare(max, est) < 0)
                max = est;
        }

        return max;
    }

    /**
     * Get estimation. If estimation is array, then gets the best estimation in array 
     * @param estimation - current estimation
     * @return (double) estimation
     */
    public double getBestEstimation(Object estimation){
        //if estimation not array
        if(estimation instanceof Double)
            return (Double)estimation;
        //if estimation is array
        Object[] array = (Object[])estimation;
        if(array == null || array.length == 0)
            return Double.NaN;
        Object best = array[0];
        AbstractComparator comp = getComparator();
        for(Object est: (Object[])estimation){
            if(comp.compare(best, est) < 0)
                best = est;
        }
        
        return (Double)best;
    }

    /**
     * Get minimum value of current estimations
     * @param estimations - current estimations
     * @return minimum estimation
     */
    public T getWorseEstimation(List<T> estimations){
        T min = estimations.get(0);

        for (T est: estimations){
            if(getComparator().compare(min, est) > 0)
                min = est;
        }

        return min;
    }

    /*----------------ABSTRACT---------------------*/
    /**
     * Return the specific estimate of model for chosen DataSet
     * @param model - model to check
     * @return specific estimate of model
     */
    public abstract T estimate(PlaneModel model);

    /**
     * Print obtained estimates
     */
    public abstract String toString(Object estimate);

    /**
     * Get name of the current estimate
     * @return - name of the current estimate
     */
    public abstract String getName();

    /**
     * Get comparator for result values
     * @return comparator
     */
    public abstract AbstractComparator getComparator();
    /*---------------/ABSTRACT---------------------*/

    /*-----------------ANCILLARY-------------------*/
    /**
     * Get average increase of elements in column
     * @param index - index of the column
     * @return 2*(MaxInColumn - MinInColumn)/(MaxInColumn + MinInColumn)
     */
    double getAverageIncrease(int index){
        if(getDataSet().columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");

        double min = getMinInColumn(index),
                max = getMaxInColumn(index);

        return (max - min) * 2 / (max + min);
    }

    /**
     * Get average increase of elements
     * @param max - max element
     * @param min - min element
     * @return 2*|Max - Min|/(Max + Min)
     */
    double getAverageIncrease(double max, double min) {
        return Math.abs(max - min) * 2 / (max + min);
    }

    /**
     * Get max value in column
     * @param index - index of column
     */
    double getMaxInColumn(int index){
        if(getDataSet().columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = getDataSet().get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = getDataSet().get(i, index);
            if(temp > res) res = temp;
        }

        return res;
    }

    /**
     * Get min value in column
     * @param index - index of column
     */
    double getMinInColumn(int index){
        if(getDataSet().columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = getDataSet().get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = getDataSet().get(i, index);
            if(temp < res) res = temp;
        }

        return res;
    }

    /**
     * Get mean value of elements in column
     * @param index - index of the column
     * @return mean value
     */
    double getMean(int index){
        if(!in.get(index))
            throw new IllegalArgumentException("Index out of range");
        if(rows < 1)
            throw new IllegalArgumentException("No elements in dataset");
        double res = 0;

        for(int i = 0; i < rows; i++)
            res += getDataSet().get(i, index);
        return res/rows;
    }

    /**
     * Gets adequacy for model for current indexes
     * @param indexes working indexes
     * @return A = (n - Sum|Ymodel[i] - Yout[i]|)/n
     */
    double adequacy(int[] indexes, PlaneModel model){
        double res = 0;
        for(int i = 0; i < indexes.length; i++){
            DataRow dr = getDataSet().getRow(indexes[i]);
            double a = model.getOut(dr);
            Double b = dr.get(getOut().nextSetBit(0));
            res += Math.abs(a - b);
        }

        return 1.0 - res/indexes.length;
    }

    double meanAbsolutePercentError(int[] indexes, PlaneModel model){
        double res = 0;
        for(int i = 0; i < indexes.length; i++){
            res += absolutePercentError(i, model);
        }

        return res/indexes.length;
    }

    double absolutePercentError(int index, PlaneModel model){
        DataRow dr = getDataSet().getRow(index);
        double Ymodel = model.getOut(dr);
        double Yreal  = dr.get(getOut().nextSetBit(0));
        double res = Math.abs((Yreal - Ymodel));/// (Yreal));
        return res;

    }

    String toStringDouble(Object estimate){
        return new String(getName() + ":\n\t\t" + (Double)estimate);
    }

    String toStringDoubleArr(Object estimate){
        String res = new String(getName() + ":\n\t\t");
        for(int i = 0; i < ((Double[])estimate).length; i++)
            res += ((Double[])estimate)[i].toString() + ", ";
        return res;
    }

    String toStringDoubleDoubleArr(Object estimate){
        String res = new String(getName() + ":");

        for(int j = 0; j < ((Double[][])estimate).length; j++){
            res += "\n\t\t";

            for(int i = 0; i < ((Double[][])estimate)[j].length; i++)
                res += ((Double[][])estimate)[j][i].toString() + ", ";
        }
        return res;
    }
    /*----------------/ANCILLARY-------------------*/
}
