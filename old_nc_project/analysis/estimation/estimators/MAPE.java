package com.netcracker.analysis.estimation.estimators;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.comparators.AbstractComparator;
import com.netcracker.analysis.estimation.comparators.DoubleComparatorMax;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;

@Deprecated
public class MAPE extends AbstractEstimate<Double> {

    protected MAPE(DataSetHeader data, BitSet in, BitSet out, int[] check_indexes) {
        super(data, in, out, check_indexes);
    }

    /**
     * Return the specific estimate of model for chosen DataSet
     * MAPE - mean absolute percent error
     *
     * @param model - model to check
     * @return specific estimate of model
     */
    @Override
    public Double estimate(PlaneModel model) {
        AbstractSamplingGetter a = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, rows);
        return meanAbsolutePercentError(a.getLearn_indexes(), model);
    }

    @Override
    public String toString(Object estimate) {
        return toStringDouble(estimate);
    }

    @Override
    public String getName() {
        return "MAPE";
    }

    @Override
    public AbstractComparator getComparator() {
        if(comparator == null)
            comparator = new DoubleComparatorMax();
        return comparator;
    }
}
