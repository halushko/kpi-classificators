package com.netcracker.analysis.estimation;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.BinaryTreeSpliter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.ArrayList;
import java.util.BitSet;

@Deprecated
public class Estimate {

    private DataSetHeader dataSetHeader;
    private DataSet data;
    private Model model;
    private BitSet in, out;

    private int rows, columns;

    public Model getModel() {
        return model;
    }

    public DataSetHeader getDataSetHeader() {
        return dataSetHeader;
    }
    public DataSet getDataSet() {
        return data;
    }
    public BitSet getOut() {
        return out;
    }
    public BitSet getIn() {
        return in;
    }

    public void setModel(Model model) {
        this.model = model;
    }
    public void setDataSetHeader(DataSetHeader dsh) {
        this.data = dsh.getDataSet();
        this.dataSetHeader = dsh;
        this.rows = data.rows();
    }
    public void setOut(BitSet out) {
        this.out = out;
    }
    public void setIn(BitSet in) {
        this.in = in;
        columns = Util.countFlags(in);
    }

    public Estimate(DataSetHeader data, PlaneModel model){
        setModel(model);
        setDataSetHeader(data);
        setIn(model.getColsIn());
        setOut(model.getColsOut());
    }

    /**
     * Checks model
     * @return array of sensitivity of each parameter.
     *          Row[0] - input parameters
     *          Row[1] - corresponding them output parameter
     */
    public double[][] sensitivity(){
        double[][] res = new double[2][columns];
        double[] means = new double[columns];

        for(int i = 0; i < columns; i++){
            int bi = Util.getNSetFlag(getIn(), i);
            means[i] = getMean(bi);
            res[0][i] = getAverageIncrease(bi);
        }

        DataRow dr = getDataSetHeader().createHistoryRowFromArray(means, getIn(), getOut());

        for(int i = 0; i < columns; i++){
            double ymin, ymax;
            int bi = Util.getNSetFlag(getIn(), i);
            double avg = means[i];

            dr.set(bi, getMaxInColumn(bi));
            ymax = getModel().getOut(dr);

            dr.set(bi, getMinInColumn(bi));
            ymin = getModel().getOut(dr);

            dr.set(bi, avg);

            res[1][i] = getAverageIncrease(ymax, ymin);
        }

        return res;
    }

    /**
     * Checks model
     * @return - fraction of right answers of model
     */
    public double checkModel(int[] indexes) {
        int fail_count = 0;
        for(int i = 0; i < indexes.length; i++){
            if(model.compareOut(data.getRow(indexes[i]))!=0)
                fail_count++;
        }

        return ((double)fail_count)/(indexes.length+0.00000001);  // to not have division by zero
    }

    /**
     * Check model
     * @return A = (n - Sum|Ymodel[i] - Yout[i]|)/n
     */
    public double adequacy(){
        AbstractSamplingGetter a = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, rows);
        return adequacy(a.getLearn_indexes());
    }
    
    private double adequacy(int[] indexes){
        double res = 0;
        for(int i = 0; i < indexes.length; i++){
            DataRow dr = getDataSet().getRow(i);
            res += Math.abs(getModel().getOut(dr) - dr.get(getOut().nextClearBit(0)));
        }

        return 1.0 - res/indexes.length;
    }

    public double stability(int steps){
        double res=1.0;
        if(steps < 2) throw new IllegalArgumentException("Steps must be more then '2'");

        BinaryTreeSpliter binaryTreeSpliter = new BinaryTreeSpliter();
        binaryTreeSpliter.count = rows;
        binaryTreeSpliter.steps = steps;
        
        ArrayList<int[]> indexes = binaryTreeSpliter.GenerateSplitedIndexes();
        
        for(int[] i: indexes){
            double temp = adequacy(i);
            if(temp < res)
                res = temp;
        }

        return res;
    }





    /***********************************MAPE*******************************************/
    /**
     * MAPE - средняя относительная ошибка (mean absolute percent error).
     * @see <a href="http://extrapolation.ru/adequacy-calc.html">Средняя относительная ошибка</a>
     */
    public double meanAbsolutePercentError(){
        AbstractSamplingGetter a = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, rows);
        return meanAbsolutePercentError(a.getLearn_indexes());
    }

    private double meanAbsolutePercentError(int[] indexes){
        double res = 0;
        for(int i = 0; i < indexes.length; i++){
            res += absolutePercentError(i);
        }

        return res/indexes.length;
    }

    /**
     * gets percent error for each line
     * @see <a href="http://extrapolation.ru/adequacy-calc.html">Средняя относительная ошибка</a>
     */
    public double[] absolutePercentError(){
        double[] errors = new double[rows];
        for(int i = 0; i< rows; i++){
            errors[i]=absolutePercentError(i);
        }

        return errors;
    }

    public double absolutePercentError(int index){
        DataRow dr = getDataSet().getRow(index);
        double Ymodel = getModel().getOut(dr);
        double Yreal  = dr.get(getOut().nextClearBit(0));
        double res = Math.abs((Yreal - Ymodel) / Yreal);
        return res;

    }

    /**************end*****************MAPE*******************************************/
    /**
     * Get mean value of elements in column
     * @param index - index of the column
     * @return mean value
     */
    private double getMean(int index){
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = 0;

        for(int i = 0; i < rows; i++)
            res += data.get(i, index);
        return res/rows;
    }

    /**
     * Get average increase of elements in column
     * @param index - index of the column
     * @return 2*(MaxInColumn - MinInColumn)/(MaxInColumn + MinInColumn)
     */
    private double getAverageIncrease(int index){
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");

        double min = getMinInColumn(index),
                max = getMaxInColumn(index);

        return (max - min) * 2 / (max + min);
    }

    /**
     * Get average increase of elements
     * @param max - max element
     * @param min - min element
     * @return 2*|Max - Min|/(Max + Min)
     */
    private double getAverageIncrease(double max, double min) {
        return Math.abs(max - min) * 2 / (max + min);
    }

    /**
     * Get max value in column
     * @param index - index of column
     */
    private double getMaxInColumn(int index){
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = data.get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = data.get(i, index);
            if(temp > res) res = temp;
        }

        return res;
    }

    /**
     * Get min value in column
     * @param index - index of column
     */
    private double getMinInColumn(int index){
        if(data.columns() < index - 1) throw new IllegalArgumentException("Index out of range");
        if(rows < 1) throw new IllegalArgumentException("No elements in dataset");
        double res = data.get(0, index);

        for(int i = 1; i < rows; i++){
            double temp = data.get(i, index);
            if(temp < res) res = temp;
        }

        return res;
    }

}
