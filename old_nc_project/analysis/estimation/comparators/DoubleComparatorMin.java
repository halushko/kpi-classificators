package com.netcracker.analysis.estimation.comparators;

public class DoubleComparatorMin extends AbstractComparator<Double> {
    @Override
    public int compare(Double o1, Double o2) {
        return Double.compare(o2, o1);
    }
}