package com.netcracker.analysis.estimation.comparators;

public class DoubleArrayMaxComparator extends AbstractComparator<Double[]> {
    @Override
    public int compare(Double[] o1, Double[] o2) {
        double max1 = Double.NEGATIVE_INFINITY, max2 = Double.NEGATIVE_INFINITY;
        for(Double d1: o1)
        if(Double.compare(max1, d1) < 0)
            max1 = d1;
        for(Double d2: o2)
        if(Double.compare(max2, d2) < 0)
            max2 = d2;
        return Double.compare(max1, max2);
    }
}

