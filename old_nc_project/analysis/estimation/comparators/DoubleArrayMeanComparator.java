package com.netcracker.analysis.estimation.comparators;

public class DoubleArrayMeanComparator extends AbstractComparator<Double[]> {
     @Override
    public int compare(Double[] o1, Double[] o2) {
        double mean1 = 0, mean2 = 0;
        for(Double d1: o1)
            mean1 += d1;
        for(Double d2: o2)
            mean2 += d2;
        return Double.compare(mean1,mean2);
    }
}
