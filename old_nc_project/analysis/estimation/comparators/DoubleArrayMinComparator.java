package com.netcracker.analysis.estimation.comparators;

public class DoubleArrayMinComparator extends AbstractComparator<Double[]> {

    @Override
    public int compare(Double[] o1, Double[] o2) {
        double min1 = Double.NEGATIVE_INFINITY, min2 = Double.NEGATIVE_INFINITY;
        for(Double d1: o1)
            if(Double.compare(min1, d1) > 0)
                min1 = d1;
        for(Double d2: o2)
            if(Double.compare(min2, d2) > 0)
                min2 = d2;
        return Double.compare(min1, min2);
    }
}

