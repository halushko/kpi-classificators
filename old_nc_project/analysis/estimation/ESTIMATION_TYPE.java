package com.netcracker.analysis.estimation;

public enum ESTIMATION_TYPE{
    ADEQUACY, SENSITIVITY, STABILITY, TIME_TO_GET_OUT
}
