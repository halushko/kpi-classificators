package com.netcracker.analysis.resources;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.data.HistoryRow;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.presentation.JSONUtils;
import com.netcracker.analysis.util.Print;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.*;

import static com.netcracker.analysis.util.Util.getFromSession;

@Path("/datasets")
public class DataSets {
    Log log = LogFactory.getLog(getClass());

    @GET
    @Path("dropSession")
    @Produces(MediaType.APPLICATION_JSON)
    public String dropSession(@Context HttpServletRequest req){
        req.getSession().setAttribute("dataSetsBean",null);

        return "1";
    }

    @GET
    @Path("getLatestParams/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLatestParams(@Context HttpServletRequest req,@PathParam("id") String id){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((id));
        DataRow dataRow=dsh.getLatestParameters();
        //Hash of output data
        JSONObject hashMap=new JSONObject();
        //Adding parameters
        JSONObject parameters_hm=new JSONObject();
        for(DataSetParameter parameter:dsh.getParameterList()){
            parameters_hm.put(parameter.getId().toString(),dataRow.get(parameter.getName()));
        }
        hashMap.put("params",parameters_hm);
        JSONObject models_hm=new JSONObject();
        //Models
        for(GenericModel model:dsh.getModelList()){
            try{
                models_hm.put(model.getId().toString(),model.getOut(dataRow));
            }catch (Throwable th){
                log.error("Error during model ("+model+") value calculation: " + th);
                models_hm.put(model.getId().toString(), "ERROR");
            }
        }
        hashMap.put("models",models_hm);
        hashMap.put("paramsDate", dataRow.getDate().toString());
        return JSONUtils.toJSON(hashMap).toString();
    }

    @GET
    @Path("getBenchmarks/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getBenchmarks(@Context HttpServletRequest req,@PathParam("id") String id){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((id));
        DataRow dataRow=dsh.getLatestParameters();

        JSONObject models_hm=new JSONObject();
        
        List<ModelBenchmark> benchs = new ArrayList<ModelBenchmark>();
        //Models
        AbstractSamplingGetter sg = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, dsh.getDataSet().rows());
        for(GenericModel model:dsh.getModelList()){
            try{

                ModelBenchmark mb = new ModelBenchmark((PlaneModel) model, sg.getCheck_indexes());
                models_hm.put(model.getId().toString(), Print.printModelBenchmarks(Arrays.asList(mb)));

                benchs.add(mb);
            }catch (Throwable th){
                log.error("Error during model ("+model+") value calculation: " + th);
                models_hm.put(model.getId().toString(), "ERROR");
            }
        }
        JSONObject response =  new JSONObject();
        response.put("benchmarks", models_hm);
        response.put("table", Print.printModelBenchmarksTable(benchs));
        return response.toString();
    }

    @GET
    @Path("{id}/report")
    @Produces(MediaType.APPLICATION_JSON)
    public String generateReport(
            @Context HttpServletRequest req,
            @PathParam("id") String id
    ){
        return null;
    }

///rest/datasets/'+viewModel.selectedDataSet().idString()+'/eval/'+new_values+'/'+viewModel.saveExplicitValues(),
    @GET
    @Path("{id}/eval/{new_values}/{saveExplicit}")
    @Produces(MediaType.APPLICATION_JSON)
    public String evaluateModels(
            @Context HttpServletRequest req,
            @PathParam("id") String id,
            @PathParam("new_values") String new_values,
            @PathParam("saveExplicit") boolean saveExplicit
    ){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((id));

        double vals[] = new double[dsh.getParameterListUnmodified().size()];

        int counter = 0;
        for(String val : new_values.split("_"))
        {
            vals[counter] = Double.valueOf(val);
            counter++;
        }

        DataRow d = new HistoryRow(dsh, new Date());
        for (int i = 0; i<vals.length; i++)
            d.set(i, vals[i]);

        if(saveExplicit) {
            //DaoFactory.TT_DAO.
        }
        
        
        JSONObject models_hm=new JSONObject();
        //Models
        for(Model model:dsh.getModelList()){
            try{
                models_hm.put(model.getId().toString(),model.getOut(d));
            }catch (Throwable th){
                log.error("Error during model ("+model+") value calculation");
                models_hm.put(model.getId().toString(), "ERROR");
            }
        }
        return models_hm.toString();
    }


    @GET
    @Path("deleteModel/{id_ds}/{id_m}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteModel(@Context HttpServletRequest req,@PathParam("id_ds") String id_ds, @PathParam("id_m") String id_m ){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);

        List<GenericModel> models = bean.getDataSets().get((id_ds)).getModelList();

        synchronized (models){
            HashMap<String, GenericModel> hm = Util.toHashMap(models);

            GenericModel m = hm.get((id_m));

            boolean res = DaoFactory.MODEL_DAO.delete(m.getId());

            if (res)
                models.remove(m);

            //Hash of output data
            HashMap<String,Boolean> hashMap=new HashMap();
            hashMap.put("result", res);
            return JSONUtils.toJSON(hashMap).toString();
        }
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAll(@Context HttpServletRequest req){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        return bean.getDataSetsAsJSON().toString();
    }

    @GET
    @Path("refreshModels/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String refreshModels(@Context HttpServletRequest req,@PathParam("id") String id){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((id));

        List<GenericModel> modelList = dsh.refreshModelList();

        return JSONUtils.toJSON(modelList).toString();
    }

    @GET
    @Path("refreshDataSet/{id}")
    public String refreshDataSet(@Context HttpServletRequest req,@PathParam("id") String id){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((id));
        dsh.refreshDataSet();

        return "";
    }

    @GET
    @Path("setGetter/{type}/{dataset_id}")
    public String setGetter(@Context HttpServletRequest req,@PathParam("type") String type,@PathParam("dataset_id") String id){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        bean.setSamplingGetter(id,type);
        return "";
    }
}
