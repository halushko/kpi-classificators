package com.netcracker.analysis.resources;

import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.dao.SaveException;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.data.HistoryRow;
import com.netcracker.analysis.data.LearningRow;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.netcracker.analysis.util.Util.getFromSession;

@Path("/parameters")
public class Parameters {

    @POST
    @Path("/post")
    @Consumes("application/json")
    public Response acceptParameters(@Context HttpServletRequest req,String json) throws ParseException, SaveException {
        JSONObject object=JSONObject.fromObject(json);
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get(object.get("datasetId"));
        if(dsh==null){
            return Response.status(404).build();
        }
        List<DataSetParameter> params=dsh.getParameterListUnmodified();
        String date_timeString= (String) object.get("date_time");
        DateFormat df = new SimpleDateFormat("HH:mm:ss MM/dd/yy");
        Date date_time=df.parse(date_timeString);
        HistoryRow row = new HistoryRow(dsh, date_time);

        JSONObject jsonParams= (JSONObject) object.get("params");
        for (int i=0;i<params.size();i++){
            DataSetParameter param=params.get(i);
            String jsonParam= (String) jsonParams.get(param.getMapping());
            if (jsonParam!=null){
                row.set(i,Double.parseDouble(jsonParam));
            }
        }
        DaoFactory.DATA_ROW_DAO.save(row);
        return Response.status(200).build();
    }

    @GET
    @Path("savePoint/{data_set_id}/{parvals}")
    @Produces(MediaType.APPLICATION_JSON)
    public String learnModel_nosave(@Context HttpServletRequest req,
                                    @PathParam("data_set_id") String data_set_id,
                                    @PathParam("parvals") String par ){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh=bean.getDataSets().get((data_set_id));

        LearningRow row = new LearningRow(dsh, new Date());

        try {
            String[] pars = par.split("_");
            for (int i=0;i<dsh.getRealParametersCount();i++){
                String jsonParam = pars[i];
                if (jsonParam!=null){
                    row.set(i,Double.parseDouble(jsonParam));
                }
            }
            DaoFactory.DATA_ROW_DAO.save(row);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return "false";
        }

        return "true";
    }
}
