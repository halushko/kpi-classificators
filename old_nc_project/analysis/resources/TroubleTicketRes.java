package com.netcracker.analysis.resources;


import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.RealTroubleTicket;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.presentation.JSONUtils;
import com.netcracker.analysis.taskman.CompareModelsTask;
import com.netcracker.analysis.taskman.Task;
import com.netcracker.analysis.taskman.TaskMan;
import com.netcracker.analysis.util.Util;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.math.BigInteger;
import java.util.List;

import static com.netcracker.analysis.util.Util.getFromSession;

@Path("/tickets")
public class TroubleTicketRes {
    Log log = LogFactory.getLog(getClass());

    @GET
    @Path("{dataSetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getForDs(@Context HttpServletRequest req,
                           @PathParam("dataSetId") String dataSetId
    )
    {
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        final DataSetHeader dsh = bean.getDataSets().get(dataSetId);
        if(dsh == null){
            throw new WebApplicationException(Response.status(403).type("text/plain")
                    .entity("Can't find data set with ID:" + dataSetId).build());
        }
        final List<RealTroubleTicket> tts =DaoFactory.REAL_TT_DAO.getForDataSet(dsh);
        JSONObject o = new JSONObject();
        o.put("dataSetName",dsh.getName() );
        o.put("tickets",JSONUtils.toJSON(tts));
        return o.toString();
    }

}
