package com.netcracker.analysis.resources;

import com.netcracker.analysis.presentation.JSONUtils;
import com.netcracker.analysis.taskman.Task;
import com.netcracker.analysis.taskman.TaskMan;
import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/tasks")
public class Tasks {

    @GET
    @Path("/{task_id}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskStatus (@Context HttpServletRequest req, @PathParam("task_id") int task_id){
        Task task = TaskMan.getInstance().getTask(task_id);
        Task.Status status = task!=null? task.getStatus() : Task.Status.TaskNotFound;

        JSONObject o = new JSONObject();
        o.put("status", status);
        if (status.equals(Task.Status.Completed))
            o.put("response", task.getResponse());
        if (status.equals(Task.Status.Error))
            o.put("error", task.getResponse());

        return o.toString();
    }

    @GET
    @Path("/{task_id}/get")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTask(@Context HttpServletRequest req, @PathParam("task_id") int task_id){
        Task task = TaskMan.getInstance().getTask(task_id);
        return JSONUtils.toJSON(task).toString();
    }

    @GET
    @Path("/{task_id}/stop")
    @Produces(MediaType.APPLICATION_JSON)
    public String stopTask(@Context HttpServletRequest req, @PathParam("task_id") int task_id){
        return JSONUtils.toJSON(TaskMan.getInstance().stopTask(task_id)).toString();
    }
}
