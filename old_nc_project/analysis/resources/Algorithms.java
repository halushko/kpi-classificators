package com.netcracker.analysis.resources;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.AlgorithmLister;
import com.netcracker.analysis.algorithms.DecisionTreeAlgorithm;
import com.netcracker.analysis.algorithms.DecisionTreeCoreAlgorithm;
import com.netcracker.analysis.algorithms.NonLinearRegressionAlgorithm;
import com.netcracker.analysis.algorithms.PotentialFunctionAlgorithm;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.PotentialFunctionModel;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.presentation.JSONUtils;
import com.netcracker.analysis.taskman.*;
import com.netcracker.analysis.util.Util;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.BitSet;

import static com.netcracker.analysis.util.Util.getFromSession;

@Path("/algorithms")
public class Algorithms {
    Log log = LogFactory.getLog(getClass());

    @GET
    @Path("CompareBest")
    @Produces(MediaType.APPLICATION_JSON)
    public String compareAll(@Context HttpServletRequest req){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);

        Task task = new CompareModelsTask(bean.getModelBenchmarks());
        int task_number = TaskMan.getInstance().start(task);

        return task_number+"";
    }

    @GET
    @Path("SaveBest")
    @Produces(MediaType.APPLICATION_JSON)
    public String saveBest(@Context HttpServletRequest req){
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);

        Task task = new SaveBestModelsTask(bean.getModelBenchmarks());
        int task_number = TaskMan.getInstance().start(task);

        return task_number+"";
    }

    @GET
    @Path("{algName}/learn/{data_set_id}/{inPars}_{outPars}/{params}")
    @Produces(MediaType.APPLICATION_JSON)
    public String learnModel(@Context HttpServletRequest req,
                              @PathParam("data_set_id") String data_set_id,
                              @PathParam("algName") String algName,
                              @PathParam("inPars") String inPars,
                              @PathParam("outPars") String outPars,
                              @PathParam("params") String par ){

        return learnModelMethod(req, data_set_id, algName, inPars, outPars, par, true);
    }

    @GET
    @Path("{algName}/learn-nosave/{data_set_id}/{inPars}_{outPars}/{params}")
    @Produces(MediaType.APPLICATION_JSON)
    public String learnModel_nosave(@Context HttpServletRequest req,
                             @PathParam("data_set_id") String data_set_id,
                             @PathParam("algName") String algName,
                             @PathParam("inPars") String inPars,
                             @PathParam("outPars") String outPars,
                             @PathParam("params") String par ){

        return learnModelMethod(req, data_set_id, algName, inPars, outPars, par, false);
    }

    private String learnModelMethod(HttpServletRequest req, String data_set_id, String algName, String inPars, String outPars, String par, boolean save) {
        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh = bean.getDataSets().get(data_set_id);
        Algorithm a = AlgorithmLister.getInstance().getAlgorithm(algName);
        BitSet inp = Util.bitsetFromString(inPars);
        BitSet oup = Util.bitsetFromString(outPars);

        setParToAlgorithm(a, par);

        if (dsh == null || a == null || inp == null || oup == null){

            log.error(String.format("LearnModel not started. Parameters: algName: %s, I/O: %s|%s", algName, inPars, outPars));
            return JSONUtils.toJSON(-1).toString();
        }

        log.info(String.format("LearnModel algName: %s, I/O: %s|%s",algName,inPars,outPars));
        Task task = new LearnModelTask(bean, dsh, a, inp, oup, bean.getSamplingGetter(data_set_id), save);
        int task_number = TaskMan.getInstance().start(task);

        return task_number+"";
    }

    @GET
    @Path("{algName}/CompareByParams/{data_set_id}/{inPars}_{outPars}/{params}")
    @Produces(MediaType.APPLICATION_JSON)
    public String compareByParams(@Context HttpServletRequest req,
                             @PathParam("data_set_id") String data_set_id,
                             @PathParam("algName") String algName,
                             @PathParam("inPars") String inPars,
                             @PathParam("outPars") String outPars,
                             @PathParam("params") String par ){

        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh = bean.getDataSets().get(data_set_id);
        Algorithm a = AlgorithmLister.getInstance().getAlgorithm(algName);
        BitSet inp = Util.bitsetFromString(inPars);
        BitSet oup = Util.bitsetFromString(outPars);

        setParToAlgorithm(a, par);

        if (dsh == null || a == null || inp == null || oup == null){

            log.error(String.format("CompareByParams not started. Parameters: algName: %s, I/O: %s|%s", algName, inPars, outPars));
            return JSONUtils.toJSON(-1).toString();
        }

        log.info(String.format("CompareByParams algName: %s, I/O: %s|%s",algName,inPars,outPars));
        Task task = new DiscarderTask(dsh, a, inp, oup, bean);
        int task_number = TaskMan.getInstance().start(task);

        return task_number+"";
    }

    @GET
    @Path("SimplifyNonLinear/{data_set_id}/{inPars}_{outPars}/{params}")
    @Produces(MediaType.APPLICATION_JSON)
    public String compareByParams(@Context HttpServletRequest req,
                             @PathParam("data_set_id") String data_set_id,
                             @PathParam("inPars") String inPars,
                             @PathParam("outPars") String outPars,
                             @PathParam("params") String par ){

        DataSetsControllerBean bean=getFromSession (req,"dataSetsBean", DataSetsControllerBean.class);
        DataSetHeader dsh = bean.getDataSets().get(data_set_id);
        BitSet inp = Util.bitsetFromString(inPars);
        BitSet oup = Util.bitsetFromString(outPars);

        int a;
        try{a = Integer.parseInt(par);}catch (Exception e){a = 0;}

        if (dsh == null || a < 2 || inp == null || oup == null){

            log.error(String.format("SimplifyNonLinear not started. Parameters:, I/O: %s|%s, degree: %s", inPars, outPars, par));
            return JSONUtils.toJSON(-1).toString();
        }

        log.info(String.format("SimplifyNonLinear  I/O: %s|%s, degree: %s",inPars,outPars, par));
        Task task = new SimplifyNonLinearTask(dsh, a, inp, oup, bean);
        int task_number = TaskMan.getInstance().start(task);

        return task_number+"";
    }

    private void setParToAlgorithm(Algorithm a, String par) {
        double k;
        try{k = Double.parseDouble(par);}catch (Exception e){k = 0;}
        if (a instanceof NonLinearRegressionAlgorithm){
            if ((int)k!=0)
            ((NonLinearRegressionAlgorithm) a).setDegree((int)k);
        }
        if (a instanceof PotentialFunctionAlgorithm){
            ((PotentialFunctionAlgorithm) a).setPotentialFunction(new PotentialFunctionModel.PotentialFunction1(k));
        }
        if (a instanceof DecisionTreeAlgorithm){
            DecisionTreeCoreAlgorithm alg = ((DecisionTreeAlgorithm) a).getAlgorithm();
            if(alg instanceof PotentialFunctionAlgorithm){
                ((PotentialFunctionAlgorithm) alg).setPotentialFunction(new PotentialFunctionModel.PotentialFunction1(k));
            }
        }
    }
}