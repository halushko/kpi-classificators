package com.netcracker.analysis.benchmark;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class BenchmarkResult {

    private final String algorithmName;
    private long learnTime;
    private long learningMemoryUsed;
    private List<ValidationResult> validations = new ArrayList<ValidationResult>();

    public BenchmarkResult(String algorithmName){
        this.algorithmName = algorithmName;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setLearningNanoTime(long learnTime) {

        this.learnTime = learnTime;
    }

    public void setLearningMemoryUsed(long learningMemoryUsed) {
        this.learningMemoryUsed = learningMemoryUsed;
    }


    public static String formatNanoTime(long time){
        return time+" ns";
    }

    public List<ValidationResult> getValidations() {
        return validations;
    }

    public void prettyPrint(PrintWriter writer) {
        for (ValidationResult validationResult : getValidations()) {
            printSelf(writer);
            validationResult.prettyPtint(writer);
            writer.println();
        }
        if(getValidations().size()==0){
            printSelf(writer);
            writer.println();
        }

    }

    private void printSelf(PrintWriter writer){
        writer.printf("%s\t%s\t%s",algorithmName,learningMemoryUsed,learnTime);
    }
}
