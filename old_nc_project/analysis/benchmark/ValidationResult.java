package com.netcracker.analysis.benchmark;

import java.io.PrintWriter;

@Deprecated
public class ValidationResult {
    public final String validatorName;
    public final Object value;
    public final long validationTime;

    public ValidationResult(String validatorName, Object value, long validationTime){
        this.validatorName = validatorName;
        this.value = value;
        this.validationTime = validationTime;
    }

    public void prettyPtint(PrintWriter writer) {
        writer.printf("\t%s\t%f\t%d",validatorName,value,validationTime);
    }
}
