package com.netcracker.analysis.benchmark;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.AlgorithmLister;
import com.netcracker.analysis.LearningParameters;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.estimation.Estimate;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import static java.lang.System.nanoTime;

public class AlgorithmBenchmark {
    //private LearningParameters params;
    private static Runtime runtime = Runtime.getRuntime();
    private final Log log = LogFactory.getLog(getClass());

//    private static final Validator[] VALIDATORS = new Validator[]{
//        new PrimitiveValidator(),
//    };
    private static final AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, 1);

    public AlgorithmBenchmark(){
    }

    /**
     *
     * @param etalonParams general parameters (input property is ignored)
     * @param inputParametersSets list ov differemt lenghts input parameters sets, to check how model is operating on different param count
     */
    public MultiBenchmarkResult runMultipleTests(LearningParameters etalonParams, List<BitSet> inputParametersSets){

        log.info("benchmark for different params sets: Started");

        for (BitSet inputSet : inputParametersSets){
            log.info(MessageFormat.format("benchmark for different params sets: {0} of {1}",
                    inputParametersSets.indexOf(inputSet),
                    inputParametersSets.size()));

            LearningParameters parameters =  etalonParams.clone();
            parameters.setColsIn(inputSet);

            run(parameters);

        }
        log.info("benchmark for different params sets: Done");


        return new MultiBenchmarkResult();
    }

    public List<BenchmarkResult> run(LearningParameters params){
        List<Algorithm> algorithmList = AlgorithmLister.getInstance().getAll();
        List<BenchmarkResult> results = new ArrayList<BenchmarkResult>( algorithmList.size());

        for (Algorithm algorithm : algorithmList) {
            BenchmarkResult result;

            result = estimateAlgorithm(algorithm, params);
            results.add(result);
        }


        return results;
    }

    private BenchmarkResult estimateAlgorithm(Algorithm algorithm, LearningParameters params) {
        assert false;
        BenchmarkResult result;
        runtime.gc();
        result = new BenchmarkResult(getClassName(algorithm));
        long mem = memoryUsed();
        long time = nanoTime();

        Model model = algorithm.buildModel(params, null);

        long time2 = nanoTime();
        long mem2 = memoryUsed();

        result.setLearningMemoryUsed(mem2-mem);
        result.setLearningNanoTime(time2 - time);

        //samplingGetter.setCount(params.getDataSet().getDataSet().rows());

        /*for(Validator validator : VALIDATORS)*/
        {
            time = nanoTime();
            double quality = new Estimate(params.getDataSet(), (PlaneModel) model).checkModel(samplingGetter.getCheck_indexes());
            time2 = nanoTime();

            result.getValidations().add(new ValidationResult("Delta",quality, time2-time));
        }

        runtime.gc();
        return result;
    }

    static long memoryUsed ()
    {
        return runtime.totalMemory () - runtime.freeMemory ();
    }

    static String getClassName(Object o){
        return o.getClass().getSimpleName();
    }
}
