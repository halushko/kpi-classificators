package com.netcracker.analysis.benchmark;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.estimation.estimators.AbstractEstimate;
import com.netcracker.analysis.models.PlaneModel;

import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;

public class ModelBenchmark {
    private HashMap<ESTIMATION_TYPE, AbstractEstimate> _estimators;
    private HashMap<ESTIMATION_TYPE, Object> _values;
    private PlaneModel _model;
    private BitSet _in, _out;
    private DataSetHeader _dataSetHeader;
    private int[] _checkRows;

    public ModelBenchmark(PlaneModel model, int[] checkRows){
        setModel(model, checkRows);
        Initialize();
    }



    private void Initialize() {
        HashMap<ESTIMATION_TYPE, AbstractEstimate> estimators = getEstimators();
        for(ESTIMATION_TYPE est: ESTIMATION_TYPE.values())
            estimators.put(
                    est, AbstractEstimate.GenerateEstimate(getDataSetHeader(), _in, _out, getCheckRows(), est));
    }
    private void generateValues(){
        for (ESTIMATION_TYPE est: getEstimators().keySet()){
            getValues().put(est, getEstimators().get(est).estimate(getModel()));
        }
    }


    public PlaneModel getModel() {
        return _model;
    }
    public DataSetHeader getDataSetHeader() {
        return _dataSetHeader;
    }
    public int[] getCheckRows() {
        if(_checkRows == null)
            _checkRows = new int[0];
        return _checkRows;
    }
    public HashMap<ESTIMATION_TYPE, Object> getValues() {
        if(_values == null)
            _values = new HashMap<ESTIMATION_TYPE, Object>();
        return _values;
    }
    public HashMap<ESTIMATION_TYPE, AbstractEstimate> getEstimators() {
        if(_estimators == null)
            _estimators = new HashMap<ESTIMATION_TYPE, AbstractEstimate>();
        return _estimators;
    }
    
    private void setModel(PlaneModel model, int[] checkRows) {
        if(model == null)
            throw new IllegalArgumentException("Model can't be null");
        this._model = model;
        _in = model.getColsIn();
        _out = model.getColsOut();
        _checkRows = checkRows;
        
        setDataSetHeader(model.getParent());
        getValues().clear();
    }
    private void setDataSetHeader(DataSetHeader dataSetHeader) {
        if(dataSetHeader == null)
            throw new IllegalArgumentException("DataSetHeader can't be null");
        this._dataSetHeader = dataSetHeader;
    }

    public Object getValue(ESTIMATION_TYPE type){
        if(getValues().isEmpty())
            generateValues();
            
        return getValues().get(type);
    }
    public double getDoubleEstimate(ESTIMATION_TYPE type){
        return getEstimator(type).getBestEstimation(getValue(type));
    }

    public AbstractEstimate getEstimator(ESTIMATION_TYPE type){
        if(getEstimators().isEmpty())
            Initialize();
        return getEstimators().get(type);
    }
    public String toString(){
        PlaneModel model = getModel();
        String res = "\n\n\t" + model.toString() + (model.isInvalid() ? " invalid " : " valid ");
        BitSet in = _in;

        for(int i= 0; i < in.length(); i++){
            if(in.get(i))
                res += " [" + model.getParent().getParameterList().get(i).getName() + "] ";
        }

        res += "\n------------------------------\n";
        res += "Model params:\n\t\t"+model.getLegend();

        for(ESTIMATION_TYPE est_type: ESTIMATION_TYPE.values()){
            AbstractEstimate currentEstimator = getEstimator(est_type);
            Object value = getValue(est_type);

            res += "\n" + currentEstimator.toString(value);
        }
        res += "\nLearning Time:\n\t\t" + model.getTrainingTime();
        res += "\nLearning Memory:\n\t\t" + model.getLearnMemory()/1024+" kb";

        return res;
    }

    /**
     * @return Name -> [P_1][P_2]..[P_N] -> EST_1 -> EST_2 -> EST_K
     */
    public String printIntoTable(){
        PlaneModel model = getModel();
        BitSet in = _in;
        String res = model.toString() + "\t";

        for(int i= 0; i < in.length(); i++){
            if(in.get(i))
                res += "[" + model.getParent().getParameterList().get(i).getName() + "]";
        }

    //    res += "\t" + model.getLegend();
        res += "\t" + model.getTrainingTime();
        res += "\t" + model.getLearnMemory()/1024+" kb";

        Object[] ests = ESTIMATION_TYPE.values();
        Arrays.sort(ests);
        for(Object type: ests){
            AbstractEstimate estimator = getEstimator((ESTIMATION_TYPE)type);
            double value = estimator.getBestEstimation(getValue((ESTIMATION_TYPE)type));
            res += "\t" + value;
        }
        
        return res;
    }
}
