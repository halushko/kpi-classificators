package com.netcracker.analysis;

import com.netcracker.analysis.data.DataRow;

import java.math.BigInteger;

/**
 *
 */
public interface Model {
    double getOut(DataRow input_vars_row);
    int[] getLearnIndexes();
    int[] getCheckIndexes();

    void setLearnIndexes(int[] value);
    void setCheckIndexes(int[] value);

//    /**
//     * @deprecated Use getOut(DataRow input_vars_row)
//     * @param input_vars_row
//     * @return
//     */
//    double getOut(double[] input_vars_row);
    double compareOut(DataRow input_vars_row);

    String getLegend();

    BigInteger getId();
    String getIdString();

    long getLearnMemory();

    void setLearnMemory(long learnMemory);
}
