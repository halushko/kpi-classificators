package com.netcracker.analysis.models;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.HistoryRow;

import java.math.BigInteger;
import java.util.BitSet;

public class NonLinearRegressionModel extends LogisticLinearRegressionModel{

    public NonLinearRegressionModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent, serializedData);
    }

    public NonLinearRegressionModel(DataSetHeader parent, double[] w, BitSet colsin, BitSet colsout, int[] learn, int[] check, long trainingTime) {
        super(parent, w, colsin, colsout, learn,  check, trainingTime);
    }

    public NonLinearRegressionModel(LogisticLinearRegressionModel model) {
        super(model.getParent(), model.getParamCoefs(), model.getColsIn(), model.getColsOut(), model.getLearnIndexes(), model.getCheckIndexes(), model.getTrainingTime());
        isInvalid(model.isInvalid());
    }

    /**
     * Get class of the point
     * @param input_vars_row - values of parameters
     * @return 1 - if good class
     *         0 - if bad class
     */
    public double getOut(DataRow input_vars_row) {

        DataRow normalized = normalizeRow(input_vars_row);

        return super.getOut(normalized);
    }

    private DataRow normalizeRow(DataRow input_vars_row) {
        DataRow row = new HistoryRow(input_vars_row.getParent(), input_vars_row.getDate());
        for (int i = 0; i<input_vars_row.getParent().getRealParametersCount(); i++){
            row.set(i, input_vars_row.get(i));
        }
        row.normalizeColumns();
        return row;
    }
}
