package com.netcracker.analysis.models;


import com.netcracker.analysis.Model;
import com.netcracker.analysis.algorithms.DecisionTree;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.exceptions.ExceptionFactory;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class DecisionTreeModel extends PlaneModel<DecisionTreeModel> implements Model {

    public DecisionTreeModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent);
        fillDataFromString(serializedData);
    }

    public DecisionTreeModel(DecisionTree tree, int[] learn, int[] check){
        super(tree.getDataSetHeader(), tree.getIn(), tree.getOut(), learn, check, -1);
        decisionTree = tree;
    }

    @Override
    public void fillDataFromString(String serializedData) {
        deserialize(serializedData, DecisionTreeModelSettings.class);
    }

    @Override
    protected PlaneModelSettings deserialize(String serialized_string, Class c){
        DecisionTreeModelSettings modelSettings = (DecisionTreeModelSettings) super.deserialize(serialized_string, c);

        decisionTree = getDecisionTree(modelSettings.decisionTree);

        return modelSettings;
    }

    private DecisionTree getDecisionTree(DecisionTreeSettings decisionTree) {
        if (!Double.isNaN(decisionTree.classMembership)) {
            DecisionTree dt = new DecisionTree(decisionTree.classMembership);
            return dt;
        } else if (decisionTree.modelID != null && decisionTree.good!= null && decisionTree.bad != null){
            DecisionTree dt = new DecisionTree((PlaneModel) DaoFactory.MODEL_DAO.get(getParent(), decisionTree.modelID), getDecisionTree(decisionTree.good), getDecisionTree(decisionTree.bad));
            return dt;
        } else {
            throw ExceptionFactory.IllegalArgument(log, "Unable to deserialise DesisionTree. class=%s, modelID=%s, good_not_null=%s, bad_not_null=%s", decisionTree.classMembership, decisionTree.modelID, decisionTree.good!=null, decisionTree.bad!=null);
        }
    }


    @Override
    public String serializeData() {
        DecisionTreeModelSettings sd = new DecisionTreeModelSettings(this);
        String ser = sd.serialize();
        return ser;
    }

    public double getOut(DataRow input_vars_row) {
        return decisionTree.getDecision(input_vars_row);
    }

    public String getLegend() {
        return print(decisionTree, 1);
    }
    
    private String print(DecisionTree tree, int step){
        String res = "\n|";
        for(int i = 0; i < step; i++)
            res+="--";
        if(((Double)tree.getClassMembership()).isNaN()){
            res += tree.getSeparatingModel().getLegend();
            res += print(tree.getGood(), step + 1);
            res += print(tree.getBad(), step + 1);
        }
        else{
            res += tree.getClassMembership();
        }
        return res;
    }
    
    private DecisionTree decisionTree;
    DecisionTree getDecisionTree(){
        return decisionTree;
    }

    public List<GenericModel> getModels(){
        List<GenericModel> models = new ArrayList<GenericModel>();
        decisionTree.getModels(models);
        return models;
    }
}

@XmlRootElement(name = "ModelSettings")
class DecisionTreeModelSettings extends PlaneModelSettings {
    @XmlElement(name = "DT")
    public DecisionTreeSettings decisionTree;

    public DecisionTreeModelSettings(){}

    public DecisionTreeModelSettings(DecisionTreeModel model){
        super(model);
        decisionTree = new DecisionTreeSettings(model.getDecisionTree());
    }
}

@XmlRootElement(name = "DTS")
class DecisionTreeSettings {
    @XmlElement(name = "Mdl")
    public BigInteger modelID;
    @XmlElement(name = "G")
    public DecisionTreeSettings good;
    @XmlElement(name = "B")
    public DecisionTreeSettings bad;
    @XmlElement(name = "Cls")
    double classMembership;

    public DecisionTreeSettings(){
    }

    public DecisionTreeSettings(DecisionTree model){
        if (model.getSeparatingModel()!= null)
            modelID = model.getSeparatingModel().getId();
        if (model.getGood() != null)
            good = new DecisionTreeSettings(model.getGood());
        if (model.getBad() != null)
            bad = new DecisionTreeSettings(model.getBad());
        classMembership = model.getClassMembership();
    }

}
