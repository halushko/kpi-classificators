package com.netcracker.analysis.models;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

public abstract class PlaneModel<T extends PlaneModel<T>> extends GenericModel<PlaneModel<T>>{

    private BitSet colsin;
    private BitSet colsout;
    private int[] learning_indexes, check_indexes;
    private long learnMemory;


    public int[] getLearnIndexes() {
        if(learning_indexes == null){
            assert false;
            learning_indexes = new int[0];
        }
        return learning_indexes;
    }

    public int[] getCheckIndexes() {
        if(check_indexes == null){
            assert false;
            check_indexes = new int[0];
        }
        return check_indexes;
    }
    public void setLearnIndexes(int[] value) {
        learning_indexes = value;

        int rows = getParent().getDataSet().rows();
        
        for(int i: getLearnIndexes()){
            if(i < 0 || i >= rows)
                throw new IllegalArgumentException("Learning indexes is out of badge");
        }
    }

    public void setCheckIndexes(int[] value) {
        check_indexes = value;

        int rows = getParent().getDataSet().rows();

        for(int i: getCheckIndexes()){
            if(i < 0 || i >= rows)
                throw new IllegalArgumentException("Learning indexes is out of badge");
        }
    }
    
    public PlaneModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, int[] learn, int[] check, long trainingTime) {
        super(dataset, trainingTime);
        this.colsin = colsin;
        this.colsout = colsout;
        setLearnIndexes(learn);
        setCheckIndexes(check);
    }

    public PlaneModel(BigInteger id, DataSetHeader parent) {
        super(id, parent);
    }

    public BitSet getColsIn() {
        return colsin;
    }

    public BitSet getColsOut() {
        return colsout;
    }

    protected void setColsin(BitSet colsin) {
        this.colsin = colsin;
    }

    protected void setColsout(BitSet colsout) {
        this.colsout = colsout;
    }

    /**
     * The number of parameters, used by model as input
     * @return
     */
    public int getParametersCount(){
        return Util.countFlags(getColsIn());
    }

    @Override
    public boolean equalCols(PlaneModel<T> model) {
        return model.getColsIn().equals(getColsIn());
    }

    protected String serializePlaneParameters() {
        return serializer.serializeParameters(getColsIn()) + "||" + serializer.serializeParameters(getColsOut());
    }

    protected void deSerializePlaneParameters(String in_and_out) {
        String[] sd = in_and_out.split("\\|\\|");
        if (sd.length < 2)
            throw ExceptionFactory.IllegalArgument(log, ExceptionFactory.MODEL_DESERIALIZATION_FORMAT, new Object[]{this.getClass(), getName()});

        deSerializePlaneParameters(sd[0], sd[1]);
    }

    protected void deSerializePlaneParameters(String in, String out) {
        List<BigInteger> i = serializer.parseBigIntegers(in);
        setColsin(getParent().generateBitSet(i));
        List<BigInteger> o = serializer.parseBigIntegers(out);
        setColsout(getParent().generateBitSet(o));
    }

    public double compareOut(DataRow input_vars_row) {
        Iterator iter = input_vars_row.iterator(getColsOut());

        if (!iter.hasNext())
            return Double.NaN;
        return getOut(input_vars_row) - (Double)iter.next();
    }

    protected PlaneModelSettings deserialize(String serialized_string, Class c){
        JAXBContext jc;
        PlaneModelSettings modelSettings = null;
        try {
            jc = JAXBContext.newInstance(new Class[] { c});
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            modelSettings = (PlaneModelSettings)unmarshaller.unmarshal(new StringReader(serialized_string));
        } catch (JAXBException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        setLastRowID(modelSettings.lastRowID);
        setTrainingTime(modelSettings.trainingTime);
        setLearnMemory(modelSettings.trainingMemory==0?-1:modelSettings.trainingMemory);
        deSerializePlaneParameters(modelSettings.colsin, modelSettings.colsout);
        return modelSettings;
    }

    public long getLearnMemory() {
        return learnMemory;
    }

    public void setLearnMemory(long learnMemory) {
        this.learnMemory = learnMemory;
    }
}

class PlaneModelSettings{
    @XmlElement(name = "LastRowID")
    public BigInteger lastRowID;
    @XmlElement(name = "TrainingTime")
    public long trainingTime;
    @XmlElement(name = "TrainingMemory")
    public long trainingMemory;

    @XmlElement(name = "InputParameters")
    public String colsin;
    @XmlElement(name = "OutputParameters")
    public String colsout;

    public PlaneModelSettings(){}

    public PlaneModelSettings(PlaneModel model){
        this.lastRowID = model.getLastRowID();
        this.trainingTime = model.getTrainingTime();
        this.trainingMemory = model.getLearnMemory();
        this.colsin = model.serializer.serializeParameters(model.getColsIn());
        this.colsout = model.serializer.serializeParameters(model.getColsOut());
    }

    public String serialize(){
        JAXBContext jc = null;
        String res = null;
        try {
            jc = JAXBContext.newInstance(new Class[] { getClass() });
            Marshaller marshaller = jc.createMarshaller();

            StringWriter sw = new StringWriter();

            marshaller.marshal(this, sw);
            res = sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return res;
    }
}
