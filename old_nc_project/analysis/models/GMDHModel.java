package com.netcracker.analysis.models;

import com.netcracker.analysis.data.DataSetHeader;

import java.math.BigInteger;
import java.util.BitSet;

public class GMDHModel extends LogisticLinearRegressionModel{

    public GMDHModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent, serializedData);
    }

    public GMDHModel(DataSetHeader parent, double[] w, BitSet colsin, BitSet colsout, long trainingTime) {
        super(parent, w, colsin, colsout, null, null, trainingTime);
    }


}
