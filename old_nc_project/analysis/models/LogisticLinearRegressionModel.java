package com.netcracker.analysis.models;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.util.Util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import static com.netcracker.analysis.util.Util.fd;

public class LogisticLinearRegressionModel extends PlaneModel<LogisticLinearRegressionModel> implements Model {

    private double[] paramCoefs;

    public LogisticLinearRegressionModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent);
        fillDataFromString(serializedData);
    }

    public LogisticLinearRegressionModel(DataSetHeader parent, double[] w, BitSet colsin, BitSet colsout, int[] learn, int[] check, long trainingTime) {
        super(parent, colsin, colsout, learn,  check, trainingTime);

        setParamCoefs(w);
    }

    public double[] getParamCoefs() {
        return paramCoefs;
    }

    public void setParamCoefs(double[] paramCoefs) {
        this.paramCoefs = paramCoefs;
    }

    /**
     * Get class of the point
     * @param input_vars_row - values of parameters
     * @return 1 - if good class
     *         0 - if bad class
     */
    public double getOut(DataRow input_vars_row) {
        int sum = 0;
        int mindex = 0;
        sum += getParamCoefs()[mindex++];

        for (int i = getColsIn().nextSetBit(0); i != -1; i = getColsIn().nextSetBit(i + 1))
            sum += input_vars_row.get(i) * getParamCoefs()[mindex++];

        return sum > 0 ? 1 : 0;
    }

    @Override
    public void fillDataFromString(String serializedData) {
        deserialize(serializedData, LogisticModelSettings.class);
    }

    @Override
    protected PlaneModelSettings deserialize(String serialized_string, Class c){
        LogisticModelSettings modelSettings = (LogisticModelSettings) super.deserialize(serialized_string, c);

        setParamCoefs(serializer.parseDoubles(modelSettings.paramCoefs));
        return modelSettings;
    }

    @Override
    public String serializeData() {
        String serialized = new LogisticModelSettings(this).serialize();
        return serialized;
    }

    public String getLegend() {
        double[] coefs = getParamCoefs();
        if (coefs == null || coefs.length == 0)
            return "Untrained";

        StringBuilder sum = new StringBuilder();
        int mindex = 0;
        sum.append(fd(coefs[mindex++])).append(" + ");

        List<DataSetParameter> paramheaders = getParent().getParameterList();

        for (int i = getColsIn().nextSetBit(0); i != -1; i = getColsIn().nextSetBit(i + 1))
            sum.append(fd(coefs[mindex++])).append(" * ").append(paramheaders.get(i).getName()).append(" + ");

        return sum.substring(0, sum.length()-3);
    }

    /**
     * Creastes a reduced parameter set
     * @param leaveParametersCount number of desired parameters number
     * @return BitSet representing the reduced parameters list
     */
    public BitSet reduceParameters (int leaveParametersCount){
        BitSet colsin = getColsIn();
        if (leaveParametersCount >= Util.countFlags(colsin))
            throw new IllegalArgumentException("leaveParametersCount less then parameters count");

        final BitSet result = new BitSet(colsin.size());

        class item implements Comparable<item>{
            public int index;
            public double value;
            public item(int index, double value){
                this.index = index;
                this.value = value;
            }

            public int compareTo(item o) {
                return Double.compare(value, o.value);
            }
        }

        final double[] pc = getParamCoefs();
        List<item> items = new ArrayList<item>(pc.length-1);
        int mindex = 1;
        for (int i = colsin.nextSetBit(0); i>-1; i = colsin.nextSetBit(i+1)){
            items.add(new item(i, pc[mindex++]));
        }

        Collections.sort(items);
        List<item> itemList = items.subList(items.size() - leaveParametersCount, items.size());
        for (item i : itemList){
            result.set(i.index);
        }

        assert Util.countFlags(result) == leaveParametersCount;
        return result;
    }
}

@XmlRootElement(name = "ModelSettings")
class LogisticModelSettings extends PlaneModelSettings {
    @XmlElement(name = "ParameterCoefs")
    public String paramCoefs;

    public LogisticModelSettings(){}

    public LogisticModelSettings(LogisticLinearRegressionModel model){
        super(model);
        paramCoefs = model.serializer.serializeArray(model.getParamCoefs());
    }
}
