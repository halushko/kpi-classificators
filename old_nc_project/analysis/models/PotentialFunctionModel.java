package com.netcracker.analysis.models;

import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class PotentialFunctionModel extends PlaneModel<PotentialFunctionModel>{
    private List<Point> points = new ArrayList<Point>();
    private PotentialFunction potential;
    private int learningPointsProcessed;

    public PotentialFunctionModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, int[] learn, int[] check) {
        this(dataset, colsin, colsout, learn,  check, new PotentialFunction1());
    }

    public PotentialFunctionModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, int[] learn, int[] check, PotentialFunction potentialFunction) {
        super(dataset, colsin, colsout, learn,  check, -1);
        potential = potentialFunction;
    }

    public PotentialFunctionModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent);
        fillDataFromString(serializedData);
    }

    public List<Point> getPoints() {
        return points;
    }

    public PotentialFunction getFunction() {
        return potential;
    }
    
    public int getLearningPointsProcessed(){
        return learningPointsProcessed;
    }

    public double getLearningFailPercentage(){
        return ((double)points.size()) / learningPointsProcessed;
    }

    /**
     * Вычисляем значение функции в точке Х
     * @param X точка
     * @return result
     */
    private double evaluate (DataRow X)
    {
        return evaluate(X, 0);
    }

    /**
     * Вычисляем значение функции в точке Х при виде ф-ии несколько шагов обучения назад
     * @param X точка
     * @param step количество шагов отката
     * @return result
     */
    private double evaluate(DataRow X, int step)
    {
        if (step > points.size())
            return 0;

        double val = 0;
        List<Point> ps = step == 0 ? points : points.subList(0, points.size() - step);
        for (Point p : ps)
        {
            val += p.sign*potential.evaluate(X, p.getX(), getColsIn());
        }
        return val;
    }

    /**
     * Вычисляем следующее приближение функции
     * @param X новая точка
     * @return if the function was changed
     */
    public boolean next (DataRow X)
    {
        int sign = X.get(getColsOut().nextSetBit(0)) > 0.5 /*calibrated value*/ ? 1 : -1;
        return next(X, sign);
    }

    /**
     * Вычисляем следующее приближение функции
     * @param X новая точка
     * @param sign образ
     * @return if the function was changed
     */
    public boolean next (DataRow X, int sign)
    {
        learningPointsProcessed++;
        double f = evaluate(X);
        if (f*sign > 0)    // no mistake
            return false;

        points.add(new Point(X, sign));
        return true;
    }

    @Override
    public void fillDataFromString(String serializedData) {
        deserialize(serializedData, PotentialFunctionsModelSettings.class);
    }

    @Override
    protected PlaneModelSettings deserialize(String serialized_string, Class c){
        PotentialFunctionsModelSettings modelSettings = (PotentialFunctionsModelSettings) super.deserialize(serialized_string, c);

        learningPointsProcessed = modelSettings.learningPointsProcessed;
        String[] ss = modelSettings.points.split("\\|");
        int[] signs = new int[ss.length];
        BigInteger[] ids = new BigInteger[ss.length];
        for (int i = 0; i<ss.length; i++){
            int sign = ss[i].charAt(0) == '-' ? -1 : +1;
            BigInteger id = serializer.parseBigInt(ss[i].substring(1));
            signs[i] = sign;
            ids[i] = id;
        }
        List<DataRow> drs = getParent().getParameters(getParent(), ids);
        
        for (int i = 0; i< ids.length; i++){
            int index = 0;
            for (; index < ids.length; index++){
                if (ids[i].equals(drs.get(index).getId()))
                    break;
            }
            if (index<ids.length){
                points.add(new Point(drs.get(index), signs[i]));
            }
        }

        potential = new PotentialFunction1(Double.parseDouble(modelSettings.potentialFunction));

        return modelSettings;
    }

    @Override
    public String serializeData() {
        String serialized = new PotentialFunctionsModelSettings(this).serialize();
        return serialized;
    }

    public double getOut(DataRow input_vars_row) {
        return evaluate(input_vars_row) < 0 ? 0 : 1;
    }

    public String getLegend() {
//        StringBuilder sb = new StringBuilder();
//        for (Point p : points){
//            sb.append(p.sign + '_').append(p.X.getId()).append(" ");
//        }
//        return sb.toString();

        StringBuilder sb = new StringBuilder();
        for (Point p : points){
            sb.append(p.sign > 0 ? "+" : "-");
            sb.append(MessageFormat.format("1/(1 + {0}*{1})", ((PotentialFunction1)potential).alpha, getDistance2String(p.getX(), getColsIn())));
            sb.append(" ");
        }
        return sb.toString();
    }

    String getDistance2String (DataRow X, BitSet colsin){
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (int i = colsin.nextSetBit(0); i!=-1; i=colsin.nextSetBit(i+1)){
            sb.append(MessageFormat.format("({0} - {1})^2", X.get(i), getParent().getParameterList().get(i).getName()));
            sb.append(" + ");
        }
        return sb.substring(0, sb.length()-3) + ")";
    }

    public class Point
    {
        public Point(DataRow X, int sign)
        {
            this.X = X;
            datarowID = X.getId();
            this.sign = sign;
        }

        public DataRow getX(){
            if (X == null){
                List<DataRow> dr = DaoFactory.DATA_ROW_DAO.getRows(getParent(), new BigInteger[]{datarowID});
                if (dr.size() == 0) 
                    return null;    // throw exception?
                X = dr.get(0);
            }
            return X;            
        }

        public final int sign;

        private BigInteger datarowID;
        private DataRow X;
        
        @Override
        public String toString(){
            return (sign == -1 ? "-" : "+") + datarowID;
        }
    }

    public static abstract class PotentialFunction
    {
        public abstract double evaluate(DataRow X, DataRow Y, BitSet colsin);

        /**
         * Distance between X and Y points, squered
         * @param X
         * @param Y
         * @param colsin
         * @return
         */
        double getDistance2 (DataRow X, DataRow Y, BitSet colsin){
            double dist = 0;
            for (int i = colsin.nextSetBit(0); i!=-1; i=colsin.nextSetBit(i+1)){
                dist += Math.pow(X.get(i) - Y.get(i), 2);
            }
            return dist;
        }
    }

    public static class PotentialFunction1 extends PotentialFunction {
        private double alpha;

        public PotentialFunction1(){
            this(10);
        }

        public PotentialFunction1(double alpha) {
            this.alpha = alpha;
        }

        public double getAlpha(){
            return alpha;
        }

        @Override
        public double evaluate(DataRow X, DataRow Y, BitSet colsin){
            return 1/(1 + alpha*getDistance2(X, Y, colsin));
        }
    }
}

@XmlRootElement(name = "ModelSettings")
class PotentialFunctionsModelSettings extends PlaneModelSettings {
    @XmlElement(name = "learningPointsProcessed")
    public int learningPointsProcessed;
    @XmlElement(name = "Points")
    public String points;

    @XmlElement(name = "PotentialFunction")
    public String potentialFunction;

    public PotentialFunctionsModelSettings(){}

    public PotentialFunctionsModelSettings(PotentialFunctionModel model){
        super(model);
        this.points = model.serializer.serializeList(model.getPoints());
        this.learningPointsProcessed = model.getLearningPointsProcessed();
        this.potentialFunction = ""+((PotentialFunctionModel.PotentialFunction1)model.getFunction()).getAlpha();
    }
}