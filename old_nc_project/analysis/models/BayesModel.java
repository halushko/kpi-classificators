package com.netcracker.analysis.models;

import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.BitSet;

public class BayesModel extends PlaneModel<BayesModel>{

    private double[][] avg;
    private double[][] disp;

    public BayesModel(BigInteger id, DataSetHeader parent, String serializedData) {
        super(id, parent);
        fillDataFromString(serializedData);
    }

    public BayesModel(DataSetHeader parent, double[][] avg, double[][] disp, BitSet colsin, BitSet colsout, int[] learn, int[] check, long trainingTime) {
        super(parent, colsin, colsout, learn,  check, trainingTime);
        setAvg(avg);
        setDisp(disp);
    }

    public double[][] getAvg() {
        return avg;
    }

    public void setAvg(double[][] avg) {
        this.avg = avg;
    }

    public double[][] getDisp() {
        return disp;
    }

    public void setDisp(double[][] disp) {
        this.disp = disp;
    }
    
    private double[][] fillMatrixFromString(String serializedData){
        String[] sd = serializedData.split("\\|\\|");
        
        if(sd.length != 2)
            throw ExceptionFactory.IllegalArgument(log, ExceptionFactory.MODEL_DESERIALIZATION_FORMAT, new Object[]{this.getClass(), getName()});
        
        double [][] res = new double[sd.length][];
        for(int i = 0; i < sd.length; i++)
            res[i] = serializer.parseDoubles(sd[i]);
        return res;
    }

    @Override
    public void fillDataFromString(String serializedData) {
//        String[] sd = serializedData.split("\\|\\|\\|\\|");
//        if(sd.length != 3)
//            throw ExceptionFactory.IllegalArgument(ExceptionFactory.MODEL_DESERIALIZATION_FORMAT, new Object[]{this.getClass(), getName()}, log);
//        setDisp(fillMatrixFromString(sd[0]));
//        setAvg(fillMatrixFromString(sd[1]));
//
//        deSerializePlaneParameters(sd[2]);
        deserialize(serializedData, BayesModelSettings.class);
    }


    @Override
    public String serializeData() {
//        String serialized = serializeArray(getDisp()[0]) + "||" + serializeArray(getDisp()[1]) + "||||" + serializeArray(getAvg()[0]) + "||" + serializeArray(getAvg()[1]);
//        serialized += "||||" + serializePlaneParameters();
        String serialized = new BayesModelSettings(this).serialize();
        return serialized;
    }

        @Override
    protected PlaneModelSettings deserialize(String serialized_string, Class c){
        BayesModelSettings modelSettings = (BayesModelSettings) super.deserialize(serialized_string, c);

        setDisp(fillMatrixFromString(modelSettings.disp));
        setAvg(fillMatrixFromString(modelSettings.avg));
        return modelSettings;
    }

    public double getOut(DataRow input_vars_row) {
        BitSet colsin = getColsIn();
        int count = Util.countFlags(colsin);
        double res0=1.0, res1 = 1.0;
        double[][] avg = getAvg();
        double[][] disp = getDisp();

        int m = colsin.nextSetBit(0);
        for(int i = 0; i < count; i++, m = colsin.nextSetBit(m + 1)){
            res0 *= Math.exp( Math.pow(input_vars_row.get(m)-avg[0][i], 2)/(2*disp[0][i]))/Math.pow(disp[0][i], 0.5);
            res1 *= Math.exp( Math.pow(input_vars_row.get(m)-avg[1][i], 2)/(2*disp[1][i]))/Math.pow(disp[1][i], 0.5);
        }

        return res0 < res1 ? 0 : 1;
    }

    public String getLegend() {
        BayesModelSettings bsms = new BayesModelSettings(this);
        return MessageFormat.format("[{0}<||>{1}", bsms.disp, bsms.avg);
    }
}

@XmlRootElement(name = "ModelSettings")
class BayesModelSettings extends PlaneModelSettings {
    @XmlElement(name = "disp")
    public String disp;
    @XmlElement(name = "avg")
    public String avg;

    public BayesModelSettings(){}

    public BayesModelSettings(BayesModel model){
        super(model);
        disp = model.serializer.serializeArray(model.getDisp()[0]) + "||" + model.serializer.serializeArray(model.getDisp()[1]);
        avg  = model.serializer.serializeArray(model.getAvg()[0]) + "||" + model.serializer.serializeArray(model.getAvg()[1]);
    }
}
