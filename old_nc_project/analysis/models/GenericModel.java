package com.netcracker.analysis.models;

import com.netcracker.analysis.HasId;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.Transient;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public abstract class GenericModel<T extends GenericModel<T>> implements Model, HasId {

    protected Log log = LogFactory.getLog(getClass());
    protected final Serializer serializer = new Serializer();

    private DataSetHeader parent;
    private BigInteger id;

    private BigInteger lastRowID;
    private long trainingTime;
    private String name;
    private boolean isInvalid;

    public GenericModel(DataSetHeader dataset, long trainingTime) {
        this(Util.getID(), dataset);
        this.trainingTime = trainingTime;
    }

    public GenericModel(BigInteger id, DataSetHeader parent) {
        this.id = id;
        this.parent = parent;
        name = parent.getName() + "_" + getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+ "{" +
                id +
                "," +
                name +
                '}';
    }

    public BigInteger getId() {
        return id;
    }

    public void isInvalid(boolean valid){
        isInvalid = valid;
    }

    public boolean isInvalid(){
        return isInvalid;
    }

    @Transient
    public DataSetHeader getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getLastRowID() {
        return lastRowID;
    }

    public void setLastRowID(BigInteger lastRowID) {
        this.lastRowID = lastRowID;
    }

    public long getTrainingTime() {
        return trainingTime;
    }

    public void setTrainingTime(long trainingTime) {
        this.trainingTime = trainingTime;
    }

    public String getType(){
        return getClass().getSimpleName();
    }

    public abstract void fillDataFromString(String serializedData);

    public abstract String serializeData();

    protected BigInteger parseBigInt(String id) {
        return serializer.parseBigInt(id);
    }

    public String getIdString() {
        return getId().toString();
    }

    public abstract boolean equalCols (T model);

    public class Serializer {
        /**
         * Constructs a string of parameters IDs which has format 'bi|bi|bi'
         * @param parameters a bitset of used parameters
         */
        protected String serializeParameters(BitSet parameters) {
            int realParametersCount = getParent().getRealParametersCount();

            List<BigInteger> parametersIDs = getParent().getParametersIDs(parameters.get(0, realParametersCount));
            String realstr = serializeList(parametersIDs);

            // multiplied parameters
            if (parameters.nextSetBit(realParametersCount) > -1) {
                List<DataSetParameter> pl = getParent().getParameterList();

                for (int i = parameters.nextSetBit(0); i > -1; i = parameters.nextSetBit(i + 1)) {
                    DataSetParameter dsp = pl.get(i);
                    int[] b = dsp.getIncludedParameters();
                    parametersIDs = getParent().getParametersIDs(b);
                    String ser = serializeList(parametersIDs).replace("|", "*");
                    realstr += "|" + ser;
                }
            }
            return realstr;
        }

        /**
         * Constructs a string of objects which has format 'v|v|v'
         * @param vars a list of doubles
         */
        protected String serializeList(List vars) {
            if (vars == null || vars.size() == 0)
                return "";

            StringBuilder sb = new StringBuilder();
            for (Object p : vars) {
                sb.append(p);
                sb.append("|");
            }
            return sb.substring(0, sb.length() - 1);
        }

        /**
         * Constructs a string of doubles which has format 'd|d|d'
         * @param vars an array of doubles
         */
        protected String serializeArray(double[] vars) {
            if (vars == null || vars.length == 0)
                return "";

            StringBuilder sb = new StringBuilder();
            for (Object p : vars) {
                sb.append(p);
                sb.append("|");
            }
            return sb.substring(0, sb.length() - 1);
        }

        /**
         * Parses a string of doubles which has format 'd|d|d'
         * @param doubles_string a string of doubles which has format 'd|d|d'
         */
        protected double[] parseDoubles(String doubles_string) {
            String[] doubles = doubles_string.split("\\|");
            double[] array = new double[doubles.length];
            for (int i = 0; i < doubles.length; i++) {
                try {
                    double d = Double.parseDouble(doubles[i]);
                    array[i] = d;
                } catch (NumberFormatException e) {
                    throw ExceptionFactory.IllegalArgument(log, ExceptionFactory.MODEL_DESERIALIZATION_WRONG_DOUBLE, new Object[]{getClass(), getName(), doubles[i]});
                }
            }
            return array;
        }

        /**
         * Parses a string of bigints which has format 'd|d|d*d|d'
         * @param ids_string a string of bigints which has format 'd|d|d'
         */
        protected List<BigInteger> parseBigIntegers(String ids_string) {
            String[] ids = ids_string.split("\\|");
            List<BigInteger> array = new ArrayList<BigInteger>();
            for (String id : ids) {
                if (id.contains("*")) {
                    String[] ids2 = id.split("\\*");
                    int number = 1;
                    for (String id2 : ids2) {
                        number *= getParent().getParameter(parseBigInt(id2)).getPrime_number();
                    }
                    DataSetParameter parameter = getParent().addColumn(number); // finds or adds a column, which is a multiplication of the given
                    array.add(parameter.getId());
                } else
                    array.add(parseBigInt(id));
            }
            return array;
        }

        protected BigInteger parseBigInt(String id) {
            try {
                BigInteger d = new BigInteger(id);
                return d;
            } catch (NumberFormatException e) {
                throw ExceptionFactory.IllegalArgument(log, ExceptionFactory.MODEL_DESERIALIZATION_WRONG_BIGINT, new Object[]{getClass(), getName(), id});
            }
        }
    }
}
