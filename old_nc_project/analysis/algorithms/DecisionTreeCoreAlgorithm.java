package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.PlaneModel;

import java.util.BitSet;

public interface DecisionTreeCoreAlgorithm {
    PlaneModel createModel(DataSetHeader data, BitSet colsin, BitSet colsout, int[] learn, int[] check);
}
