package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.models.PotentialFunctionModel;
import com.netcracker.analysis.util.Timer;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;

import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class PotentialFunctionAlgorithm extends PlaneModelAlgorithm <PotentialFunctionModel> implements DecisionTreeCoreAlgorithm {

    public AbstractSamplingGetter samplingGetter;

    private PotentialFunctionModel.PotentialFunction potentialFunction;
    
    public PotentialFunctionAlgorithm(){
        samplingGetter = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, 1);
    }

    public PotentialFunctionAlgorithm(PotentialFunctionModel.PotentialFunction potentialFunction){
        this();
        this.potentialFunction = potentialFunction;
    }

    public PotentialFunctionModel.PotentialFunction getPotentialFunction() {
        return potentialFunction;
    }

    public void setPotentialFunction(PotentialFunctionModel.PotentialFunction potentialFunction) {
        this.potentialFunction = potentialFunction;
    }

    public AbstractSamplingGetter getSamplingGetter() {
        return samplingGetter;
    }

    public void setSamplingGetter(AbstractSamplingGetter samplingGetter) {
        this.samplingGetter = samplingGetter;
    }

    public Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter) {
        logStartLearning();
        Timer timer = new Timer();

        PlaneModel model = createModel(dataset, colsin, colsout, samplingGetter.getLearn_indexes(), samplingGetter.getCheck_indexes());

        model.setTrainingTime(timer.stop());
        logEndLearning(timer.getStopTicks());

        return model;
    }

    public PlaneModel createModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, int[] learn, int[] check) {
        PotentialFunctionModel model = new PotentialFunctionModel(dataset, colsin, colsout, learn, check, potentialFunction);
        model.setName(model.getName() + "_" + ((PotentialFunctionModel.PotentialFunction1)model.getFunction()).getAlpha());

        DataSet dataSet = dataset.getDataSet();
        for (int i : learn){
            DataRow dr = dataSet.getRow(i);
            boolean model_changed = model.next(dr);
        }

        model.setLastRowID(dataSet.getRow(dataSet.rows()-1).getId());
        return model;
    }

    @Override
    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, AbstractSamplingGetter samplingGetter) {
            List<PotentialFunctionModel> originalModels = new ArrayList<PotentialFunctionModel>();
            Class c = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            for (Model m : models){
                if (!(c.isInstance(m)))
                    continue;
                PotentialFunctionModel model = (PotentialFunctionModel) m;
                originalModels.add(model);
            }

        BigInteger lastLearnedRow = BigInteger.valueOf(Long.MAX_VALUE);
        for (PotentialFunctionModel om : originalModels){
            if (lastLearnedRow.compareTo(om.getLastRowID())>0)
                lastLearnedRow = om.getLastRowID();
        }

        DataSet dataSet = dataset.getDataSet();
        int rowindex = -1;
        for (int i = 0; i < dataSet.rows(); i++){
            if (dataSet.getRow(i).getId().compareTo(lastLearnedRow)<=0)
                continue;
            rowindex = i;
            break;
        }

        if (rowindex == -1)
            return (List) originalModels; // no data to learn on

        for (PotentialFunctionModel model : originalModels){
            int mrowindex = -1;
            for (int i = rowindex; i < dataSet.rows(); i++){
                if (dataSet.getRow(i).getId().compareTo(lastLearnedRow)<=0)
                    continue;
                mrowindex = i;
                break;
            }

            for (int i = mrowindex; i < dataSet.rows(); i++){
                DataRow dr = dataSet.getRow(i);
                boolean model_changed = model.next(dr);
            }
        }

        return (List) originalModels;
    }
}
