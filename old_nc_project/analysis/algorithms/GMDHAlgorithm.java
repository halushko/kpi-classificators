package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.LearningParameters;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.util.Matrix;
import com.netcracker.analysis.util.PrimeNumbers;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

public class GMDHAlgorithm implements Algorithm {

    public AbstractSamplingGetter _samplingGetter = AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.ALL, 1);
    private int[] _learn_rows, _check_rows;

    public AbstractSamplingGetter getSamplingGetter() {
        return _samplingGetter;
    }
    public int[] get_check_rows() {
        return _check_rows;
    }
    public int[] get_learn_rows() {
        return _learn_rows;
    }

    public void setSamplingGetter(AbstractSamplingGetter samplingGetter) {
        this._samplingGetter = samplingGetter;
    }

    public Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout) {
        int dimension = Util.countFlags(colsin)-1;
        List<Integer> dataSetNumbers = AlgUtils.getDataSetParametersPrimes(dataset, colsin);
        List<Integer> primes = AlgUtils.getExtendedDataColumnsPrimeNumbers(dataSetNumbers, dimension, 3, 3);

        Matrix m = Util.getMatrix(dataset.getDataSet(), colsin, null);
        Matrix y = Util.getMatrix(dataset.getDataSet(), colsout, null);

        doStep(m, dataSetNumbers, primes, y);


         return null;
    }

    private LModel doStep(Matrix m, List<Integer> sourceprimes, List<Integer> primes, Matrix y) {
        List<LModel> models = new ArrayList<LModel>();
        for (int i : primes){
            int[] elements = PrimeNumbers.getPrimes(i);
            LModel model = buildLinearModel(m, sourceprimes, elements, y);
            models.add(model);
        }
        Collections.sort(models);
        if (models.size() > 3)
            models = models.subList(0, (int) (models.size()*0.7));  // take N% of models for next step
        else
            return models.get(0);

        // next step preparation
        List<Integer> modelints = new ArrayList<Integer>(models.size());
        Matrix modelsmatrix = new Matrix(m.rows(), models.size());
        for (int i = 0; i< models.size(); i++){
            modelints.add(PrimeNumbers.get(i));
            for (int j = 0; j< m.rows(); j++){
                modelsmatrix.set(j, i, models.get(i).getOut(models.get(i).matrix.getRow(j)));
            }
        }
        List<Integer> modelprimes = AlgUtils.getExtendedDataColumnsPrimeNumbers(modelints, modelints.size()-1, 3, 3);

        LModel resModel = doStep(modelsmatrix, modelints, modelprimes, y);
        //resModel.paramCoefs

        return null;
    }

    private LModel buildLinearModel(Matrix matrix, List<Integer> sourcePrimes, int[] elements, Matrix y) {
        Matrix modelmatr = new Matrix(matrix.rows(), 3);

        // v = A*x0 + B*x1*x2
        for (int j = 0; j< matrix.rows(); j++){
            modelmatr.set(j, 0, matrix.get(j, sourcePrimes.indexOf(elements[0])));
            modelmatr.set(j, 1, matrix.get(j, sourcePrimes.indexOf(elements[1]))*matrix.get(j, sourcePrimes.indexOf(elements[2])));
        }

        Matrix X = Matrix.GetElements(modelmatr, getSamplingGetter().getLearn_indexes(), false);
        Matrix Y = Matrix.GetElements(modelmatr, getSamplingGetter().getLearn_indexes(), false);

        Matrix W = GetWeights(X, Y, 80);

        double[] weights = W.getColumn(0);

        LModel m = new LModel(X, elements, weights);
        m.count_adequacy(getSamplingGetter().getCheck_indexes(), m, y);

        return m;
    }

    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Model buildModel(LearningParameters params) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


    private Matrix GetWeights(Matrix X, Matrix Y, int max_iteration){

        Matrix W = X.pinv().times(Y);
        double min_epsilon = 100000;
        Matrix W_min = new Matrix(W);

        while(--max_iteration>0){
            Matrix t = Matrix.eye(W.rows()).mult(0.003);
            Matrix Z = X.times(W);
            Matrix P = new Matrix(Z);
            Matrix G = new Matrix(Z.rows(), Z.rows());
            Matrix U = new Matrix(Z);
            for(int i = 0; i < Z.rows(); i++){
                double z = Z.get(i, 0);
                double p = 1.0/(1 + Math.exp(-z));
                double g = p*(1-p);
                double u = z + (Y.get(i, 0)-p)/g;
                P.set(i, 0, p);
                G.set(i, i, g);
                U.set(i, 0, u);
            }
            Matrix W_new =
                    (X.transpose().times(G).times(X).plus(t)).getInverse().times(X.transpose()).times(G).times(U);

            double curent_epsilone = MNK(W, W_new);
            if( ((Double)curent_epsilone).isNaN() || ((Double)curent_epsilone).isInfinite())
            {
                W = W_min;
                break;
            }
            if(curent_epsilone < min_epsilon){
                W_min = W_new;
                min_epsilon = curent_epsilone;
            }
            W = W_new;
            if(curent_epsilone <= EPSILON) break;
        }

        return W;
    }
    private double MNK(Matrix W, Matrix W_new){
        double curent_epsilone = 0;
        for(int i = 0; i < W.rows(); i++){
            curent_epsilone+= Math.pow(W_new.get(i, 0) - W.get(i, 0), 2);
        }
        return curent_epsilone;
    }

    public final double EPSILON = 0.00001;

    @Override
    public Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, AbstractSamplingGetter samplingGetter) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Model buildModel(LearningParameters params, AbstractSamplingGetter samplingGetter) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


    class LModel implements Comparable<LModel>{
        Matrix matrix;
        int[] primes;
        double[] paramCoefs;
        double adequacy;

        public LModel(Matrix matrix, int[] primes, double[] paramCoefs){
            this.primes = primes;
            this.matrix = matrix;
            this.paramCoefs = paramCoefs;
        }

        public double getOut(double[] input_vars_row) {
            int sum = 0;
            int mindex = 0;

            sum += paramCoefs[mindex++];

            for (double i:input_vars_row)
                sum += i * paramCoefs[mindex++];

            return sum > 0 ? 1 : 0;
        }

        double count_adequacy(int[] indexes, LModel model, Matrix y){
            double res = 0;
            for(int i = 0; i < indexes.length; i++){
                double[] dr = model.matrix.getRow(indexes[i]);
                double a = model.getOut(dr);
                Double b = y.get(i, 0);
                res += Math.abs(a - b);
            }

            adequacy = 1.0 - res/indexes.length;
            return adequacy;
        }

        @Override
        public int compareTo(LModel o) {
            return -Double.compare(adequacy, o.adequacy);
        }
    }
}
