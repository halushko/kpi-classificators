package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.estimation.estimators.AbstractEstimate;
import com.netcracker.analysis.models.LogisticLinearRegressionModel;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Matrix;
import com.netcracker.analysis.util.Timer;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;

public class LogisticLinearRegressionAlgorithm extends PlaneModelAlgorithm<LogisticLinearRegressionModel> 
        implements Algorithm, DecisionTreeCoreAlgorithm{


    public Model buildModel(DataSetHeader data, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter) {
        logStartLearning();
        Timer timer = new Timer();

        Matrix X, Y;


        int[] learn = samplingGetter.getLearn_indexes(), check = samplingGetter.getCheck_indexes();

        LogisticLinearRegressionModel model = (LogisticLinearRegressionModel) createModel(data, colsin, colsout, learn, check);

        model.setTrainingTime(timer.stop());
        logEndLearning(timer.getStopTicks());

        model.isInvalid(!checkModel(data, colsin, colsout, check, model));
        if (model.isInvalid())
            log.warn("Model validation failed");
        return model;
    }

    public boolean checkModel(DataSetHeader data, BitSet colsin, BitSet colsout, int[] check, LogisticLinearRegressionModel model) {
        AbstractEstimate validator = AbstractEstimate.GenerateEstimate(data, colsin, colsout, check, ESTIMATION_TYPE.ADEQUACY);
        return ((Double) validator.estimate(model)) > 1 - ALLOWABLE_ERROR;
    }

    public PlaneModel createModel(DataSetHeader data, BitSet colsin, BitSet colsout, int[] learn, int[] check) {
        Matrix X;
        Matrix Y;
        int[] all = new int[data.getDataSet().rows()];
        for(int i = 0; i < all.length; i++)
            all[i] = i;

        X = Util.getMatrix(data.getDataSet(), colsin, all);
        Y = Util.getMatrix(data.getDataSet(), colsout, all);

        X = Matrix.GetElements(X, learn, true);
        Y = Matrix.GetElements(Y, learn, false);

        Matrix W = GetWeights(X, Y, getMaxIterationsCount());

        return new LogisticLinearRegressionModel(data, W.getColumn(0), colsin, colsout, learn, check, -1);
    }

    public double getEpsilon() {
        return _epsilon;
    }
    public int getMaxIterationsCount() {
        return max_iterations;
    }
    
    public void setAllowableError(double allowable_error) {
        if(allowable_error < ALLOWABLE_ERROR)
            this._allowable_error = ALLOWABLE_ERROR;
        else
            this._allowable_error = allowable_error;
    }
    public void setEpsilon(double epsilon) {
        if(epsilon < EPSILON)
            this._epsilon = EPSILON;
        else
            this._epsilon = epsilon;
    }
    public void setMaxIterationsCount(int value) {
        if(max_iterations > MAX_ITERATIONS_COUNT)
            this.max_iterations = MAX_ITERATIONS_COUNT;
        else
            this.max_iterations = value;
    }
    
    public LogisticLinearRegressionAlgorithm(){}
        

    private Matrix GetWeights(Matrix X, Matrix Y, int max_iteration){
        
        Matrix W = X.pinv().times(Y);
        double min_epsilon = 100000;
        Matrix W_min = new Matrix(W);

        while(--max_iteration>0){
            Matrix t = Matrix.eye(W.rows()).mult(0.003);
            Matrix Z = X.times(W);
            Matrix P = new Matrix(Z);
            Matrix G = new Matrix(Z.rows(), Z.rows());
            Matrix U = new Matrix(Z);
            for(int i = 0; i < Z.rows(); i++){
                double z = Z.get(i, 0);
                double p = 1.0/(1 + Math.exp(-z));
                double g = p*(1-p);
                double u = z + (Y.get(i, 0)-p)/g;
                P.set(i, 0, p);
                G.set(i, i, g);
                U.set(i, 0, u);
            }
            Matrix W_new =
                    (X.transpose().times(G).times(X).plus(t)).getInverse().times(X.transpose()).times(G).times(U);

            double curent_epsilone = MNK(W, W_new);
            if( ((Double)curent_epsilone).isNaN() || ((Double)curent_epsilone).isInfinite())
            {
                W = W_min;
                break;
            }
            if(curent_epsilone < min_epsilon){
                W_min = W_new;
                min_epsilon = curent_epsilone;
            }
            //System.out.println(curent_epsilone);
            W = W_new;
            if(curent_epsilone <= getEpsilon()) break;
        }

        return W;
    }
    private static double MNK(Matrix W, Matrix W_new){
        double curent_epsilone = 0;
            for(int i = 0; i < W.rows(); i++){
                curent_epsilone+= Math.pow(W_new.get(i, 0) - W.get(i, 0), 2);
            }
        return curent_epsilone;
    }


    public final double ALLOWABLE_ERROR = 0.30;
    public final double EPSILON = 0.00001;
    public final int MAX_ITERATIONS_COUNT = 100;

    private double _allowable_error = 0.30;
    private double _epsilon = 0.0001;
    private int max_iterations = 100;
}
