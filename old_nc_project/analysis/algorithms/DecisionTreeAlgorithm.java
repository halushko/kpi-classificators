package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.DecisionTreeModel;
import com.netcracker.analysis.models.PotentialFunctionModel;
import com.netcracker.analysis.util.Timer;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;

public class DecisionTreeAlgorithm extends PlaneModelAlgorithm<DecisionTreeModel> implements Algorithm {

    private DecisionTreeCoreAlgorithm algorithm;

    public DecisionTreeAlgorithm(DecisionTreeCoreAlgorithm algorithm){
        this.algorithm = algorithm;
    }

    public DecisionTreeAlgorithm (){
        this(new LogisticLinearRegressionAlgorithm());
    }

    public DecisionTreeCoreAlgorithm getAlgorithm() {
        return algorithm;
    }

    public Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter) {
        Timer timer = new Timer();

        DecisionTreeModel m = new DecisionTreeModel(new DecisionTree(dataset,  colsin, colsout, samplingGetter.getLearn_indexes(), algorithm), samplingGetter.getLearn_indexes(), samplingGetter.getCheck_indexes());

        String name = algorithm instanceof PotentialFunctionAlgorithm
                            ? ("_P_" + ((PotentialFunctionModel.PotentialFunction1)((PotentialFunctionAlgorithm)algorithm).getPotentialFunction()).getAlpha())
                            :  ("_"+algorithm.getClass().getSimpleName().charAt(0));
        m.setName(m.getName()+name);

        m.setTrainingTime(timer.stop());
        return m;
    }
}
