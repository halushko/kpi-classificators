package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.util.PrimeNumbers;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class AlgUtils {

    public static List<Integer> getDataSetParametersPrimes(DataSetHeader dataSetHeader, BitSet colsin) {
        List<DataSetParameter> params = dataSetHeader.getParameterList();
        ArrayList<Integer> l = new ArrayList<Integer>();
        for (int i = colsin.nextSetBit(0); i != -1; i = colsin.nextSetBit(i + 1)) {
            l.add(params.get(i).getPrime_number());
        }
        return l;
    }

    public static List<Integer> getExtendedDataColumnsPrimeNumbers(List<Integer> dataSetNumbers, int dimension, int degree) {
        return getExtendedDataColumnsPrimeNumbers(dataSetNumbers, dimension, degree, 2);
    }

    public static List<Integer> getExtendedDataColumnsPrimeNumbers(List<Integer> dataSetNumbers, int dimension, int degree, int mindegree) {
        List<Integer> plainNumbers = new ArrayList<Integer>();

        for (int i = mindegree; i <= degree; i++) {
            int[] counters = new int[i];
            for (int j = 0; j < i; j++) {
                counters[j] = 0;
            }
            while (true) {
                plainNumbers.add(GetProduct(dataSetNumbers, counters));

                int tailOverflow = i - 1;
                while (tailOverflow >= 0 && counters[tailOverflow] == dimension) {
                    tailOverflow--;
                }
                if (tailOverflow == -1) {
                    break;
                }
                for (int k = tailOverflow + 1; k < i; k++) {
                    counters[k] = counters[tailOverflow] + 1;
                }
                counters[tailOverflow]++;
            }
        }

        return plainNumbers;
    }

    public static List<Integer> getPrimeNumbersOfDegree(List<Integer> primeNumbers, int mindegree, int maxdegree) {
        List<Integer> ret = new ArrayList<Integer>();
        for (int pn : primeNumbers) {
            int count = PrimeNumbers.getIndexes(pn).length;
            if (count >= mindegree && count <= maxdegree)
                ret.add(pn);
        }
        return ret;
    }

    private static Integer GetProduct(List<Integer> dataSetNumbers, int[] counters) {
        int rez = 1;
        for (int j = 0; j < counters.length; j++) {
            rez *= dataSetNumbers.get(counters[j]);
        }
        return rez;
    }
}
