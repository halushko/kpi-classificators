package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.LearningParameters;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class PlaneModelAlgorithm<T extends PlaneModel<T>> implements Algorithm {

    public Model buildModel(LearningParameters params, AbstractSamplingGetter samplingGetter) {
        return buildModel(params.getDataSet(),params.getColsIn(),params.getColsOut(), samplingGetter);
    }

    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, AbstractSamplingGetter samplingGetter) {
        List<T> originalModels = getOriginalModels(models);

        List<Model> retrainedModels = new ArrayList<Model>();
        for (T om : originalModels){
            Model m = buildModel(dataset, om.getColsIn(), om.getColsOut(), samplingGetter);
            if (m == null){
                continue;
            }
            retrainedModels.add(m);
        }
        return retrainedModels;
    }

    protected <T extends PlaneModel<T>> List<T> getOriginalModels(List<Model> models) {
        List<T> originalModels = new ArrayList<T>();
        Class c = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        for (Model m : models){
            if (!(c.isInstance(m)))
                continue;
            T model = (T) m;
            boolean isOriginal = true;
            for (T om : originalModels){
                if (model.equalCols(om)) {
                    isOriginal = false;
                    break;
                }
            }
            if (!isOriginal) continue;

            originalModels.add(model);
        }
        return originalModels;
    }

    protected Log log = LogFactory.getLog(getClass());

    protected void logStartLearning(){
        //log.info(getClass().getSimpleName() + " started learning model");
    }

    protected void logEndLearning(long ticks){
        //log.info(getClass().getSimpleName() + " ended learning model in " + ticks + " ms");
    }
    
    public enum TYPES{
        BAYES, LOGISTIC_LINEAR, NONLINEAR, POTENTIAL
    }
}
