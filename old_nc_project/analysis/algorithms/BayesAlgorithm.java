package com.netcracker.analysis.algorithms;


import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.BayesModel;
import com.netcracker.analysis.util.Matrix;
import com.netcracker.analysis.util.Timer;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.BitSet;

public class BayesAlgorithm extends PlaneModelAlgorithm<BayesModel> implements Algorithm {

    public Model buildModel(DataSetHeader dataSetHeader , BitSet cols_in, BitSet cols_out, AbstractSamplingGetter samplingGetter) {
        logStartLearning();
        Timer timer = new Timer();

        int[] learnIndexes = samplingGetter.getLearn_indexes();
        int[] checkIndexes = samplingGetter.getCheck_indexes();

        DataSet dataSet = dataSetHeader.getDataSet();
        
        Matrix X = Util.getMatrix(dataSet, cols_in, learnIndexes);
        Matrix Y = Util.getMatrix(dataSet, cols_out, learnIndexes);
        
        int[] member = GetMembershipToClass(Y);

        double[][] avg = GetAvgs(X, member);
        double[][] disp = GetDisp(X, avg, member);

        

        BayesModel a = new BayesModel(dataSetHeader, avg, disp, cols_in, cols_out, learnIndexes, checkIndexes, timer.getStopTicks());
        logEndLearning(timer.stop());
        return a;
    }


    private int[] GetMembershipToClass(Matrix y){
        int count = y.rows();
        int[] res = new int[count];
        
        for(int i = 0; i < count; i++)
            res[i] = y.get(i, 0) > 0 ? 1 : 0;

        return res;
    }
    private double[][] GetAvgs(Matrix x, int[] member) {
        int rows = x.rows();
        int columns = x.columns();
        int[] countInClass = new int[2];
        double[][] result = new double[2][columns];
        
        for(int i = 0; i < rows; i++)
        {
            int yy = member[i];
            countInClass[yy]++;
            double[] row = x.getRow(i);
            
            for(int j = 0; j < columns; j++)
                result[yy][j] += row[j];
        }

        for(int i = 0; i < columns; i++){
            result[0][i] /= countInClass[0];
            result[1][i] /= countInClass[1];
        }
            
        return result;           
    }
    private double[][] GetDisp(Matrix x, double[][] avg, int[] member){
        int rows = x.rows();
        int columns = x.columns();
        double[][] res = new double[2][columns];
        int[] countInClass = new int[2];

        for(int i = 0 ; i < rows; i++){
            int y = member[i];
            countInClass[y]++;
            for(int j = 0; j < columns; j++)
                res[y][j] += Math.pow(x.get(i,j) - avg[y][j], 2);
        }

        for(int i = 0 ; i < columns; i++){
            res[0][i] /= countInClass[0]-1;
            res[1][i] /= countInClass[1]-1;
        }

        return res;
    }

    private int[] _learn_rows, _check_rows;
}
