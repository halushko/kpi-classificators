package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.LearningParameters;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.DataSetParameter;
import com.netcracker.analysis.models.LogisticLinearRegressionModel;
import com.netcracker.analysis.models.NonLinearRegressionModel;
import com.netcracker.analysis.util.Timer;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class NonLinearRegressionAlgorithm implements Algorithm{

    protected Log log = LogFactory.getLog(getClass());
    private int degree;
    LogisticLinearRegressionAlgorithm algorithm = new LogisticLinearRegressionAlgorithm();


    public NonLinearRegressionAlgorithm(int degree){
        this.degree = degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

//    @Override
//    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset) {
//        List<NonLinearRegressionModel> originalModels = getOriginalModels(models);
//
//        LogisticLinearRegressionAlgorithm alg = new LogisticLinearRegressionAlgorithm();
//
//        List<Model> retrainedModels = new ArrayList<Model>();
//        for (NonLinearRegressionModel om : originalModels){
//            log.info("Retraining model " + om.getName() + " " + om.getId());
//            LogisticLinearRegressionModel m =
//                    (LogisticLinearRegressionModel) alg.buildModel(dataset, om.getColsIn(), om.getColsOut(), );
//            if (m == null){
//                continue;
//            }
//            NonLinearRegressionModel nlrm = new NonLinearRegressionModel(m);
//            nlrm.setName(om.getName() + "_");
//            retrainedModels.add(nlrm);
//        }
//        return retrainedModels;
//    }

    private List<NonLinearRegressionModel> getOriginalModels(List<Model> models) {
        List<NonLinearRegressionModel> originalModels = new ArrayList<NonLinearRegressionModel>();
        Class c = NonLinearRegressionModel.class;
        for (Model m : models){
            if (!(c.isInstance(m)))
                continue;
            NonLinearRegressionModel model = (NonLinearRegressionModel) m;
            boolean isOriginal = true;
            for (NonLinearRegressionModel om : originalModels){
                if (model.equalCols(om)) {
                    isOriginal = false;
                    break;
                }
            }
            if (!isOriginal) continue;

            originalModels.add(model);
        }
        return originalModels;
    }

    List<Integer> getExtendedDataColumns(DataSetHeader dataSet, BitSet colsin)
    {
        int dimension = Util.countFlags(colsin)-1;

        List<Integer> dataSetNumbers = AlgUtils.getDataSetParametersPrimes(dataSet, colsin);
        return AlgUtils.getExtendedDataColumnsPrimeNumbers(dataSetNumbers, dimension, degree);
    }

    public Model buildModel(DataSetHeader dataset, BitSet colsin, BitSet colsout, AbstractSamplingGetter samplingGetter) {
//logStartLearning();
        Timer timer = new Timer();

        List<Integer> extendedColumnsNumbers = getExtendedDataColumns(dataset, colsin);

        BitSet new_colsin;

        synchronized (dataset.getDataSet()){
            for (int paramProduct : extendedColumnsNumbers){
                DataSetParameter p = dataset.addColumn(paramProduct);
            }

            new_colsin = new BitSet(dataset.getParameterList().size());
            new_colsin.or(colsin);

            for (int paramProduct : extendedColumnsNumbers){
                new_colsin.set(dataset.indexOf(paramProduct));
            }
        }


        LogisticLinearRegressionModel linearModel = (LogisticLinearRegressionModel) algorithm.buildModel(dataset, new_colsin, colsout, samplingGetter);

        NonLinearRegressionModel model = new NonLinearRegressionModel(linearModel);
        model.setTrainingTime(timer.stop());

        //logEndLearning();

        return model;
    }

    @Override
    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, AbstractSamplingGetter samplingGetter) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<Model> rebuildModel(List<Model> models, DataSetHeader dataset, List<Integer> parameters_to_leave, AbstractSamplingGetter samplingGetter) {
        List<NonLinearRegressionModel> originalModels = getOriginalModels(models);

        LogisticLinearRegressionAlgorithm alg = new LogisticLinearRegressionAlgorithm();

        List<Model> retrainedModels = new ArrayList<Model>();
        for (NonLinearRegressionModel om : originalModels){
            for (int i: parameters_to_leave){
                if (i > om.getParametersCount()){
                    log.info("Retraining model " + om.getName() + " " + om.getId() + " canceled - no parameters to reduce");
                    continue;
                }
                BitSet colsin = om.reduceParameters(i);
                log.info("Retraining model " + om.getName() + " " + om.getId() + " parameters " + colsin);
                LogisticLinearRegressionModel m = (LogisticLinearRegressionModel) alg.buildModel(dataset, colsin, om.getColsOut(), samplingGetter);
                if (m == null){
                    continue;
                }
                NonLinearRegressionModel nlrm = new NonLinearRegressionModel(m);
                nlrm.setName(om.getName() + "_" + i + "_parameters");
                retrainedModels.add(nlrm);
            }
        }
        return retrainedModels;
    }

    @Override
    public Model buildModel(LearningParameters params, AbstractSamplingGetter samplingGetter) {
        return buildModel(params.getDataSet(),params.getColsIn(),params.getColsOut(), samplingGetter);
    }
}

