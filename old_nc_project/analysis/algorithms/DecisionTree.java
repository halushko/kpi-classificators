package com.netcracker.analysis.algorithms;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.data.DataRow;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Util;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class DecisionTree {

    public DecisionTree(PlaneModel model, DecisionTree good, DecisionTree bad) {
        this._separatingModel = model;
        this._good = good;
        this._bad = bad;
    }

    public DecisionTree(double classMembership) {
        this._classMembership = classMembership;
    }

    public DecisionTree(DataSetHeader dataSetHeader, BitSet colsIn, BitSet colsOut, int[] indexes, DecisionTreeCoreAlgorithm algorithm) {
        this(dataSetHeader, colsIn, colsOut, indexes, algorithm, MAX_STEP);
    }

    public DecisionTree(DataSetHeader dataSetHeader, BitSet colsIn, BitSet colsOut, int[] indexes, DecisionTreeCoreAlgorithm algorithm, int steps) {
        this._in = colsIn;
        this._out = colsOut;
        this._dataSetHeader = dataSetHeader;
        _algorithm = algorithm;
        setMaxStep(steps);

        double dec = checkIfEnd(indexes);
        if(((Double)dec).isNaN())
            separate(indexes,  steps);
        else
            this._classMembership = dec;
    }


    public DecisionTreeCoreAlgorithm getAlgorithm() {
        return _algorithm;
    }
    public DataSetHeader getDataSetHeader() {
        return _dataSetHeader;
    }
    public DecisionTree getGood() {
        return _good;
    }
    public DecisionTree getBad() {
        return _bad;
    }
    public Model getSeparatingModel() {
        return _separatingModel;
    }
    public double getClassMembership() {
        return _classMembership;
    }
    public BitSet getIn() {
        return _in;
    }
    public BitSet getOut() {
        return _out;
    }
    public double getMinAccuracy() {
        if(((Double)_minAccuracy).isNaN())
            _minAccuracy = MIN_ACCURACY;
        return _minAccuracy;
    }
    public double getMaxStep() {
        if(((Double)_maxStep).isNaN())
            _maxStep = MAX_STEP;
        return _maxStep;
    }

    public void setMinAccuracy(double minAccuracy) {
        if(minAccuracy < MIN_ACCURACY)
            _minAccuracy = MIN_ACCURACY;
        else if(minAccuracy > 1)
            _minAccuracy = 1;
        else _minAccuracy = minAccuracy;
    }

    public void setMaxStep(double value) {
        if(value > MAX_STEP)
            _maxStep = MAX_STEP;
        else if(value < 1)
            _maxStep = 1;
        else
            this._maxStep = value;
    }

    public void separate(int[] indexes, int step){
        if(step < 1 || indexes.length < 4){
            this._classMembership = getGoodOrBad(indexes);
            return;
        }

        DataSetHeader dsh = getDataSetHeader();
        DataSet dataSet = dsh.getDataSet();

        Model model;
        try{
            model = getAlgorithm().createModel(getDataSetHeader(), getIn(), getOut(), indexes, new int[0]);
        }
        catch(IllegalStateException e){
            this._classMembership = getGoodOrBad(indexes);
            return;
        }

        List<Integer> goodPoints = new ArrayList<Integer>(), badPoints = new ArrayList<Integer>();
        
        for(int i: indexes){
            if(model.getOut(dataSet.getRow(i)) > 0.5)
                goodPoints.add(i);
            else
                badPoints.add(i);
        }
        if(goodPoints.size() == 0){
            this._classMembership = (double) 1;
            return;
        }
        if(badPoints.size() == 0){
            this._classMembership = (double) 0;
            return;
        }

        this._separatingModel = model;

        this._good = new DecisionTree(getDataSetHeader(), getIn(), getOut(), Util.toIntArr(goodPoints), getAlgorithm(), step - 1);
        this._bad = new DecisionTree(getDataSetHeader(), getIn(), getOut(), Util.toIntArr(badPoints), getAlgorithm(), step - 1);
    }
    
    public double getDecision(DataRow row){
        if(((Double)getClassMembership()).isNaN()){
            double a = getSeparatingModel().getOut(row);
            if(a > 0.5)
                return getGood().getDecision(row);
            else
                return getBad().getDecision(row);
        }
        else return getClassMembership();
    }

    private double checkIfEnd(int[] indexes) {
        double good = 0;
        DataSet ds = getDataSetHeader().getDataSet();
        for(int i: indexes){
            if(ds.get(i, getOut().nextSetBit(0)) > 0.5)
                good++;
        }
        if(good/indexes.length > getMinAccuracy())
            return 1;
        if(((double)indexes.length - good)/indexes.length > getMinAccuracy())
            return 0;
        else return Double.NaN;
    }
    
    private double getGoodOrBad(int[] indexes){
        DataSet dataSet = getDataSetHeader().getDataSet();
        int good = 0;
        for(int i: indexes)
            if(dataSet.getRow(i).get(getOut().nextSetBit(0)) > 0.5)
                good++;
        return good > (double) indexes.length / 2 ? 1 : 0;
    }


    private DataSetHeader _dataSetHeader;
    private DecisionTreeCoreAlgorithm _algorithm;
    private BitSet _in, _out;
    private Model _separatingModel;
    private DecisionTree _good;
    private DecisionTree _bad;
    private double _classMembership= Double.NaN;
    private double _minAccuracy = Double.NaN;
    private double _maxStep = Double.NaN;
    
    private final double MIN_ACCURACY = 0.95;
    private final static int MAX_STEP = 10;

    public void getModels(List<GenericModel> models) {
        if (getSeparatingModel()!=null)
            models.add((GenericModel) getSeparatingModel());
        if (getGood()!= null)
            getGood().getModels(models);
        if (getBad()!=null)
            getBad().getModels(models);
    }
}
