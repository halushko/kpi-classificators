package com.netcracker.analysis.taskman;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.dao.SaveException;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.util.MemoryBanchmarker;
import com.netcracker.analysis.util.Print;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.BitSet;

public class LearnModelTask extends Task {

    private Log log = LogFactory.getLog(getClass());

    private DataSetsControllerBean bean;
    private DataSetHeader dataSet;
    private Algorithm algorithm;
    private BitSet inp;
    private BitSet oup;
    private AbstractSamplingGetter samplingGetter;
    private boolean saveModel;

    public LearnModelTask(DataSetsControllerBean bean, DataSetHeader dsh, Algorithm a, BitSet inp, BitSet oup, AbstractSamplingGetter samplingGetter, boolean saveModel) {

        this.bean = bean;
        this.dataSet = dsh;
        this.algorithm = a;
        this.inp = inp;
        this.oup = oup;
        this.samplingGetter = samplingGetter;
        this.saveModel = saveModel;
    }

    @Override
    protected void runTask() {

        GenericModel mo = null;
        try {

            MemoryBanchmarker<GenericModel> bench =
                    new MemoryBanchmarker<GenericModel>(){
                        @Override
                        protected GenericModel run() {
                            return (GenericModel) algorithm.buildModel(dataSet, inp, oup, samplingGetter);
                        }
                    };
            mo = bench.doBencmark();
            mo.setLearnMemory(bench.getMemUsed());

            ModelBenchmark mb = new ModelBenchmark((PlaneModel) mo, samplingGetter.getCheck_indexes());

            if (saveModel)
                DaoFactory.MODEL_DAO.save(mo);
            else {
                bean.getModelBenchmarks().add(mb);
            }

            String ret = Print.printModelBenchmarks(Arrays.asList(mb));
            setResponse(ret);
        } catch (SaveException e) {
            setResponse(e);
            setStatus(Status.Error);
        }
    }

    @Override
    public void stopTask() {
        return;
    }
}
