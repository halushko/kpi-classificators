package com.netcracker.analysis.taskman;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.algorithms.NonLinearRegressionAlgorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.discarder.params.AbstractDiscarder;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.models.NonLinearRegressionModel;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.util.Print;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class SimplifyNonLinearTask extends Task {

    private Log log = LogFactory.getLog(getClass());

    private DataSetHeader dataSet;
    private NonLinearRegressionAlgorithm algorithm;
    private BitSet inp;
    private BitSet oup;

    private DataSetsControllerBean bean;

    public SimplifyNonLinearTask(DataSetHeader dsh, int degree, BitSet inp, BitSet oup, DataSetsControllerBean bean) {

        this.dataSet = dsh;
        this.algorithm = new NonLinearRegressionAlgorithm(degree);
        this.inp = inp;
        this.oup = oup;
        this.bean = bean;
    }

    @Override
    protected void runTask() {

        Model mo = null;
        try {
            List<ModelBenchmark> modelBenchmarks = new ArrayList<ModelBenchmark>();
            AbstractSamplingGetter asg = bean.getSamplingGetter(dataSet.getIdString());

            NonLinearRegressionModel model = (NonLinearRegressionModel) algorithm.buildModel(dataSet, inp, oup, asg);
            modelBenchmarks.add(new ModelBenchmark(model, asg.getCheck_indexes()));

            int pc = model.getParametersCount();

            List<Model> models = algorithm.rebuildModel(Arrays.asList((Model) model), dataSet, Arrays.asList(pc*5/6, pc*4/6, pc*3/6, pc*2/6), asg);
            for (Model m : models){
                try{
                    NonLinearRegressionModel mod = (NonLinearRegressionModel) m;
                    modelBenchmarks.add(new ModelBenchmark(mod, asg.getCheck_indexes()));
                } catch (Exception e){
                }
            }

            List<ModelBenchmark> best = AbstractDiscarder.getBestEstimationModels(modelBenchmarks, ESTIMATION_TYPE.ADEQUACY);

            bean.getModelBenchmarks().addAll(best);

            String ret = "Best models\n\n";
            ret += Print.printModelBenchmarks(best);//printModelHeaders(best);
            ret += "\n\n\n\n All models\n\n";
            ret += Print.printModelBenchmarks(modelBenchmarks);

            setResponse(ret);
        } catch (Exception e) {
            log.error("Error in task",e);

            StringWriter sw = new StringWriter();

            e.printStackTrace(new PrintWriter(sw));

            setResponse(sw.toString());
            setStatus(Status.Error);
        }
    }

    @Override
    public void stopTask() {
        return;
    }
}
