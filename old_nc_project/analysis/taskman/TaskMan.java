package com.netcracker.analysis.taskman;

import java.util.HashMap;

public class TaskMan {

    private static TaskMan instance;
    private static int last_id = 1;

    private TaskMan(){
    }

    public static TaskMan getInstance(){
        if (instance == null)
            instance = new TaskMan();
        return instance;
    }

    private HashMap<Integer,Task> tasks = new HashMap<Integer,Task>();
    private HashMap<Integer,Thread> treads = new HashMap<Integer,Thread>();

    public synchronized Task getTask (int task_id){
        Task task = tasks.get(task_id);

        if (task!= null && !task.getStatus().needThread())
            treads.remove(task_id);

        return task;
    }

    public int nextId(){
        return last_id++;
    }

    public synchronized int start(Task task) {
        tasks.put(task.getId(), task);

        Thread t = new Thread(task);
        t.start();

        return task.getId();  //To change body of created methods use File | Settings | File Templates.
    }

    @SuppressWarnings({"deprecation"})
    public synchronized Task stopTask(int task_id) {
        Task t = getTask(task_id);
        if (t == null)
            return null;

        t.stop();

        Thread th = treads.remove(task_id);
        if (th != null)
            th.stop();

        return t;
    }


}
