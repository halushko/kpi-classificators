package com.netcracker.analysis.taskman;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.Model;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.discarder.params.AbstractDiscarder;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.presentation.DataSetsControllerBean;
import com.netcracker.analysis.util.Print;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.BitSet;
import java.util.List;

public class DiscarderTask extends Task {

    private Log log = LogFactory.getLog(getClass());

    private DataSetHeader dataSet;
    private Algorithm algorithm;
    private BitSet inp;
    private BitSet oup;

    private DataSetsControllerBean bean;

    public DiscarderTask(DataSetHeader dsh, Algorithm a, BitSet inp, BitSet oup, DataSetsControllerBean bean) {

        this.dataSet = dsh;
        this.algorithm = a;
        this.inp = inp;
        this.oup = oup;
        this.bean = bean;
    }

    @Override
    protected void runTask() {

        Model mo = null;
        try {
            List<ModelBenchmark> modelBenchmarks = Print.getModelBenchmarks(dataSet, inp, oup, algorithm, bean.getSamplingGetter(dataSet.getIdString()));
            List<ModelBenchmark> best = AbstractDiscarder.getBestEstimationModels(modelBenchmarks, ESTIMATION_TYPE.ADEQUACY);

            bean.getModelBenchmarks().addAll(best);

            String ret = "Best models\n\n";
            ret += Print.printModelBenchmarks(best);//printModelHeaders(best);
            ret += "\n\n\n\n All models\n\n";
            ret += Print.printModelBenchmarks(modelBenchmarks);

            setResponse(ret);
        } catch (Exception e) {
            log.error("Error in task",e);

            StringWriter sw = new StringWriter();

            e.printStackTrace(new PrintWriter(sw));

            setResponse(sw.toString());
            setStatus(Status.Error);
        }
    }

    @Override
    public void stopTask() {
        return;
    }
}
