package com.netcracker.analysis.taskman;

import com.netcracker.analysis.Model;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.discarder.params.AbstractDiscarder;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.util.Print;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

public class SaveBestModelsTask extends Task {

    private Log log = LogFactory.getLog(getClass());

    private List<ModelBenchmark> modelBenchmarks;

    public SaveBestModelsTask(List<ModelBenchmark> modelBenchmarks) {
        this.modelBenchmarks = modelBenchmarks;
    }

    @Override
    protected void runTask() {

        Model mo = null;
        try {
            List<ModelBenchmark> best = AbstractDiscarder.getBestEstimationModels(modelBenchmarks, ESTIMATION_TYPE.ADEQUACY);

            String ret = "Best models\n\n";
            ret += Print.printModelBenchmarks(best);//printModelHeaders(best);
            ret += "\n\n\n\n All models\n\n";
            ret += Print.printModelBenchmarks(modelBenchmarks);

            ret += "\n\n\n\n Exceptions\n\n";

            for (ModelBenchmark m : best){
                try{
                    DaoFactory.MODEL_DAO.save(m.getModel());
                } catch (Exception e){
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));

                    ret += "\n" + m.getModel().getName() + "\n" + m.getModel().getLegend()+"\n"+e.getMessage() + "\n" + sw + "\n\n";
                }
            }
            
            setResponse(ret);
        } catch (Exception e) {
            setResponse(e);
            setStatus(Status.Error);
        }
    }

    @Override
    public void stopTask() {
        return;
    }
}
