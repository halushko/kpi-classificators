package com.netcracker.analysis.taskman;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class Task implements Runnable{

    public enum Status {
        TaskNotFound, NotStarted, Running, Completed, Error, Canceled;

        public boolean needThread (){
            return (this.equals(NotStarted) || this.equals(Running) || this.equals(Completed));
        }
    }

    private Object response;
    private Status status;
    private final int id;

    public Task(){
        this.id = TaskMan.getInstance().nextId();
        this.status = Status.NotStarted;
    }

    public void run() {
        if (!status.equals(Status.NotStarted))
            return;

        status = Status.Running;
        try{
            runTask();
        } catch (Throwable e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            setResponse(sw.toString());
            setStatus(Status.Error);
        }
        if (!status.equals(Status.Error))
            status = Status.Completed;
    }

    public void stop(){
        if(status.equals(Status.Completed))
            return;
        try{
        stopTask();
        } catch (Throwable e){
        }
        status = Status.Canceled;
    }

    protected abstract void runTask();

    /**
     * Correct task finishing. After stopTask returns, Thread.abort() is called
     */
    public abstract void stopTask();

    public int getId(){
        return id;
    }

    public Status getStatus() {
        return status;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    public Object getResponse() {
        return response;
    }

    protected void setResponse(Object response){
        this.response = response;
    }


}
