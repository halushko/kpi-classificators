package com.netcracker.analysis.presentation;


import com.netcracker.analysis.data.DataSetHeader;

import java.util.HashMap;

public class DataSetsViewBean {
    private String dsId;

    public DataSetsControllerBean getDataSetsBean() {
        return dataSetsBean;
    }

    public void setDataSetsBean(DataSetsControllerBean dataSetsBean) {
        this.dataSetsBean = dataSetsBean;
    }

    private DataSetsControllerBean dataSetsBean;

    public String getDsId() {
        return dsId;
    }

    public void setDsId(String dsId) {
        this.dsId = dsId;
    }

    public DataSetHeader getDs() {
        HashMap<String, DataSetHeader> dataSets = getDataSetsBean().getDataSets();
        //getDs().getParameterListUnmodified().size()
        return dataSets.get(getDsId());
    }


}
