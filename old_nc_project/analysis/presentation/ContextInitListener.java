package com.netcracker.analysis.presentation;

import com.netcracker.analysis.models.GenericModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.*;
import java.util.Map;
import java.util.Properties;

@WebListener()
public class ContextInitListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {
    Log log = LogFactory.getLog(getClass());
    
    // Public constructor is required by servlet spec
    public ContextInitListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {

        final InputStream labels2= getClass().getClassLoader().getResourceAsStream("labels.properties");
        labels2.getClass();
        ServletContext context = sce.getServletContext();
        Properties labels = new Properties() {{

            try {
                load(new InputStreamReader(labels2,"UTF-8"));
            } catch (IOException e) {
                log.error("Error loading resources", e);
            }
        }};

        for ( Map.Entry<Object, Object> propEntry : labels.entrySet()){
            context.setAttribute((String) propEntry.getKey(), propEntry.getValue());
        }
    }


    public void contextDestroyed(ServletContextEvent sce) {
        /* This method is invoked when the Servlet Context 
           (the Web application) is undeployed or 
           Application Server shuts down.
        */
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
        /* Session is created. */
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        /* Session is destroyed. */
    }

    // -------------------------------------------------------
    // HttpSessionAttributeListener implementation
    // -------------------------------------------------------

    public void attributeAdded(HttpSessionBindingEvent sbe) {
        /* This method is called when an attribute 
           is added to a session.
        */
    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {
        /* This method is called when an attribute
           is removed from a session.
        */
    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {
        /* This method is invoked when an attibute
           is replaced in a session.
        */
    }
}
