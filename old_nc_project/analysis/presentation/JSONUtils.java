package com.netcracker.analysis.presentation;


import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonBeanProcessor;
import net.sf.json.processors.JsonValueProcessor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class JSONUtils {
    private static JsonConfig DEFAULT_CONFIG = new JsonConfig();
    static
    {
        DEFAULT_CONFIG.setIgnoreJPATransient(true);
        DEFAULT_CONFIG.registerJsonValueProcessor(Date.class, new DateValProcessor());

    }
    public static JSON toJSON(Object o) {
        return JSONSerializer.toJSON(o,DEFAULT_CONFIG);
    }
}



class DateValProcessor implements JsonValueProcessor
{
    private final static SimpleDateFormat dateFormat;
    static {
        //as per ISO 8601
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        //make sure the formatter requires an exact string
        dateFormat.setLenient(false);
        //set "timezone" to Universal Time Coordinates (GMT adjusted by leap seconds)
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    @Override
    public Object processArrayValue(Object value, JsonConfig jsonConfig) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
        Date date = (Date)value ;

        return  "new Date("+date.getTime()+")";
    }

}