package com.netcracker.analysis.presentation;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.AlgorithmLister;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;
import net.sf.json.JSON;
import org.codehaus.jettison.json.JSONArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataSetsControllerBean {
    HashMap<String,DataSetHeader> allDss;
    private List<ModelBenchmark> modelBenchmarks = new ArrayList<ModelBenchmark>();
    HashMap<String,AbstractSamplingGetter> samplingGetters = new HashMap<String,AbstractSamplingGetter>();


    public synchronized HashMap<String,DataSetHeader> getDataSets() {
        if(allDss== null)
            allDss = Util.toHashMap(DaoFactory.DATA_SET_DAO.getAll());

        return allDss;
    }

    public JSON getDataSetsAsJSON() {
        return JSONUtils.toJSON(getDataSets().values());
    }

    public String getSamplingGetters(){
        return JSONUtils.toJSON(SAMPLING_TYPE.values()).toString();
    }

    public JSONArray getAlgorithms() {
        List<Algorithm> algs = AlgorithmLister.getInstance().getAll();
        JSONArray array = new JSONArray();
        for (Algorithm alg : algs) {
            array.put(alg.getClass().getSimpleName().replace("Model",""));
        }

        return array;
    }

    public List<ModelBenchmark> getModelBenchmarks() {
        return modelBenchmarks;
    }

    public AbstractSamplingGetter getSamplingGetter(String DatasetId) {
        DataSetHeader dsh = getDataSets().get(DatasetId);
        if (dsh == null)
            return null;

        AbstractSamplingGetter samplingGetter = samplingGetters.get(DatasetId);
        if (samplingGetter == null){
            samplingGetter = AbstractSamplingGetter.GetSamplingGetter("ALL", dsh.getDataSet().rows());
            samplingGetters.put(DatasetId, samplingGetter);
        }
        return samplingGetter;
    }

    public void setSamplingGetter(String DatasetId,String type) {
        DataSetHeader dsh = getDataSets().get(DatasetId);
        if (dsh == null)
            return;
        AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(type, dsh.getDataSet().rows());
        samplingGetters.put(DatasetId,samplingGetter);
    }
}
