package com.netcracker.analysis.data;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

/**
 * Matrix
 */
public class DataSet implements Iterable<List<Double>>{

    private DataSetHeader parent;
    private List<DataRow> vals;
    
    public DataSet(DataSetHeader parent, List<DataRow> vals) {
        this.parent = parent;
        this.vals = vals;
    }

    public DataSetHeader getParent() {
        return parent;
    }

    public Double get(int row, int col)
    {
        return vals.get(row).get(col);
    }

    public void set (int m, int n, double value){
        vals.get(m).set(n, value);
    }

    public DataRow getRow(int row)
    {
        return vals.get(row);
    }

    public List<Double> getCol(int col)
    {
        List<Double> column = new ArrayList<Double>();
        for (List<Double> val : vals) {
            column.add(val.get(col));
        }
        return column;
    }
    
    public int rows(){
        return vals.size();
    }

    public int columns(){
        if (rows() == 0) return 0;
        return vals.get(0).size();
    }

    /**
     * Add one more parameter, as a product of other parameters (for non linear regression)
     * @param columns flags which columns to multiply
     */
    void addColumn (List<DataSetParameter> columns) {
        List<DataRow> dataRows = vals;
        int count = Math.min(dataRows.get(0).size(), columns.size());
        for (List<Double> row : dataRows) {
            double value = 1;
            for (DataSetParameter p : columns) {
                value *= row.get(parent.indexOf(p));
            }
            row.add(value);
        }
    }

    public Iterator iterator() {
        return new DSIterator();
    }

    public Iterator iterator(BitSet rows) {
        return new DSIterator(rows);
    }

    public class DSIterator implements Iterator<List<Double>> {

        private final BitSet rows;
        private int curIndex;

        public DSIterator () {
                this(new BitSet(rows()){{set(0,rows()>0? rows()-1:0);}});
        }

        public DSIterator (BitSet rows) {
            this.rows = rows;
        }

        public boolean hasNext() {
            for (int i = curIndex; i < rows(); i++)
                if (rows.get(i))
                    return true;
            return false;
        }

        public List<Double> next() {
            for (; curIndex < rows(); curIndex++)
                if (rows.get(curIndex)){
                    return getRow(curIndex++);
                }

            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        public void remove() {

        }
    }
}
