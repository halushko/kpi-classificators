package com.netcracker.analysis.data;

import com.netcracker.analysis.util.PrimeNumbers;

import javax.persistence.Transient;
import java.math.BigInteger;
import java.util.BitSet;

public class DataSetParameter {
    private BigInteger id;
    private String name;
    private String mapping;
    private ParameterType type;
    private int prime_number;

    public DataSetParameter (BigInteger id, String name, int prime_number) {
        this.id = id;
        this.name = name;
        this.prime_number = prime_number;
    }

    public enum ParameterType{
        IN, OUT
    }

    public BigInteger getId() {
        return id;
    }


    public String getIdString(){
        return id.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    public ParameterType getParameterType() {
        return type;
    }

    public int getPrime_number() {
        return prime_number;
    }

    @Transient
    public int[] getIncludedParameters() {
        return PrimeNumbers.getIndexes(prime_number);
    }
}
