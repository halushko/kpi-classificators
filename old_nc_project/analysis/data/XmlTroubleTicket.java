package com.netcracker.analysis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat;

@Deprecated
@XmlRootElement(name = "XmlTroubleTicket")
public class XmlTroubleTicket{
    public String dataset;
    public int type;
    //public Date date;
    
    public static String dateFormatString = "29.05.2012 18:40:34";
    private DateFormat dateFormat;

    @XmlElementWrapper(name = "parameters")
    @XmlElement(name = "XmlTTParameter")
    public XmlTTParameter[] parameters;

    public XmlTroubleTicket(String dataset, int type, XmlTTParameter[] parameters, String date){
        this.dataset = dataset;
        this.type = type;
        this.parameters = parameters;
        //this.date = new Date();
    }
    public XmlTroubleTicket(){}

    public static String TEST_TROUBLE_TICKET =
            "<XmlTroubleTicket>\n" +
                    "    <dataset>6555408163513261980</dataset>\n" +
                    "    <type>0</type>\n" +
                    "    <date>2012-05-31T11:43:48.914+03:00</date>" +
                    "<parameters>"+
                    "    <XmlTTParameter>\n" +
                    "        <id>6555408163513261981</id>\n" +
                    "        <value>777</value>\n" +
                    "    </XmlTTParameter>\n" +
                    "    <XmlTTParameter>\n" +
                    "        <id>6555408163513261982</id>\n" +
                    "        <value>888</value>\n" +
                    "    </XmlTTParameter>\n" +
                    "    <XmlTTParameter>\n" +
                    "        <id>6555408163513261983</id>\n" +
                    "        <value>0</value>\n" +
                    "    </XmlTTParameter>\n" +
                    "</parameters>"+
                    "</XmlTroubleTicket>";
}