package com.netcracker.analysis.data;

import java.math.BigInteger;
import java.util.Date;

public class HistoryRow extends DataRow{

    /**
     * load from DB
     * @param parent
     */
    public HistoryRow(DataSetHeader parent, BigInteger row_id, Date date){
        super(parent, row_id, date);
    }

    /**
     * create new LearningRow
     * @param parent
     */
    public HistoryRow(DataSetHeader parent, Date date/*, int type*/) {
        super(parent, date);
        //this.date = date;
    }
}
