package com.netcracker.analysis.data;

import javax.xml.bind.annotation.XmlRootElement;

@Deprecated
@XmlRootElement(name = "XmlTTParameter")
public class XmlTTParameter{
    public String id;
    public double value;

    public XmlTTParameter(String id, double value){
        this.id = id;
        this.value = value;
    }

    public XmlTTParameter(){}
}