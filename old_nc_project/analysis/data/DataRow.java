package com.netcracker.analysis.data;

import com.netcracker.analysis.util.PrimeNumbers;
import com.netcracker.analysis.util.Util;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.*;

/**
 * A row of a matrix
 */
public abstract class DataRow extends ArrayList<Double> {

    private DataSetHeader parent;

    private BigInteger row_id;

    protected Date date;
    private String name = "";
    private String description = "";

    /**
     * load from DB
     * @param parent
     */
    public DataRow(DataSetHeader parent, BigInteger row_id, Date date) {
        super(parent.getParameterList().size());

        for(int i = 0; i < parent.getParameterList().size(); i++)
            this.add(0.0);

        this.parent = parent;
        this.date = date;
        this.row_id = row_id;
        this.name = MessageFormat.format("{0}_{1}", getClass().getSimpleName().charAt(0), date);
    }

    /**
     * create new DataRow
     * @param parent
     */
    public DataRow(DataSetHeader parent, Date date) {
        this(parent, Util.getID(), date);
    }

    public double get(String variable) throws IllegalArgumentException {
        int index = parent.indexOf(variable);
        if (index == -1)
            throw new IllegalArgumentException("Variable not exists: " + variable);
        return get(index);
    }

    public DataSetHeader getParent(){
        return parent;
    }

    public Iterator iterator() {
        return new DRIterator();
    }

    public Iterator iterator(BitSet cols) {
        return new DRIterator(cols);
    }

    public void normalizeColumns() {
        List<DataSetParameter> pl = parent.getParameterList();
        synchronized (pl){
            for (int i = 0; i < pl.size(); i++){
                int number = pl.get(i).getPrime_number();
                if (PrimeNumbers.isPrime(number))
                    continue;
                double value = 1;
                for (int p : PrimeNumbers.getIndexes(number)) {
                    value *= this.get(p);
                }
                this.set(i, value);
            }
        }
    }

    public BigInteger getId(){
        return row_id;
    }

    public Date getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public class DRIterator implements Iterator<Double> {

        private final BitSet cols;
        private int curIndex;

        public DRIterator () {
            this(new BitSet(size()){{set(0, size()-1);}});
        }

        public DRIterator (BitSet cols) {
            this.cols = cols;
        }

        public boolean hasNext() {
            for (int i = curIndex; i < size(); i++)
                if (cols.get(i))
                    return true;
            return false;
        }

        public Double next() {
            for (; curIndex < size(); curIndex++)
                if (cols.get(curIndex)){
                    return get(curIndex++);
                }

            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        public void remove() {

        }
    }

}
