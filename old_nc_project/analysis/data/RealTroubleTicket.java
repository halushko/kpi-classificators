package com.netcracker.analysis.data;

import com.netcracker.analysis.HasId;
import com.netcracker.analysis.data.states.State;
import com.netcracker.analysis.exceptions.ExceptionFactory;
import com.netcracker.analysis.util.Util;

import javax.persistence.Transient;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class RealTroubleTicket  implements HasId {
    private DataSetHeader dataSet;

    /**
     * from DB
     */
    public RealTroubleTicket(BigInteger id, DataSetHeader dataSet) {
        if(dataSet == null)
            throw ExceptionFactory.IllegalArgument("Data set can't be NULL");
        if(id == null)
            throw ExceptionFactory.IllegalArgument("ID can't be NULL");

        this.id = id;
        this.dataSet = dataSet;
    }

    public RealTroubleTicket(DataSetHeader dataSet){
        if(dataSet == null)
            throw ExceptionFactory.IllegalArgument("Data set can't be NULL");

        this.dataSet = dataSet;
        this.id =  Util.getID();
    }

    private BigInteger id;
    Date receivedWhen;
    Date startTime;
    Date endTime;
    State stateOnReceive = State.bad();
    State overallState  = State.bad();

    List<HistoryRow> assignedRows;

    @Override
    public BigInteger getId() {
        return id;
    }

    public Date getReceivedWhen() {
        return receivedWhen;
    }

    public void setReceivedWhen(Date receivedWhen) {
        this.receivedWhen = receivedWhen;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public State getStateOnReceive() {
        return stateOnReceive;
    }

    public void setStateOnReceive(State stateOnReceive) {
        this.stateOnReceive = stateOnReceive;
    }

    public State getOverallState() {
        return overallState;
    }

    public void setOverallState(State overallState) {
        this.overallState = overallState;
    }

    public List<HistoryRow> getAssignedRows() {
        return assignedRows;
    }

    public void setAssignedRows(List<HistoryRow> assignedRows) {
        this.assignedRows = assignedRows;
    }

    @Transient
    public DataSetHeader getDataSet() {
        return dataSet;
    }
}
