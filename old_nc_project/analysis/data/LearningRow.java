package com.netcracker.analysis.data;

import java.math.BigInteger;
import java.util.Date;

public class LearningRow extends DataRow{

    /**
     * NCReference, can be null
     */
    private HistoryRow targetRow;

    /**
     * load from DB
     * @param parent
     */
    public LearningRow(DataSetHeader parent, BigInteger row_number, Date date){
        super(parent, row_number, date);
    }

    /**
     * create new LearningRow
     * @param parent
     */
    public LearningRow(DataSetHeader parent, Date date/*, int type*/) {
        super(parent, date);
    }
}
