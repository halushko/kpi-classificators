
package com.netcracker.analysis.data;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.HasId;
import com.netcracker.analysis.dao.DaoFactory;
import com.netcracker.analysis.dao.SaveException;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.util.MemoryBanchmarker;
import com.netcracker.analysis.util.PrimeNumbers;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.Transient;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.List;

public class DataSetHeader implements HasId {
    
    private static final Log log = LogFactory.getLog(DataSetHeader.class);

    private BigInteger id;

    private String name;
    private static final Runtime RUNTIME = Runtime.getRuntime();

    public DataSetHeader(BigInteger object_id, String name) {
        id = object_id;
        setName(name);
    }

    private List<DataSetParameter> parameterList;
    private int realParametersCount;

    private DataSet dataSet;

    private List<GenericModel> modelList;

    public BigInteger getId() {
        return id;
    }

    public String getIdString(){
        return id.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
    * Matrix column headers
    */
    @Transient
    public synchronized List<DataSetParameter> getParameterList() {
        if (parameterList == null){
            parameterList = DaoFactory.DATA_SET_DAO.getParametersList(id);
            realParametersCount = parameterList.size();
        }
        return parameterList;
    }

    /**
    * Matrix column headers - unmodified
    */
    public List<DataSetParameter> getParameterListUnmodified() {
        List<DataSetParameter> pl = getParameterList();
        return pl.subList(0, realParametersCount);
    }

    public int getRealParametersCount(){
        return realParametersCount;
    }

    /**
    * Matrix
    */
    @Transient
    public synchronized DataSet getDataSet() {
        if (dataSet == null){
            dataSet = DaoFactory.DATA_ROW_DAO.getMatrix(this);
        }
        return dataSet;
    }

    public void refreshDataSet(){
        dataSet=null;
        getDataSet();
    }

    /**
     * Matrix
     */
    @Transient
    public synchronized void setDataSet(DataSet value) {
        dataSet = value;
    }

    @Transient
    public DataRow getLatestParameters() {
        return DaoFactory.DATA_ROW_DAO.getLatestRow(this);
    }

    /**
    * Models
    */
    public synchronized List<GenericModel> getModelList() {
        if (modelList == null)
            refreshModelList();
        return modelList;
    }

    public List<GenericModel> refreshModelList() {
        List<GenericModel>  ml = DaoFactory.MODEL_DAO.getForDataSet(this);
        if (ml != null)
            modelList = ml;
        else
            log.warn("Can't refreshModelList, no results found");
        return modelList;
    }

    public GenericModel createModel(final Algorithm algorithm, final BitSet colsin, final BitSet colsout, final AbstractSamplingGetter samplingGetter) throws SaveException {
        MemoryBanchmarker<GenericModel> bench =
            new MemoryBanchmarker<GenericModel>(){
                @Override
                protected GenericModel run() {
                    return (GenericModel) algorithm.buildModel(DataSetHeader.this, colsin, colsout, samplingGetter);
                }
            };
        GenericModel model = bench.doBencmark();
        model.setLearnMemory(bench.getMemUsed());

        DaoFactory.MODEL_DAO.save(model);
        if (modelList != null)
            modelList.add(model);

        return model;
    }

    public HistoryRow createHistoryRowFromArray(double[] arr, BitSet colsin, BitSet colsout){
        if (Util.countFlags(colsin) > arr.length || colsin.length()>getParameterList().size() || colsout.length()>getParameterList().size())
            throw new IllegalArgumentException("colsin or colsout count more then parameters or array size");

        HistoryRow dr = new HistoryRow(this, new Date());
        int j=0;
        for (int i = colsin.nextSetBit(0); i>-1; i=colsin.nextSetBit(i+1), j++){
            dr.set(i, arr[j]);
        }
        if (j >= arr[j])
            return dr;
        for (int i = colsout.nextSetBit(0); i>-1; i=colsout.nextSetBit(i+1), j++){
            dr.set(i, arr[j]);
        }
        return dr;
    }

    public int indexOf(String parameter_name) {
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            for (int i = 0; i<pl.size(); i++)
                if (parameter_name.equals(pl.get(i).getName()))
                    return i;
        }
        return -1;
    }

    public int indexOf(BigInteger parameter_id) {
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            for (int i = 0; i<pl.size(); i++)
                if (parameter_id.equals(pl.get(i).getId()))
                    return i;
            return -1;
        }
    }

    public DataSetParameter getParameter(BigInteger parameter_id) {
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            return pl.get(indexOf(parameter_id));
        }
    }

    public int indexOf(DataSetParameter parameter) {
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            return pl.indexOf(parameter);
        }
    }

    public int indexOf(Integer prime_number) {
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            for (int i = 0; i<pl.size(); i++)
                if (prime_number.equals(pl.get(i).getPrime_number()))
                    return i;
            return -1;
        }
    }

    /**
     * Generate bitset mask for the chosen parameters
     * @param params) List<String|BigInteger|DataSetParameter> of parameters to include in bitset
     * @return
     */
    public BitSet generateBitSet (List params){
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){

        BitSet b = new BitSet(pl.size());

        if (params == null || params.size() == 0)
            return b;

        Object first = params.get(0);
        if (first instanceof String)
            for (String s : (List<String>) params){
                b.set(indexOf(s));
            }
        else if (first instanceof BigInteger)
            for (BigInteger s : (List<BigInteger>) params){
                b.set(indexOf(s));
            }
        else if (first instanceof DataSetParameter)
            for (DataSetParameter s : (List<DataSetParameter>) params){
                b.set(pl.indexOf(s));
            }
        else
            throw new IllegalArgumentException("To generate a BitSet parameter list must be of type List<String|BigInteger|DataSetParameter>. Received type: " + first.getClass());
        return b;
        }
    }

    /**
     * Update existing bitset mask for the chosen parameters
     * @param params_set List<String|BigInteger|DataSetParameter> of parameters to include in bitset
     * @param params_clear List<String|BigInteger|DataSetParameter> of parameters to exclude from bitset
     * @return
     */
    public BitSet updateBitSet (BitSet b, List params_set, List params_clear){
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){

        Object first;
        if (params_set == null || params_set.size() == 0)
            if(params_clear == null || params_clear.size() == 0)
                return b;
            else
                first = params_clear.get(0);
        else
            first = params_set.get(0);


        if (first instanceof String) {
            if (params_set!=null)
                for (String s : (List<String>) params_set){
                    b.set(indexOf(s));
                }
            if (params_clear!=null)
                for (String s : (List<String>) params_clear){
                    b.clear(indexOf(s));
                }
        }
        else if (first instanceof BigInteger) {
            if (params_set!=null)
                for (BigInteger s : (List<BigInteger>) params_set){
                    b.set(indexOf(s));
                }
            if (params_clear!=null)
                for (BigInteger s : (List<BigInteger>) params_clear){
                    b.clear(indexOf(s));
                }
        }
        else if (first instanceof DataSetParameter) {
            if (params_set!=null)
                for (DataSetParameter s : (List<DataSetParameter>) params_set){
                    b.set(pl.indexOf(s));
                }
            if (params_clear!=null)
                for (DataSetParameter s : (List<DataSetParameter>) params_clear){
                    b.clear(pl.indexOf(s));
                }
        }
        else
            throw new IllegalArgumentException("To generate a BitSet parameter list must be of type List<String|BigInteger|DataSetParameter>. Received type: " + first.getClass());
        return b;
        }
    }

    public List<BigInteger> getParametersIDs (BitSet b){
        List<DataSetParameter> pl = getParameterList();
        List<BigInteger> l = new ArrayList<BigInteger>();
        synchronized (pl){
            for (int i = b.nextSetBit(0); i > -1; i = b.nextSetBit(i+1)){
                l.add(pl.get(i).getId());
            }
        }
        return l;
    }

    public List<BigInteger> getParametersIDs (int[] indexes){
        List<DataSetParameter> pl = getParameterList();
        List<BigInteger> l = new ArrayList<BigInteger>();
        synchronized (pl){
            for (int i : indexes){
                l.add(pl.get(i).getId());
            }
        }
        return l;
    }

    public List<DataSetParameter> getParameters (BitSet b){
        List<DataSetParameter> pl = getParameterList();
        List<DataSetParameter> l = new ArrayList<DataSetParameter>();
        synchronized (pl){
            for (int i = b.nextSetBit(0); i > -1; i = b.nextSetBit(i+1)){
                l.add(pl.get(i));
            }
        }
        return l;
    }

    /**
     *
     * @param number prime number combination
     * @return
     */
    public List<DataSetParameter> getParameters (int number){
        List<DataSetParameter> pl = getParameterList();
        List<DataSetParameter> l = new ArrayList<DataSetParameter>();
//        BitSet primes = PrimeNumbers.getIndexes(number);
//        synchronized (pl){
//            for (int i = primes.nextSetBit(0); i>-1; i = primes.nextSetBit(i+1)){
//                l.add(pl.get(i));
//            }
//        }
        int[] primes = PrimeNumbers.getIndexes(number);
        synchronized (pl){
            for (int i : primes){
                l.add(pl.get(i));
            }
        }
        return l;
    }

        /**
     * Generate bitset mask for the chosen parameters
     * @param params) List<String|BigInteger|DataSetParameter> of parameters to include in bitset
     * @return
     */
    public BitSet generatePrimeNumber (List params){
        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            BitSet b = new BitSet(pl.size());

        if (params == null || params.size() == 0)
            return b;

        Object first = params.get(0);
        if (first instanceof String)
            for (String s : (List<String>) params){
                b.set(indexOf(s));
            }
        else if (first instanceof BigInteger)
            for (BigInteger s : (List<BigInteger>) params){
                b.set(indexOf(s));
            }
        else if (first instanceof DataSetParameter)
            for (DataSetParameter s : (List<DataSetParameter>) params){
                b.set(pl.indexOf(s));
            }
        else
            throw new IllegalArgumentException("To generate a BitSet parameter list must be of type List<String|BigInteger|DataSetParameter>. Received type: " + first.getClass());
        return b;
        }
    }

    /**
     * Add one more parameter, as a product of other parameters (for non linear regression)
     * @param columns flags which columns to multiply
     */
    public DataSetParameter addColumn (BitSet columns) {
        int number = PrimeNumbers.getNumber(columns);
        return addColumn(number);
    }

    /**
     * gets parameter for the specified prime number multiplication
     * @param number
     * @return
     */
    public DataSetParameter addColumn (int number) {
        if (number == 1)
            return null;

        List<DataSetParameter> pl = getParameterList();
        synchronized (pl){
            // find
            for (DataSetParameter p : pl) {
                if (p.getPrime_number() == number)
                    return p;
            }

            // create
            List<DataSetParameter> params = getParameters(number);

            StringBuilder nsb = new StringBuilder();
            //int number = 1;

            for (DataSetParameter par : params) {
                nsb.append(par.getName());
                nsb.append("*");
                //number*=par.getPrime_number();
            }

            DataSetParameter p = new DataSetParameter(Util.getID(), nsb.substring(0, nsb.length()-1), number);

            DataSet dataSet = getDataSet();
            synchronized (dataSet){
                dataSet.addColumn(params);
            }

            pl.add(p);
            return p;
        }
    }

    public List<DataRow> getParameters(DataSetHeader parent, BigInteger[] ids) {
        List<DataRow> drs = DaoFactory.DATA_ROW_DAO.getRows(this, ids);
        return drs;
    }
}
