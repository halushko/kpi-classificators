package com.netcracker.analysis.data.states;

public class MOSState extends State {
    private final int grade;
    private static final int GOODNESS_TRASHOLD = 3;

    public MOSState(int grade){
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }

    @Override
    public boolean isOk() {
        return grade > GOODNESS_TRASHOLD;
    }

    @Override
    public String toString() {
        return String.valueOf(grade);
    }
}
