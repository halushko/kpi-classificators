package com.netcracker.analysis.data.states;

public abstract class State {
    public abstract boolean isOk();

    @Override
    public boolean equals(Object other) {
        return other instanceof State && super.equals((State) other);
    }

    public boolean equals(State other) {
        return other != null && this.isOk() == other.isOk();
    }

    public static BooleanState good(){
        return new BooleanState(true);
    }
    public static BooleanState bad(){
        return new BooleanState(false);
    }

    public static State numericState(int grade) {
        return new MOSState(grade);
    }

    public static State valueOf(String str) {
        try {
            int grade = Integer.parseInt(str);
            return numericState(grade);
        } catch (NumberFormatException _) {
            return new BooleanState(Boolean.parseBoolean(str));
        }
    }
}
