package com.netcracker.analysis.data.states;

public class BooleanState extends State {

    private final boolean state;

    public BooleanState(boolean state) {

        this.state = state;
    }
    @Override
    public boolean isOk() {
        return state;
    }

    @Override
    public String toString() {
        return String.valueOf(state);
    }
}
