package com.netcracker.analysis;

import java.math.BigInteger;

public interface HasId {
    BigInteger getId();
}
