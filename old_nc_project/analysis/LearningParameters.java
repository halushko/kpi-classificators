package com.netcracker.analysis;

import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import org.apache.commons.logging.LogFactory;

import java.util.BitSet;

public class LearningParameters implements Cloneable
{
    private final DataSetHeader dataSet;
    private BitSet colsIn;
    private final BitSet colsOut;

    public LearningParameters(DataSetHeader dataSet, BitSet colsIn, BitSet colsOut){
        this.dataSet = dataSet;
        this.colsIn = colsIn;

        this.colsOut = colsOut;
    }

    public DataSetHeader getDataSet() {
        return dataSet;
    }

    public BitSet getColsIn() {
        return colsIn;
    }

    public void setColsIn(BitSet colsIn) {
        this.colsIn = colsIn;
    }

    public BitSet getColsOut() {
        return colsOut;
    }

    public LearningParameters clone() {
        try {
            return (LearningParameters) super.clone();
        } catch (CloneNotSupportedException e) {
            LogFactory.getLog(getClass()).error("Can't clone",e);
            throw new RuntimeException(e);
        }
    }

}
