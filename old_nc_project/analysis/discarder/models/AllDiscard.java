package com.netcracker.analysis.discarder.models;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.algorithms.BayesAlgorithm;
import com.netcracker.analysis.algorithms.DecisionTreeAlgorithm;
import com.netcracker.analysis.algorithms.LogisticLinearRegressionAlgorithm;
import com.netcracker.analysis.algorithms.PotentialFunctionAlgorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.discarder.params.AbstractDiscarder;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.models.PotentialFunctionModel;
import com.netcracker.analysis.util.Print;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class AllDiscard {
    public static String bestType_String(DataSetHeader dataSetHeader, BitSet in, BitSet out, String samplingGetter_type){
        List<ModelBenchmark> mb = new ArrayList<ModelBenchmark>();
        for(int i = 0; i < 1000; i++){
            AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(samplingGetter_type, dataSetHeader.getDataSet().rows());
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.01))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.1))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(10))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(100))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1000))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
//            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new LogisticLinearRegressionAlgorithm()).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
//            mb.add(new ModelBenchmark((PlaneModel)(new LogisticLinearRegressionAlgorithm().buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
//            mb.add(new ModelBenchmark((PlaneModel)(new BayesAlgorithm().buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.01)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.1)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(10)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(100)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1000)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
        }

        return Print.printModelBenchmarks(AbstractDiscarder.getBestEstimationModels(mb, ESTIMATION_TYPE.ADEQUACY));
    }

    public static List<PlaneModel> bestType(DataSetHeader dataSetHeader, BitSet in, BitSet out, String samplingGetter_type){
        List<ModelBenchmark> mb = new ArrayList<ModelBenchmark>();
        for(int i = 0; i < 100; i++){
            AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(samplingGetter_type, dataSetHeader.getDataSet().rows());
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.01))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.1))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(10))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(100))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1000))).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new DecisionTreeAlgorithm(new LogisticLinearRegressionAlgorithm()).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new LogisticLinearRegressionAlgorithm().buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new BayesAlgorithm().buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.01)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(0.1)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(10)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(100)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
            mb.add(new ModelBenchmark((PlaneModel)(new PotentialFunctionAlgorithm(new PotentialFunctionModel.PotentialFunction1(1000)).buildModel(dataSetHeader, in, out, samplingGetter)), samplingGetter.getCheck_indexes()));
        }

        return AbstractDiscarder.getBestModels(mb, ESTIMATION_TYPE.ADEQUACY);
    }

    public static List<PlaneModel> bestModelInAlgorithm(Algorithm algorithm, DataSetHeader dataSetHeader, BitSet in, BitSet out, String samplingGetter_type){
        List<ModelBenchmark> mb = new ArrayList<ModelBenchmark>();

        for(int i = 0; i < 333; i++){
            AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(samplingGetter_type, dataSetHeader.getDataSet().rows());
            mb.add(new ModelBenchmark((PlaneModel)algorithm.buildModel(dataSetHeader, in, out, samplingGetter), samplingGetter.getCheck_indexes()));
        }

        return AbstractDiscarder.getBestModels(mb, ESTIMATION_TYPE.ADEQUACY);
    }

    public static String bestModelInAlgorithm_String(Algorithm algorithm, DataSetHeader dataSetHeader, BitSet in, BitSet out, String samplingGetter_type){
        List<ModelBenchmark> mb = new ArrayList<ModelBenchmark>();

        for(int i = 0; i < 1000; i++){
            AbstractSamplingGetter samplingGetter = AbstractSamplingGetter.GetSamplingGetter(samplingGetter_type, dataSetHeader.getDataSet().rows());
            mb.add(new ModelBenchmark((PlaneModel)algorithm.buildModel(dataSetHeader, in, out, samplingGetter), samplingGetter.getCheck_indexes()));
        }
        return Print.printModelBenchmarks(AbstractDiscarder.getBestEstimationModels(mb, ESTIMATION_TYPE.ADEQUACY));
    }
}
