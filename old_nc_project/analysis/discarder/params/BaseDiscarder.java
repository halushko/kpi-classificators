package com.netcracker.analysis.discarder.params;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.estimators.ParamsEstimator;
import com.netcracker.analysis.models.GenericModel;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.MemoryBanchmarker;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class BaseDiscarder extends AbstractDiscarder{

    protected BaseDiscarder(DataSetHeader dataSetHeader, BitSet in, BitSet out, AbstractSamplingGetter samplingGetter) {
        super(dataSetHeader, in, out, samplingGetter);
    }


    /**
     * Estimates algorithms by deleting input params one by one
     *
     * @param algorithm - algorithm to estimate
     */
    @Override
    public void discard(final Algorithm algorithm) {
        clear();
        BitSet in = (BitSet) getIn().clone();
        String legend = "";
        String s;
        int count_param = getDataSetHeader().getRealParametersCount();

        BigInteger one = new BigInteger("1");
        BigInteger current = new BigInteger("0");

        List<BitSet> models_completed = new ArrayList<BitSet>();

        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();

        int counter = 0;
        for(;;){
            current = current.add(one);
            s = current.toString(2);

            if(s.length() > count_param)
                break;

            s = deleteBits(s, getOut());
            final BitSet bs = getBitSet(s, in);

            if (Util.countFlags(bs)<2)
                continue;
            if (models_completed.contains(bs))
                continue;

            models_completed.add(bs);

            if(Util.countFlags(bs) > 0) {
                MemoryBanchmarker<PlaneModel> bench =
                        new MemoryBanchmarker<PlaneModel>(){
                            @Override
                            protected PlaneModel run() {
                                return (PlaneModel) algorithm.buildModel(getDataSetHeader(), bs, getOut(), samplingGetter);
                            }
                        };
                PlaneModel model = bench.doBencmark();
                model.setLearnMemory(bench.getMemUsed());

                ModelBenchmark modelBenchmark = new ModelBenchmark(model, samplingGetter.getCheck_indexes());

                legend += modelBenchmark.toString();

                ParamsEstimator a = new ParamsEstimator(getDataSetHeader().getDataSet(), bs, getOut());

                legend += "\n" + "Output Ratio:\n\t\t" + a.getOutRatio() +"\n";
                legend += "Input Ratio:\n\t\t";

                double [] b = a.getInRatio();

                for(int j = 0; j < getDataSetHeader().getDataSet().columns(); j++)
                    legend += b[j] + ", ";
                legend +="\n";

                addEstimation(bs, modelBenchmark);
                addModel(bs, model);

                //System.out.println(""+ ++counter + ": "+ s);
            }
        }
        //System.out.println(legend);
    }

    private String deleteBits(String s, BitSet out) {
        int length = getDataSetHeader().getRealParametersCount();
        byte[] b = s.getBytes();
        for(int i = 0; i < length; i++){
            if(i >= s.length()) break;
            if(out.get(length - i - 1))
                b[s.length() - i - 1] = '0';
        }
        
        return new String(b);
    }

    private BitSet getBitSet(String bigMask, BitSet in){
        int count_param = getDataSetHeader().getRealParametersCount();
        BitSet res = new BitSet(count_param);

        for(int i = 0; i<bigMask.length(); i++ ){
            if(bigMask.charAt(bigMask.length()-1-i) == '1')
                res.set(count_param - i - 1);
        }
        res.and(in);
        return res;
    }
}
