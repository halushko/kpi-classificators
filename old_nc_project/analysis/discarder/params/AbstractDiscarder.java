package com.netcracker.analysis.discarder.params;

import com.netcracker.analysis.Algorithm;
import com.netcracker.analysis.benchmark.ModelBenchmark;
import com.netcracker.analysis.data.DataSet;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.estimation.ESTIMATION_TYPE;
import com.netcracker.analysis.models.PlaneModel;
import com.netcracker.analysis.util.Util;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;

import java.util.*;

public abstract class AbstractDiscarder {
    private DataSetHeader _dataSetHeader;
    private DataSet _dataSet;
    private BitSet _in, _out;
    private HashMap<BitSet, PlaneModel> _models = new HashMap<BitSet, PlaneModel>();
    private HashMap<BitSet, ModelBenchmark> _values = new HashMap<BitSet, ModelBenchmark>();
    //private int[] checkRows;

    /*-----------------GETTERS---------------------*/
//    public int[] getCheckRows() {
//        return checkRows;
//    }
    public HashMap<BitSet, PlaneModel> get_models() {
        if(_models == null)
            _models = new HashMap<BitSet, PlaneModel>();
        return _models;
    }
    public HashMap<BitSet, ModelBenchmark> get_values() {
        if(_values == null)
            _values = new HashMap<BitSet, ModelBenchmark>();
        return _values;
    }
    public DataSetHeader getDataSetHeader() {
        return _dataSetHeader;
    }
    public BitSet getIn() {
        return _in;
    }
    public BitSet getOut() {
        return _out;
    }
    /*----------------/GETTERS---------------------*/

    /*-----------------SETTERS---------------------*/
//    public void setCheckRows(int[] checkRows) {
//        this.checkRows = checkRows;
//    }
    public void setDataSetHeader(DataSetHeader dataSetHeader) {
        if(dataSetHeader == null)
            throw new IllegalArgumentException("DataSetHeader is null");
        this._dataSetHeader = dataSetHeader;
        this._dataSet = dataSetHeader.getDataSet();
        if(_dataSet == null) throw new IllegalArgumentException("DataSet is null");
        clear();
    }
    public void setIn(BitSet in) {
        if(in == null || Util.countFlags(in) < 1)
            throw new IllegalArgumentException("Input params is null or there count is less then 1");
        this._in = in;
        clearAllModelsByKey(in);
    }
    public void setOut(BitSet out) {
        if(out == null || Util.countFlags(out) < 1)
            throw new IllegalArgumentException("Output params is null or there count is less then 1");
        this._out = out;
        clear();
    }
    /*----------------/SETTERS---------------------*/

    protected AbstractSamplingGetter samplingGetter;

    protected AbstractDiscarder(DataSetHeader dataSetHeader, BitSet in, BitSet out, AbstractSamplingGetter samplingGetter){
        setIn(in);
        setOut(out);
        setDataSetHeader(dataSetHeader);
        this.samplingGetter = samplingGetter;
    }

    public static AbstractDiscarder GetDiscarder(DataSetHeader dataSetHeader, BitSet in, BitSet out, AbstractSamplingGetter samplingGetter, DISCARDER_TYPE type){
        switch (type){
            case BASE: return new BaseDiscarder(dataSetHeader, in, out, samplingGetter);
            default: return null;
        }
    }

    /**
     * Add model to Map of models
     * @param key - BitSet appropriate to model
     * @param value - model
     */
    public void addModel(BitSet key,  PlaneModel value){
        get_models().put(key, value);
    }

    /**
     * Add estimation to Map of estimation
     * @param key - BitSet appropriate to model
     * @param value - estimations appropriate to model
     */
    public void addEstimation(BitSet key, ModelBenchmark value){
        get_values().put(key, value);
    }

    /**
     * Get generated model by input parameters
     * @param key - BitSet key of input parameters
     * @return generated model by input parameters
     */
    public PlaneModel getModel(BitSet key){
        return get_models().get(key);
    }

    /**
     * Get estimation value of model
     * @param key - BitSet of input parameters of model
     * @param est_type - chosen estimation type
     * @return chosen estimation value of model
     */
    public Object getValue(BitSet key, ESTIMATION_TYPE est_type){
        return get_values().get(key).getValue(est_type);
    }

    /**
     * Clear all generated models and estimations
     */
    public void clear(){
        get_values().clear();
        get_models().clear();
    }

    /**
     * Get models with the best estimation and lowest count of input parameters
     * @param estimation_type - the type of estimation
     * @return ArrayList of the best models
     */
    public List<ModelBenchmark> getBestEstimationModels(final ESTIMATION_TYPE estimation_type){
        return getBestEstimationModels(get_values().values(), estimation_type);
    }

    /**
     * Get models with the best estimation and lowest count of input parameters
     * @param estimation_type - the type of estimation
     * @return ArrayList of the best models
     */
    public static List<ModelBenchmark> getBestEstimationModels(Collection<ModelBenchmark> benchmarks, final ESTIMATION_TYPE estimation_type){
        List<ModelBenchmark> best_models = new ArrayList<ModelBenchmark>();
        best_models.addAll(benchmarks);

        Collections.sort(best_models, new Comparator<ModelBenchmark>() {
            private Comparator comp;
            private Comparator getComparator(ModelBenchmark o1){
                return comp == null ? comp = o1.getEstimator(estimation_type).getComparator() : comp;
            }

            public int compare(ModelBenchmark o1, ModelBenchmark o2) {
                int comp = getComparator(o1).compare(o1.getValue(estimation_type), o2.getValue(estimation_type));
                if (comp == 0)
                    comp = -/*the less the better*/ ((Integer)Util.countFlags(o1.getModel().getColsIn())).compareTo(Util.countFlags(o2.getModel().getColsIn()));
                return - comp;
            }
        });

        return best_models.subList(0, Math.max((int) (((double) best_models.size()) / 4), Math.min(best_models.size(), 3)));
    }

    /**
     * Get models with the best estimation and lowest count of input parameters
     * @param estimation_type - the type of estimation
     * @return ArrayList of the best models
     */
    public static List<PlaneModel> getBestModels(Collection<ModelBenchmark> benchmarks, final ESTIMATION_TYPE estimation_type){
        List<ModelBenchmark> best_models = new ArrayList<ModelBenchmark>();
        best_models.addAll(benchmarks);

        Collections.sort(best_models, new Comparator<ModelBenchmark>() {
            private Comparator comp;
            private Comparator getComparator(ModelBenchmark o1){
                return comp == null ? comp = o1.getEstimator(estimation_type).getComparator() : comp;
            }

            public int compare(ModelBenchmark o1, ModelBenchmark o2) {
                int comp = getComparator(o1).compare(o1.getValue(estimation_type), o2.getValue(estimation_type));
                if (comp == 0)
                    comp = -/*the less the better*/ ((Integer)Util.countFlags(o1.getModel().getColsIn())).compareTo(Util.countFlags(o2.getModel().getColsIn()));
                return - comp;
            }
        });

        List<ModelBenchmark> the_best = best_models.subList(0, Math.max((int) (((double) best_models.size()) / 4), Math.min(best_models.size(), 3)));
        List<PlaneModel> models = new ArrayList<PlaneModel>();
        for(ModelBenchmark mb: the_best){
            models.add(mb.getModel());
        }
        return models;
    }
    /*----------------/ABSTRACT---------------------*/
    public abstract void discard(Algorithm algorithm);
    /*----------------/ABSTRACT---------------------*/

    private void clearAllModelsByKey(BitSet in) {
        get_values();
        get_models();
        
        for(BitSet bs: _models.keySet()){
            BitSet t = (BitSet) bs.clone();
            t.and(in);
            if(Util.countFlags(t) > 0){
                _models.remove(t);
                _values.remove(t);
            }
        }
    }
}
