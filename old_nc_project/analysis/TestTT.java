package com.netcracker.analysis;

import com.netcracker.analysis.algorithms.BayesAlgorithm;
import com.netcracker.analysis.dao.DataSetDaoImpl;
import com.netcracker.analysis.dao.SaveException;
import com.netcracker.analysis.data.DataSetHeader;
import com.netcracker.analysis.data.LearningRow;
import com.netcracker.analysis.data.XmlTTParameter;
import com.netcracker.analysis.data.XmlTroubleTicket;
import com.netcracker.analysis.models.BayesModel;
import com.netcracker.analysis.util.TroubleTicketChecker;
import com.netcracker.analysis.util.sampling.AbstractSamplingGetter;
import com.netcracker.analysis.util.sampling.SAMPLING_TYPE;

import java.math.BigInteger;
import java.util.BitSet;

public class TestTT {
    
    public static void testDeserialise(){
        TroubleTicketChecker ttc = new TroubleTicketChecker();
        LearningRow tt = ttc.deserializeTroubleTicket(XmlTroubleTicket.TEST_TROUBLE_TICKET);
    }
    public static void testSerialise(){
        TroubleTicketChecker ttc = new TroubleTicketChecker();
        XmlTTParameter ttp = new XmlTTParameter("id_pappp", 1);
        XmlTroubleTicket xml_tt = new XmlTroubleTicket("datas", 0, new XmlTTParameter[]{ttp}, "2012-05-31T11:43:48.914+03:00");
        String s = ttc.serializeTroubleTicket(xml_tt);
    }

    public static void testAdding(){
//        TroubleTicketChecker ttc = new TroubleTicketChecker();
//        for(String tts: TTs){
//            ttc.ReceiveTroubleTicket(tts);
//        }

        DataSetHeader dataset = new DataSetDaoImpl().get(BigInteger.valueOf(5555408163513261983L));
    }

    
    public static void main(String[] args) {
        //testAdding2();
        test1_0();
    }

    private static void test1_0() {

    }


    private static void testAdding2() {
        final DataSetHeader dataset = new DataSetDaoImpl().get(BigInteger.valueOf(5555408163513261983L));
        BitSet in = new BitSet(5){{
            set(dataset.indexOf("Jitter"));
            set(dataset.indexOf("LostPacks"));
            set(dataset.indexOf("Delay"));
            set(dataset.indexOf("Speed"));
        }};
        BitSet out = new BitSet(5){{
            set(dataset.indexOf("OutParam"));
        }};

        BayesModel mo = null;
        try {
            mo = (BayesModel)
                    dataset.createModel(new BayesAlgorithm(), in, out, AbstractSamplingGetter.GetSamplingGetter(SAMPLING_TYPE.RANDOM, dataset.getDataSet().rows()));
        } catch (SaveException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static String[] TTs ={
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.3</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>2.8</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.1</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>2.6</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>7.7</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.3</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.4</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.4</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.1</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.9</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.1</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.7</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.1</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.9</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.1</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>5.8</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>2.7</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.8</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.2</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.7</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.3</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.7</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3</value></XmlTTParameter></parameters></XmlTroubleTicket>",
//            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.3</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>2.5</value></XmlTTParameter></parameters></XmlTroubleTicket>",
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.5</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3</value></XmlTTParameter></parameters></XmlTroubleTicket>",
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>6.2</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3.4</value></XmlTTParameter></parameters></XmlTroubleTicket>",
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XmlTroubleTicket><dataset>6555408163513261980</dataset><type>0</type><date>2012-05-31T18:50:10.037+03:00</date><parameters><XmlTTParameter><id>6555408163513261981</id><value>5.9</value></XmlTTParameter><XmlTTParameter><id>6555408163513261982</id><value>3</value></XmlTTParameter></parameters></XmlTroubleTicket>"
    };

}
